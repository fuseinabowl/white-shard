#include "lua_shard.h"

#include <fstream>
#include <vector>
#include <memory>
#include <iostream>
#include <limits>
#include "temporary_object.h"
#include "initialisation_message.h"

const int garbageCollectionSteps = 10;

namespace WhiteShard
{
  namespace Lua
  {
    template <>
      const std::string& LuaTraits<StatePtr>::getName() noexcept
    {
      static const std::string name("lua_thread");
      return name;
    }

    // the actual State stuff
    // we populate the state pointer inside the constructor so that 
    // the state can hold a weak pointer to itself
    State::State(StatePtr& pointer, Engine& engine, precedence initialPrecedence) : state(luaL_newstate()), temporaryObjects(nullptr)
    {
      luaL_openlibs(this->state);
      pointer = StatePtr(this);
      this->thisPointer = pointer;

      // need a minimalist getPrecedence function
      lua_createtable(state, 0, 1);
      lua_pushnumber(state, initialPrecedence);
      lua_pushcclosure(state, State::InitialisationMessage::getPrecedence, 1);
      lua_rawseti(state, -2, Registry::PriorityFunction);

      lua_setfield(state, LUA_REGISTRYINDEX, Registry::WhiteShard.c_str());

      // add the initialisation message function
      this->addMessage(MessagePtr(new State::InitialisationMessage(engine, initialPrecedence)));
    }

    State::~State()
    {
      this->closeState();
    }

    void State::closeState()
    {
      if (this->state != nullptr)
      {
        lua_close(this->state);
        this->state = nullptr;
      }
    }

    // messages deque uses the convention (push_back, pop_front) for normal usage
    void State::addMessage(MessagePtr& message)
    {
      this->addMessage(std::move(message));
    }

    void State::addMessage(MessagePtr&& message)
    {
      State::QueueLock queueLock(this->messageMutex);
      this->messageQueue.push_back(std::move(message));
    }

    class GarbageCollectionMessage : public Message
    {
     public:
      void sendMessage(Lua::State& luaState) const noexcept
      {
        // do an entire garbage collection step
        while(1 != lua_gc(luaState, LUA_GCSTEP, garbageCollectionSteps));
      }
    };

    void State::doMessage() throw (Lua::MessageException)
    {
      MessagePtr nextMessage;

      {
        // encapsulate the lock so we don't lock the mutex for longer than we need it
        State::QueueLock queueLock(this->messageMutex);

        if (this->messageQueue.empty())
        {
          // do garbage collection here if the state needs to
          nextMessage = MessagePtr(new GarbageCollectionMessage());
        }
        else
        {
          nextMessage = std::move(this->messageQueue.front());
          this->messageQueue.pop_front();
        }
      }

      // now do the message itself
      nextMessage->sendMessage(*this);

      (*this->temporaryObjects)->clear();
    }

    State::operator lua_State*()
    {
      return this->state;
    }

    State::precedence State::getPrecedence() const
    {
      StackChecker checker(state, 0, "State::getPrecedence");

      // check the cache
      int returnPrecedence(this->messageQueue.size()); // default value if the script breaks

      lua_State* state(this->state);
      lua_getfield(state, LUA_REGISTRYINDEX, Registry::WhiteShard.c_str());
      lua_rawgeti(state, -1, Registry::PriorityFunction);
      lua_replace(state, -2);

      // put in the number of jobs left
      lua_pushnumber(state, this->messageQueue.size());

      try
      {
        observePcall(lua_pcall(state, 1, 1, 0), state);

        if (lua_isnumber(state, -1) == 1)
        {
          returnPrecedence = lua_tointeger(state, -1);
        }

        lua_pop(state, 1); // remove the returned value from the lua stack
      }
      catch (LuaException& ex)
      {
        std::cerr << "error during \"getPriority\" function: " << ex.what() << std::endl;
      }

#ifndef NDEBUG
      // in case the user has used temporary objects during precedence getting
      // (which is not allowed!)
      if (this->temporaryObjects != nullptr)
      {
        assert((*this->temporaryObjects)->empty());
      }
#endif

      return returnPrecedence;
    }

    StatePtr State::getThisPointer() const
    {
      return StatePtr(this->thisPointer);
    }

    void State::setTemporaryObjects(TemporaryCollection* objects)
    {
      // this function should only be called once, by the initialisation message
      assert(this->temporaryObjects == nullptr);

      this->temporaryObjects = objects;
    }

    Message::~Message() noexcept
    {
    }
  }
}
