#ifndef _LUA_REGISTRY_H_
#define _LUA_REGISTRY_H_

#include <string>

namespace WhiteShard{
  namespace Registry{
    extern const std::string WhiteShard;

    enum WhiteShardRegisters
    {
      Metatables = 1, // lua indices start at one
      MessagingLoadCallbacks,
      MessagingReceiver,
      TemporaryCollection,
      PriorityFunction,
      ContextLock,
      PointerToUserdataMap,
      Engine,
      TextureMinificationFilters,
      TextureMagnificationFilters,
      TextureWrapTypes,
      TextureAnisotropicFilters,

      aux_MaxRegistryIndex,
      MaxRegistryIndex = aux_MaxRegistryIndex - 2
    };

    enum MetatableRegisters
    {
      Box = 1,
      MaxMetatableIndex = Box
    };
  }
}

#endif
