#ifndef _GRAPHICS_DESTRUCTION_THREAD_H_
#define _GRAPHICS_DESTRUCTION_THREAD_H_

#include <vector>
#include <thread>
#include <mutex>
#include <condition_variable>
#include "sync_object.h"

namespace WhiteShard{
  namespace Window{
    namespace Graphics{
      class DestructionCommand
      {
        std::vector<SyncWrapper> syncs;
       protected:
        // sync has been waited on, a context is locked
        virtual void performDestruction() noexcept = 0;
       public:
        DestructionCommand(SyncObject&) noexcept;
        void operator()() noexcept;
        virtual ~DestructionCommand() noexcept;

        DestructionCommand(const DestructionCommand&) = delete;
        DestructionCommand& operator=(const DestructionCommand&) = delete;
      };
      typedef std::unique_ptr<DestructionCommand> DestructionCommandPtr;

      class ObjectDestructor
      {
        bool tearDown; // when true, destruction thread will finish
        std::vector<DestructionCommandPtr> destructionCommands;
        std::mutex destructionCommandsAccess;
        std::condition_variable destructionCommandsCondition;
        std::thread destructionThread;
        static void destructionThreadBody(
          std::vector<DestructionCommandPtr>&,
          std::mutex&,
          std::condition_variable&,
          bool&
            ) noexcept;
       public:
        ObjectDestructor() noexcept;
        ~ObjectDestructor() noexcept;

        void pushDestructionCommand(DestructionCommandPtr&&) noexcept;
      };
    }
  }
}

#endif
