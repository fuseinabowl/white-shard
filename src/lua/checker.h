#ifndef _LUA_CHECKER_H_
#define _LUA_CHECKER_H_

#include <boost/mpl/size.hpp>
#include <string>

/*
   this file is used to centralise the strings that the user will see, in order to
   standardise the strings and to make it easier to change locales.
*/

class lua_State;

namespace WhiteShard{
  namespace Lua{
    class StackChecker
    {
#ifndef NDEBUG
      lua_State* state;
      int expectedStackSize;
      std::string identifier;
#endif
     public:
      StackChecker(lua_State*, int stackChange, std::string identifier);

      StackChecker(const StackChecker&) = delete;
      StackChecker& operator=(const StackChecker&) = delete;
      StackChecker(StackChecker&&) = delete;
      StackChecker& operator=(StackChecker&&) = delete;

      ~StackChecker();
    };

    template <class ResultsList, class Class, class MethodNameClass, class ParameterList, bool ParamsVariadic = false>
      class SignatureChecker
    {
      lua_State* state;

      std::string constructSigniature();
     public:
      SignatureChecker(lua_State*);
#ifndef NDEBUG
      ~SignatureChecker();
#endif

      static const typename boost::mpl::size<ResultsList>::type::value_type resultCount;
      static const std::string& functionName;
    };

    template <class T>
      struct Optional
    {
      typedef T type;
    };
  }
}

#include "checker_fwd.h"

#endif
