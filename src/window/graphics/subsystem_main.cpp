#include "lua/checker.h"
#include "subsystem.h"
#include "registry.h"
#include "subsystem_functions.h"
#include "mesh.h"
#include <GL/glew.h>
#include <sstream>

namespace WhiteShard{
  namespace Lua{
    template <>
      const std::string& Lua::LuaTraits<Window::Graphics::SubsystemPtr>::getName() noexcept
    {
      static const std::string name("graphics");
      return name;
    }
  }

  namespace Window{
    namespace Graphics{
      const unsigned int 
        requiredOpenGLMajorVersion = 3,
        requiredOpenGLMinorVersion = 3;

      Subsystem::Subsystem(unsigned int extraContexts) throw (SubsystemConstructorException) :
        maxAnisotropicFiltering(1.0)
      {
        {
          sf::Context glewInitialiserContext;

          glewExperimental = GL_TRUE;
          GLenum initVal = glewInit();
          if (GLEW_OK != initVal)
          {
            throw (GLEWException(initVal));
          }

          // check the openGL version
          std::string versionString(reinterpret_cast<const char*>(glGetString(GL_VERSION)));
          size_t firstDot = versionString.find_first_of('.');
          std::stringstream majorVersionStream(versionString.substr(0, firstDot));
          std::stringstream minorVersionStream(versionString.substr(firstDot + 1, versionString.find_first_of('.', firstDot + 1) - (firstDot + 1)));

          unsigned int majorVersion, minorVersion;

          if (majorVersionStream >> majorVersion && minorVersionStream >> minorVersion)
          {
            if (!(requiredOpenGLMajorVersion < majorVersion || (requiredOpenGLMajorVersion == majorVersion && requiredOpenGLMinorVersion == minorVersion)))
            {
              throw VersionInsufficientException(majorVersion, minorVersion);
            }
          }
          else
          {
            throw VersionStringException();
          }

          if (GLEW_EXT_texture_filter_anisotropic)
          {
            glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &maxAnisotropicFiltering);
          }

          // need extra local variable because this->textureUnits is GLuint
          // and compiler complains about using GLuint* in GLint* parameter
          GLint textureUnits;
          glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, &textureUnits);
          this->textureUnits = textureUnits;
        }

        if (1 > extraContexts)
        {
          extraContexts = 1;
        }

        for (unsigned int currentContext = 0; currentContext <= extraContexts; ++currentContext)
        {
          this->availablePureContexts.push_back(std::unique_ptr<PureContextWrapper>(new PureContextWrapper(*this)));
          this->activePureContexts.push_back(this->availablePureContexts.back().get());
        }

        // set up the unit mesh
        // we need a context lock while we do openGL stuff
        // no one else can have taken a lock, and there is always at least 1
        // pure context created
        ContextLockPtr tempLock(new PureContextLock(std::move(this->availablePureContexts.back()), *this));
        this->availablePureContexts.pop_back();

        // now actually create the mesh
        this->unitMesh = MeshPtr(new Mesh(
                std::vector<DataListing>{
									DataListing{GL_FLOAT_VEC2, {{0.0f, 0.0f}, {1.0f, 0.0f}, {1.0f, 1.0f}, {0.0f, 1.0f}}}
										},
                std::vector<SubmeshListing>{
									SubmeshListing{GL_POINT, {0}}, 
									SubmeshListing{GL_LINES, {0, 1}},
								 	SubmeshListing{GL_QUADS, {0, 1, 2, 3}}
										},
                *this,
                *tempLock
              ));
        tempLock->constructorLock(this->unitMesh);

        // set up the vertex pointers
        GLint maxAttrib;
        glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &maxAttrib);
        for (GLint currentAttrib(0); currentAttrib < maxAttrib; ++currentAttrib)
        {
          glEnableVertexAttribArray(currentAttrib);
        }
      }

      Subsystem::~Subsystem() noexcept
      {
        // destroy the unitMesh while the object destructor is still valid
        this->unitMesh.reset();
      }

      void Subsystem::pushSubsystem(lua_State* state)
      {
        Lua::StackChecker checker(state, 1, "Graphics::pushSubsystem");

        // add the context lock ptr to the registry
        lua_getfield(state, LUA_REGISTRYINDEX, Registry::WhiteShard.c_str());
        Lua::generateMetatable<ContextLockPtr>(state);
        new (Lua::allocate<ContextLockPtr>(state)) ContextLockPtr();
        lua_rawseti(state, -2, Registry::ContextLock);
        lua_pop(state, 1);

        // push the actual graphics object now
        Lua::generateMetatable<Graphics::SubsystemPtr>(state, 0, 1);
        new (Lua::allocate<Graphics::SubsystemPtr>(state)) Graphics::SubsystemPtr(this);
        lua_getmetatable(state, -1);
        
        // index table
        lua_createtable(state, 0, 2);

        // data
        Subsystem::pushShaderTypes(state);
        lua_setfield(state, -2, ShaderTypeMap::name.c_str());

        Subsystem::pushUnitMesh(state);
        lua_setfield(state, -2, "unitMesh");

        Subsystem::pushMagnificationFilters(state);
        lua_setfield(state, -2, "magnificationFilters");

        Subsystem::pushMinificationFilters(state);
        lua_setfield(state, -2, "minificationFilters");

        Subsystem::pushAnisotropicFilters(state, *this);
        lua_setfield(state, -2, "anisotropicFilters");

        Subsystem::pushWrapTypes(state);
        lua_setfield(state, -2, "wrapTypes");

        // functions
        Subsystem::pushCreateShader(state);
        lua_setfield(state, -2, CreateShaderFunction::name.c_str());

        Subsystem::pushCreateShaderProgram(state);
        lua_setfield(state, -2, CreateShaderProgramFunction::name.c_str());

        Subsystem::pushRender(state);
        lua_setfield(state, -2, RenderFunction::name.c_str());

        Subsystem::pushLoadTexture(state);
        lua_setfield(state, -2, LoadTextureFunction::name.c_str());

        Subsystem::pushCreateTexture(state);
        lua_setfield(state, -2, CreateTextureFunction::name.c_str());

        Subsystem::pushContextLock(state);
        lua_setfield(state, -2, ContextLockFunction::name.c_str());

        Subsystem::pushCreateMatrix(state);
        lua_setfield(state, -2, CreateMatrixFunction::name.c_str());

        lua_setfield(state, -2, "__index");

        // pop the metatable
        lua_pop(state, 1);
      }
    }
  }
}
