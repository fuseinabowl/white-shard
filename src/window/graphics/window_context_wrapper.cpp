#include "context_wrapper.h"
#include "subsystem.h"
#include <iostream>

namespace WhiteShard{
  namespace Window{
    namespace Graphics{
      // WindowContextWrapper methods
      WindowContextWrapper::WindowContextWrapper(const Subsystem& subs, WindowWrapper& parent) noexcept :
        ContextWrapperBase(subs),
        windowPtr(&parent)
      {
        auto access = parent.getAccess();
        auto size = access->getSize();
        access->setActive(true);
        this->initialise(size.x, size.y);
        access->setActive(false);
      }

      WindowContextWrapper::~WindowContextWrapper() noexcept
      {
      }

      // WindowContextLock methods
      WindowContextLock::WindowContextLock(WindowContextWrapperPtr&& windowContext, Subsystem& subsystem, WindowWrapperPtr maintainer) noexcept :
        contextPtr(std::move(windowContext)), subsystem(&subsystem), wrapperMaintainer(std::move(maintainer))
      {
        bool success = this->contextPtr->windowPtr->getAccess()->setActive(true);
        assert(success);
      }

      WindowContextLock::~WindowContextLock() noexcept
      {
        bool success = this->contextPtr->windowPtr->getAccess()->setActive(false);
        assert(success);

        // return this context to the subsystem
        this->subsystem->returnWindowContext(this->wrapperMaintainer, std::move(this->contextPtr));
      }

      ContextWrapperBase& WindowContextLock::getContextWrapper() const noexcept
      {
        return *this->contextPtr;
      }

      void WindowContextLock::relock() noexcept
      {
#ifdef NDEBUG
        this->contextPtr->windowPtr->getAccess()->setActive(true);
#else
        assert(this->contextPtr->windowPtr->getAccess()->setActive(true));
#endif
      }

      void WindowContextLock::render() const noexcept
      {
        auto windowAccess = this->contextPtr->windowPtr->getAccess();
        glFlush(); // TODO only here for testing, should be called by windowAccess display too
        windowAccess->display();

        // do resizing here so it doesn't intefere with draw commands
        auto size = windowAccess->getSize();
        this->contextPtr->setSize(size.x, size.y);
      }
    }
  }
}
