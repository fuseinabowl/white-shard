#include "subsystem.h"
#include <boost/mpl/vector.hpp>
#include <memory>
#include "interstate_message.h"

namespace WhiteShard{
  namespace Messaging{
    void Subsystem::pushCreateState(lua_State* state)
    {
      Lua::StackChecker checker(state, 1, "Messaging::pushCreateState");
      // get the state metatable for the c closure
      lua_getfield(state, LUA_REGISTRYINDEX, Registry::WhiteShard.c_str());
      lua_rawgeti(state, -1, Registry::Metatables);
      lua_getfield(state, -1, Lua::LuaTraits<Lua::StatePtr>::getName().c_str());
      lua_remove(state, -2);
      // get the engine for the c closure
      lua_rawgeti(state, -2, Registry::Engine);
      // get the pointer to userdata map
      // we need to do this for states only because states have a way of accessing their own state without going through the messaging system
      // so it is possible for them not to be registered
      lua_rawgeti(state, -3, Registry::PointerToUserdataMap);
      // make the closure
      lua_pushcclosure(state, Subsystem::luaCreateState, 3);
      // clean up lua
      lua_replace(state, -2);
    }

    struct CreateStateFunction
    {
      static const std::string& name;
    };
    const std::string& CreateStateFunction::name(Subsystem::createState);
    
    int Subsystem::luaCreateState(lua_State* state)
    {
      Lua::SignatureChecker<
        boost::mpl::vector<Lua::StatePtr>::type,
        SubsystemLuaPtr, CreateStateFunction,
        boost::mpl::vector<
          Lua::NativeTypeWrapper<LUA_TSTRING>,
          Lua::Optional<Lua::NativeTypeWrapper<LUA_TNUMBER> >,
          Lua::Optional<Lua::NativeTypeWrapper<LUA_TFUNCTION> >
            >::type
          > checker(state);

      // we'll need a filename and an initial precedence
      Subsystem& subsystem(*Lua::get<SubsystemLuaPtr>(state, 1));
      std::string filename(lua_tostring(state, 2));
      Lua::State::precedence loadingPrecedence(lua_tonumber(state, 3));
      loadingPrecedence = 0 == loadingPrecedence ? 1 : loadingPrecedence;

      // and if the user wants a callback, they'll need to supply a string globally resolvable to a callable object (function or __call metatable enabled object)
      Lua::MessagePtr loadMessage(nullptr);

      std::string callbackString;
      const char* callbackCharStar(lua_tostring(state, 4));
      if (NULL != callbackCharStar) // if the stack contained a valid string
      {
        callbackString = callbackCharStar;

        // if they want finer grained loading feedback, they should supply a number other than 0
        // tonumber returns 0 on a failure, so checking for loadStep == 0 checks for both possible errors
        unsigned int loadStep(lua_tointeger(state, 5));
        if (0 == loadStep) // if the stack contained a valid number (must be non-zero)
        {
          // if the value on the stack was unconvertible, set loadstep to max
          loadStep = UINT_MAX;
        }

        // we need to get a copy of this state's stateptr
        lua_getfield(state, -1, Subsystem::getThisState.c_str());
        lua_call(state, 0, 1);

        Lua::StatePtr& statePtr(Lua::get<Lua::StatePtr>(state, -1));

        loadMessage = Lua::MessagePtr(new LoudLoadFileMessage(std::move(filename), loadStep, std::move(LoudLoaderStepper(statePtr, callbackString, subsystem))));
      }
      else
      {
        loadMessage = Lua::MessagePtr(new QuietLoadFileMessage(std::move(filename)));
      }

      // create the handle and put the state ptr onto the lua stack
      Lua::Handle newHandle(loadingPrecedence, *subsystem.engine);
      (*newHandle)->addMessage(std::move(loadMessage));

      lua_pushnil(state);
      lua_copy(state, lua_upvalueindex(1), -1);
      new (Lua::allocate<Lua::StatePtr>(state)) Lua::StatePtr(*newHandle);

      // now register the pointer to the pointer map
      lua_pushnil(state);
      lua_copy(state, -2, -1);
      lua_rawsetp(state, lua_upvalueindex(3), &**newHandle);

      // now that we've extracted the StatePtr, send the handle to be executed
      subsystem.engine->newHandle(std::move(newHandle));

      return 1;
    }
  }
}
