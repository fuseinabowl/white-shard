#include "event_handler.h"
#include "lua_shard.h"
#include <boost/mpl/vector.hpp>
#include "subsystem.h"
#include "window_wrapper.h"

namespace WhiteShard{
  namespace Lua{
    template <>
      const std::string& LuaTraits<Window::EventHandler>::getName() noexcept
    {
      static const std::string name("event_handler");
      return name;
    }
  }

  namespace Window{
    const EventHandler::EventMap EventHandler::eventMap
    {
      {"closed", sf::Event::Closed},
      {"resized", sf::Event::Resized},
      {"lostFocus", sf::Event::LostFocus},
      {"gainedFocus", sf::Event::GainedFocus},
      {"keyPressed", sf::Event::KeyPressed},
      {"keyReleased", sf::Event::KeyReleased},
      {"mouseWheelMoved", sf::Event::MouseWheelMoved},
      {"mouseButtonPressed", sf::Event::MouseButtonPressed},
      {"mouseButtonReleased", sf::Event::MouseButtonReleased},
      {"mouseMoved", sf::Event::MouseMoved},
      {"mouseEntered", sf::Event::MouseEntered},
      {"mouseLeft", sf::Event::MouseLeft},
      {"joystickButtonPressed", sf::Event::JoystickButtonPressed},
      {"joystickButtonReleased", sf::Event::JoystickButtonReleased},
      {"joystickMoved", sf::Event::JoystickMoved},
      {"joystickConnected", sf::Event::JoystickConnected},
      {"joystickDisconnected", sf::Event::JoystickDisconnected}
    };

    void EventHandler::pushCreateEventHandler(lua_State* state)
    {
      Lua::StackChecker checker(state, 1, "pushCreateEventHandler");

      // this table is used to get handler lookup tables from event handler objects
      // has weak keys so that if a handler object is forgotten by the user, it can
      // still be deleted
      lua_createtable(state, 0, 0);
      lua_createtable(state, 0, 1);
      lua_pushstring(state, "k");
      lua_setfield(state, -2, "__mode");
      lua_setmetatable(state, -2);

      Lua::generateMetatable<EventHandler>(state, 1, 1);

      // index table
      lua_createtable(state, 0, 2);

      lua_pushnil(state);
      lua_copy(state, -4, -1);
      lua_pushcclosure(state, EventHandler::luaSetHandler, 1);
      lua_setfield(state, -2, "setHandler");

      lua_pushnil(state);
      lua_copy(state, -4, -1);
      lua_pushcclosure(state, EventHandler::luaGetHandler, 1);
      lua_setfield(state, -2, "getHandler");

      lua_pushnil(state);
      lua_copy(state, -4, -1);
      lua_pushcclosure(state, EventHandler::luaHandleEvents, 1);
      lua_setfield(state, -2, "handleEvents");

      lua_setfield(state, -2, "__index");

      // adding the closure
      lua_insert(state, -2);
      lua_pushcclosure(state, EventHandler::luaCreateEventHandler, 2);
    }

    struct CreateEventHandlerFunction
    {
      static const std::string name;
    };
    const std::string CreateEventHandlerFunction::name("createEventHandler");

    int EventHandler::luaCreateEventHandler(lua_State* state)
    {
      Lua::SignatureChecker<
        boost::mpl::vector<EventHandler>::type,
        SubsystemLuaPtr, CreateEventHandlerFunction,
        boost::mpl::vector<>::type
          > sigChecker(state);

      lua_pushnil(state);
      lua_copy(state, lua_upvalueindex(1), -1);
      Lua::allocate<EventHandler>(state);

      // get the handler->(string->function) table
      lua_pushnil(state);
      lua_copy(state, lua_upvalueindex(2), -1);
      // make a new entry for the eventHandler
      lua_pushnil(state);
      lua_copy(state, -3, -1);
      lua_createtable(state, sf::Event::Count, 0);
      lua_settable(state, -3);
      lua_pop(state, 1);

      return sigChecker.resultCount;
    }

    struct SetHandlerFunction
    {
      static const std::string name;
    };
    const std::string SetHandlerFunction::name("setHandler");

    int EventHandler::luaSetHandler(lua_State* state)
    {
      Lua::SignatureChecker<
        boost::mpl::vector<>::type,
        EventHandler, SetHandlerFunction,
        boost::mpl::vector<Lua::NativeTypeWrapper<LUA_TSTRING>, Lua::Optional<Lua::NativeTypeWrapper<LUA_TFUNCTION> > >::type
          > sigChecker(state);

      lua_settop(state, 3);
      lua_pushnil(state);
      lua_copy(state, 1, -1);
      lua_gettable(state, lua_upvalueindex(1));
      // we now have the eventHandler's own map at the top of the table, just above the parameters
      lua_insert(state, 3);
      lua_rawseti(state, 3, EventHandler::eventMap.find(lua_tostring(state, 2))->second);

      return sigChecker.resultCount;
    }

    struct GetHandlerFunction
    {
      static const std::string name;
    };
    const std::string GetHandlerFunction::name("getHandler");

    int EventHandler::luaGetHandler(lua_State* state)
    {
      Lua::SignatureChecker<
        boost::mpl::vector<Lua::Optional<Lua::NativeTypeWrapper<LUA_TFUNCTION> > >::type,
        EventHandler, GetHandlerFunction,
        boost::mpl::vector<Lua::NativeTypeWrapper<LUA_TSTRING> >::type
          > sigChecker(state);

      lua_settop(state, 2);
      lua_insert(state, 1);
      lua_gettable(state, lua_upvalueindex(1));
      lua_rawgeti(state, 2, EventHandler::eventMap.find(lua_tostring(state, 1))->second);

      return sigChecker.resultCount;
    }

    struct HandleEventsFunction
    {
      static const std::string name;
    };
    const std::string HandleEventsFunction::name("handleEvents");

    bool lowLevelPoll(WindowWrapperPtr& wrapper, sf::Event& event) noexcept
    {
      // acts like a call to wrapper->getAccess()->pollEvent(event);
      //  but allows me to do some thread safe c++ work
      // explicitly, this involves setting the window to resize the draw
      //  window when it next swaps buffers
      auto windowAccess = wrapper->getAccess();
      bool returnBool = windowAccess->pollEvent(event);
      return returnBool;
    }

    int EventHandler::luaHandleEvents(lua_State* state)
    {
      Lua::SignatureChecker<
        boost::mpl::vector<>::type,
        EventHandler, HandleEventsFunction,
        boost::mpl::vector<WindowWrapperPtr>::type
          > sigChecker(state);

      WindowWrapperPtr& windowWrapper(Lua::get<WindowWrapperPtr>(state, 2));
      lua_settop(state, 1);
      lua_gettable(state, lua_upvalueindex(1));

      sf::Event event;
      int parametersOnStack(0);

      while (lowLevelPoll(windowWrapper, event))
      {
        lua_rawgeti(state, -1, event.type);
        if (lua_isnil(state, -1) != 1)
        {
          // fill this with extra pushes to add parameters to the function call
          // set parametersOnStack too so that I can use the Lua API
          parametersOnStack = 0;

          switch(event.type)
          {
          case sf::Event::Resized:
            lua_pushinteger(state, event.size.width);
            lua_pushinteger(state, event.size.height);
            parametersOnStack = 2;
            break;
          case sf::Event::KeyPressed:
          case sf::Event::KeyReleased:
            lua_pushinteger(state, event.key.code);
            lua_pushboolean(state, event.key.shift);
            lua_pushboolean(state, event.key.alt);
            lua_pushboolean(state, event.key.control);
            lua_pushboolean(state, event.key.system);
            parametersOnStack = 5;
            break;
          case sf::Event::MouseWheelMoved:
            lua_pushinteger(state, event.mouseWheel.delta);
            lua_pushinteger(state, event.mouseWheel.x);
            lua_pushinteger(state, event.mouseWheel.y);
            parametersOnStack = 3;
            break;
          case sf::Event::MouseMoved:
            lua_pushinteger(state, event.mouseMove.x);
            lua_pushinteger(state, event.mouseMove.y);
            parametersOnStack = 2;
            break;
          case sf::Event::MouseButtonPressed:
          case sf::Event::MouseButtonReleased:
            lua_pushinteger(state, event.mouseButton.button);
            lua_pushinteger(state, event.mouseButton.x);
            lua_pushinteger(state, event.mouseButton.y);
            parametersOnStack = 3;
            break;
          default:
            break;
          }

          // allow the error to pass through the function to the caller
          // remember that this stops the event polling
          lua_call(state, parametersOnStack, 0);
        }
        else
        {
          // pop the nil element that was pushed
          // so that it doesn't interfere with the rest of the rawgeti calls
          lua_pop(state, 1);
        }
      }

      return sigChecker.resultCount;
    }
  }
}
