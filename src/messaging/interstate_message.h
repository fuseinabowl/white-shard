#ifndef _MESSAGING_INTERSTATE_MESSAGE_H_
#define _MESSAGING_INTERSTATE_MESSAGE_H_

#include "lua_shard.h"
#include "messaging.h"

namespace WhiteShard
{
  namespace Messaging
  {
    class NoMessageHandlerException : public Lua::MessageException
    {
      const char* what() const throw();
    };

    class Subsystem::InterstateMessage : public Lua::Message
    {
      typedef std::vector<BoxPtr> BoxPtrCollection;
      const Subsystem* parent;
      BoxPtrCollection boxes;
     public:
      InterstateMessage(const Subsystem& parent, BoxPtrCollection&&) noexcept;
      ~InterstateMessage() noexcept;
      void sendMessage(Lua::State&) const throw(Lua::LuaException, NoMessageHandlerException);
    };
  }
}

#endif
