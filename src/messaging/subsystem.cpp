#include "messaging.h"
#include "lua_shard.h"
#include <cassert>
#include <limits>
#include <memory>
#include "native_boxes.h"
#include "interstate_message.h"
#include <deque>
#include <boost/mpl/vector.hpp>

namespace WhiteShard
{
  namespace Lua
  {
    template <>
      const std::string& Lua::LuaTraits<Messaging::SubsystemLuaPtr>::getName() noexcept
    {
      static const std::string name("messaging_subsystem");
      return name;
    }
  }

  namespace Messaging{
    // externally visible strings
    const std::string 
      Subsystem::createState("createThread"),
      Subsystem::sendMessage("sendMessage"),
      Subsystem::killState("kill"),
      Subsystem::getThisState("getThisThread"),
      Subsystem::registerReceiver("setMessageHandler");

    // internal strings
    const std::string
      Subsystem::attachName("messaging");

    Subsystem::Subsystem(Engine& engine):
      engine(&engine)
    {}

    std::string Subsystem::pushSubsystem(Lua::State& stateParam)
    {
      // stop loads of the same cast
      lua_State* state(stateParam);

      Lua::StackChecker checker(state, 1, "Messaging::pushSubsystem");

      // create the metatable
      Lua::generateMetatable<SubsystemLuaPtr>(state, 0, 1);

      // create the subsystem with this metatable
      // then put the metatable back on top
      // we need the subsystem because the create state function needs access to it
      new (Lua::allocate<SubsystemLuaPtr>(state)) SubsystemLuaPtr(this);
      int subsystemIndex = lua_gettop(state);
      lua_getmetatable(state, -1);
      
      // can't use register metatable as it adds a box for a unique_ptr,
      // which causes a compilation error
      // have to do it manually instead
      lua_getfield(state, LUA_REGISTRYINDEX, Registry::WhiteShard.c_str());
      lua_rawgeti(state, -1, Registry::Metatables);
      // add the boxptr metatable - it is inaccessible to normal users
      // so it goes directly in the metatable table rather than the index
      Lua::generateMetatable<BoxPtr>(state);
      lua_setfield(state, -2, Lua::LuaTraits<BoxPtr>::getName().c_str());
      // clean up
      lua_pop(state, 2);

      // now that we have the subsystem, we can add the state ptr
      // as part of the metatable
      Lua::generateMetatable<Lua::StatePtr>(state, 0, 2);

      // register the metatable
      Lua::registerMetatable<Lua::StatePtr>(state);

      // resolving the name again
      lua_createtable(state, 0, 2);

      Subsystem::pushSendMessage(state, subsystemIndex);
      lua_setfield(state, -2, sendMessage.c_str());
      Subsystem::pushKillState(state);
      lua_setfield(state, -2, killState.c_str());

      lua_setfield(state, -2, "__index");

      lua_pop(state, 1);

      // add the functions to index
      lua_createtable(state, 0, 3);

      Subsystem::pushCreateState(state);
      lua_setfield(state, -2, createState.c_str());
      Subsystem::pushGetThisState(state, stateParam.getThisPointer());
      lua_setfield(state, -2, getThisState.c_str());
      Subsystem::pushRegisterReceiver(state);
      lua_setfield(state, -2, registerReceiver.c_str());

      lua_setfield(state, -2, "__index");

      // pop the metatable, so the subsystem is at the top
      lua_pop(state, 1);

      return Subsystem::attachName;
    }
  }
}
