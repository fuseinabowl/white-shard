#ifndef _SYNC_OBJECT_H_
#define _SYNC_OBJECT_H_

#include "window/graphics/openGL.h"
#include <thread>
#include <mutex>
#include <condition_variable>
#include <boost/thread/shared_mutex.hpp>
#include <boost/thread/locks.hpp>

namespace WhiteShard{
  namespace Window{
    namespace Graphics{
      class Subsystem;
      class ReadSyncLock;
      class WriteSyncLock;
      class ContextLock;
      class DestructionCommand;
      typedef std::unique_ptr<DestructionCommand> DestructionCommandPtr;

      class SyncWrapper // not thread safe
      {
        GLsync sync;
        bool active;
        SyncWrapper(GLsync) noexcept;
       public:
        SyncWrapper() noexcept;
        ~SyncWrapper() noexcept;

        SyncWrapper(const SyncWrapper&) = delete;
        SyncWrapper& operator=(const SyncWrapper&) = delete;

        SyncWrapper(SyncWrapper&&) noexcept;
        SyncWrapper& operator=(SyncWrapper&&) noexcept;

        void wait() noexcept;
        void kill() noexcept;
        void reset() noexcept;
        bool isActive() noexcept;

        static SyncWrapper generate() noexcept;
      };

      class SyncObject
      {
        typedef boost::shared_lock<boost::shared_mutex> SharedLock;
        typedef boost::unique_lock<boost::shared_mutex> UniqueLock;

        // need to be mutable because reads still require
        // synchronisation
        mutable SyncWrapper writeSync;
        mutable std::vector<SyncWrapper> readSyncs;
        mutable boost::shared_mutex mutex;
        mutable std::mutex readSyncsAccess;
        Subsystem* subsystem;
#ifndef NDEBUG
        mutable bool destructionCommandSent;
#endif
       public:
        SyncObject(Subsystem&) noexcept;
        virtual ~SyncObject() noexcept;

        friend class ::WhiteShard::Window::Graphics::ReadSyncLock;
        friend class ::WhiteShard::Window::Graphics::WriteSyncLock;
        friend class ::WhiteShard::Window::Graphics::ContextLock;
        friend class ::WhiteShard::Window::Graphics::DestructionCommand;
       protected:
        Subsystem& getSubsystem() noexcept;
        void sendDestructionCommand(DestructionCommandPtr&&) noexcept; // should be called in all inheriting destructors
      };

      class ReadSyncLock
      {
        std::shared_ptr<const SyncObject> syncTarget;
        SyncObject::SharedLock targetLock;

        friend class WriteSyncLock;
       public:
        ReadSyncLock(std::shared_ptr<const SyncObject> syncTarget) noexcept;
        ~ReadSyncLock() noexcept;

        // non-copyable
        ReadSyncLock(const ReadSyncLock&) = delete;
        ReadSyncLock& operator=(const ReadSyncLock&) = delete;
        // moveable
        ReadSyncLock(ReadSyncLock&&) noexcept;
        ReadSyncLock& operator=(ReadSyncLock&&) noexcept;
      };

      class WriteSyncLock
      {
        std::shared_ptr<const SyncObject> syncTarget;
        SyncObject::UniqueLock targetLock;
       public:
        struct Constructor_t{};

        WriteSyncLock(std::shared_ptr<SyncObject> syncTarget) noexcept;
        WriteSyncLock(std::shared_ptr<SyncObject> syncTarget, Constructor_t) noexcept;
        WriteSyncLock(ReadSyncLock&&) noexcept;
        ~WriteSyncLock() noexcept;

        // non-copyable
        WriteSyncLock(const WriteSyncLock&) = delete;
        WriteSyncLock& operator=(const WriteSyncLock&) = delete;
        // moveable
        WriteSyncLock(WriteSyncLock&&) noexcept;
        WriteSyncLock& operator=(WriteSyncLock&&) noexcept;
      };
    }
  }
}

#endif
