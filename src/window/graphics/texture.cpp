#include "texture.h"
#include "lua_shard.h"
#include <SFML/Graphics.hpp>
#include "subsystem.h"
#include "per_context_command.h"

namespace WhiteShard{
  namespace Lua{
    template <>
      const std::string& LuaTraits<WhiteShard::Window::Graphics::TexturePtr>::getName() noexcept
    {
      static const std::string name("texture");
      return name;
    }
  }

  namespace Window{
    namespace Graphics{
      BadFilenameException::BadFilenameException(const std::string& filename) noexcept
      {
        errorString.append("file \"").append(filename).append("\" could not be accessed. Check it exists, the program has permissions to access it and it is a readable format.");
      }

      BadFilenameException::~BadFilenameException() noexcept
      {}

      const char * BadFilenameException::what() noexcept
      {
        return this->errorString.c_str();
      }

      void Texture::setUpParameters(GLenum sWrap, GLenum tWrap, GLenum minificationFilter, GLenum magnificationFilter, GLfloat anisotropicFilter) noexcept
      {
        // texture wrap parameters
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, sWrap);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, tWrap);

        // min/magnification parameters
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, minificationFilter);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, magnificationFilter);

        // this is required on ATI cards in order to call glGenerateMipmaps
        // do this even if the texture isn't using mipmaps initially because
        // the user might set mipmap filtering later
        glEnable(GL_TEXTURE_2D);
        // if the minification filter uses mipmaps, construct them
        if (GL_LINEAR_MIPMAP_NEAREST == minificationFilter ||
            GL_NEAREST_MIPMAP_NEAREST == minificationFilter ||
            GL_LINEAR_MIPMAP_LINEAR == minificationFilter ||
            GL_NEAREST_MIPMAP_LINEAR == minificationFilter)
        {
          glGenerateMipmap(GL_TEXTURE_2D);
        }

        if (anisotropicFilter >= 2.0f)
        {
          glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, anisotropicFilter);
        }
      }

      Texture::Texture(Subsystem& subsystem, ContextLock& contextLock,
          const std::string& fileName, 
          GLenum sWrap, GLenum tWrap, GLenum minificationFilter, GLenum magnificationFilter, GLfloat anisotropicFilter) throw (BadFilenameException) :
        SyncObject(subsystem),
        texName(0)
      {
        sf::Image imageLoader;
        if (!imageLoader.loadFromFile(fileName))
        {
          // image failed to load
          throw BadFilenameException(fileName);
        }

        auto imageSize = imageLoader.getSize();
        this->width = imageSize.x;
        this->height = imageSize.y;

        glGenTextures(1, &this->texName);
        auto texUnits = contextLock.cacheTextures({this});
        glActiveTexture(GL_TEXTURE0 + texUnits.front());

        // although openGL is supposed to support non-power of two textures now
        // it tends to be patchy. The engine supports NPOT textures as much as it can
        // given the hardware it is currently running on
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, imageSize.x, imageSize.y, 0, GL_RGBA, GL_UNSIGNED_INT_8_8_8_8_REV, imageLoader.getPixelsPtr());

        Texture::setUpParameters(sWrap, tWrap, minificationFilter, magnificationFilter, anisotropicFilter);
      }

      Texture::Texture(Subsystem& subsystem, ContextLock& contextLock,
          GLsizei width, GLsizei height,
          GLenum sWrap, GLenum tWrap, GLenum minificationFilter, GLenum magnificationFilter, GLfloat anisotropicFilter) noexcept :
        SyncObject(subsystem),
        texName(0),
        width(width), height(height)
      {
        glGenTextures(1, &this->texName);
        auto texUnits = contextLock.cacheTextures({this});
        glActiveTexture(GL_TEXTURE0 + texUnits.front());

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_INT_8_8_8_8, nullptr);

        Texture::setUpParameters(sWrap, tWrap, minificationFilter, magnificationFilter, anisotropicFilter);
      }

      // Destruction command and texture destructor

      class TextureDestructionCommand : public DestructionCommand
      {
        GLuint texName;
       public:
        TextureDestructionCommand(Texture&, GLuint textureName) noexcept;
        ~TextureDestructionCommand() noexcept;

        void performDestruction() noexcept;
      };

      TextureDestructionCommand::TextureDestructionCommand(Texture& texture, GLuint texName) noexcept :
        DestructionCommand(texture),
        texName(texName)
      {
      }

      TextureDestructionCommand::~TextureDestructionCommand() noexcept
      {
      }

      void TextureDestructionCommand::performDestruction() noexcept
      {
        glDeleteTextures(1, &this->texName);
      }

      Texture::~Texture() noexcept
      {
        this->getSubsystem().freeTexture(this);
        this->sendDestructionCommand(DestructionCommandPtr(new TextureDestructionCommand(*this, this->texName)));
      }

      // user functions

      GLuint Texture::getTexName() const noexcept
      {
        return this->texName;
      }

      std::pair<GLsizei, GLsizei> Texture::getSize() const noexcept
      {
        return std::make_pair(this->width, this->height);
      }
    }
  }
}
