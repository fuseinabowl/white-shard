#ifndef _SUBSYSTEM_DECLARATION_
#define _SUBSYSTEM_DECLARATION_

#include <memory>
#include <vector>

namespace WhiteShard
{
  class Subsystem;
  typedef std::unique_ptr<Subsystem> SubsystemPtr;
  typedef std::vector<SubsystemPtr> SubsystemPtrCollection;
  typedef std::vector<Subsystem*> SubsystemRawPtrCollection;
}

#endif
