#ifndef _PER_CONTEXT_COMMAND_H_
#define _PER_CONTEXT_COMMAND_H_

namespace WhiteShard{
  namespace Window{
    namespace Graphics{
      class ContextLock;

      class PerContextCommand // abstract
      {
       public:
        PerContextCommand() noexcept;
        ~PerContextCommand() noexcept;

        virtual void operator()(ContextLock&) const noexcept = 0;
      };
    }
  }
}

#endif
