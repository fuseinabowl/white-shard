#include "uniform_upload_buffer.h"
#include "context_wrapper.h"

namespace WhiteShard{
  namespace Window{
    namespace Graphics{
      void UniformUploadBuffer::doBufferedUploads(ContextLock& contextLock) throw (UnsupportedNumberOfTexturesException)
      {
        assert(this->texturesToUpload.size() == this->uploadUniformTargets.size());
        auto textureUnits = contextLock.cacheTextures(this->texturesToUpload);
        assert(textureUnits.size() == this->texturesToUpload.size());

        auto textureUnitIter = textureUnits.begin();
        for (auto currentUniform : this->uploadUniformTargets)
        {
          assert(textureUnitIter != textureUnits.end());
          glUniform1i(currentUniform, *(textureUnitIter++));
        }
      }

      void UniformUploadBuffer::reset() noexcept
      {
        this->texturesToUpload.clear();
        this->uploadUniformTargets.clear();
      }

      void UniformUploadBuffer::pushTexture(const std::pair<const Texture*, GLuint>& texUpload) noexcept
      {
        this->texturesToUpload.push_back(texUpload.first);
        this->uploadUniformTargets.push_back(texUpload.second);
      }
    }
  }
}
