#include "subsystem.h"
#include "graphics/subsystem.h"
#include "event_handler.h"
#include "window_wrapper.h"
#include "lua.h"
#include <boost/mpl/vector.hpp>

namespace WhiteShard{
  namespace Lua{
    template <>
      const std::string& LuaTraits<Window::SubsystemLuaPtr>::getName() noexcept
    {
      static const std::string name("window_subsystem");
      return name;
    }

    template <>
      const std::string& LuaTraits<sf::VideoMode>::getName() noexcept
    {
      static const std::string name("fullscreen_video_mode");
      return name;
    }
  }

  namespace Window{
    const std::string subsystemAttachName("window");

    Subsystem::Subsystem(int numberOfContexts) throw (Graphics::SubsystemConstructorException) try :
      graphicsSubsystem(numberOfContexts)
    {
    }
    catch (Graphics::GLEWException& ex)
    {
      std::cout << ex.what() << std::endl;
      throw ex;
    }

    Subsystem::~Subsystem() noexcept
    {}

    std::string Subsystem::pushSubsystem(Lua::State& luaState) noexcept
    {
      lua_State* state(luaState);

      Lua::StackChecker checker(state, 1, "Window::pushSubsystem");

      // create subsystem metatable
      Lua::generateMetatable<SubsystemLuaPtr>(state);

      // index table
      lua_createtable(state, 0, 2);

      // expose graphics subsystem
      this->graphicsSubsystem.pushSubsystem(state);
      lua_setfield(state, -2, "graphics");

      // create and register window metatable
      WindowWrapper::pushMetatable(state);
      lua_pop(state, 1);

      // function to create a event handler
      EventHandler::pushCreateEventHandler(state);
      lua_setfield(state, -2, "createEventHandler");

      Subsystem::pushCreateWindow(state);
      lua_setfield(state, -2, CreateWindowFunction::name.c_str());
      
      Subsystem::pushCreateFullscreenWindow(state);
      lua_setfield(state, -2, CreateFullscreenWindowFunction::name.c_str());

      Subsystem::pushFullscreenModes(state);
      lua_setfield(state, -2, "fullscreenModes");

      // set the index field
      lua_setfield(state, -2, "__index");

      // allocate a pointer to this object
      new (Lua::allocate<SubsystemLuaPtr>(state)) SubsystemLuaPtr(this);

      return subsystemAttachName;
    }

    namespace VideoMode
    {
      struct GetWidthFunction
      {
        static const std::string name;
      };
      const std::string GetWidthFunction::name("getWidth");

      int luaGetWidth(lua_State* state)
      {
        Lua::SignatureChecker<
          boost::mpl::vector<Lua::NativeTypeWrapper<LUA_TNUMBER> >,
          sf::VideoMode, GetWidthFunction,
          boost::mpl::vector<>
            > sigChecker(state);
        lua_pushnumber(state, Lua::get<sf::VideoMode>(state, 1).width);
        return sigChecker.resultCount;
      }

      struct GetHeightFunction
      {
        static const std::string name;
      };
      const std::string GetHeightFunction::name("getHeight");

      int luaGetHeight(lua_State* state)
      {
        Lua::SignatureChecker<
          boost::mpl::vector<Lua::NativeTypeWrapper<LUA_TNUMBER> >,
          sf::VideoMode, GetHeightFunction,
          boost::mpl::vector<>
            > sigChecker(state);
        lua_pushnumber(state, Lua::get<sf::VideoMode>(state, 1).height);
        return sigChecker.resultCount;
      }

      struct GetBppFunction
      {
        static const std::string name;
      };
      const std::string GetBppFunction::name("getBpp");

      int luaGetBpp(lua_State* state)
      {
        Lua::SignatureChecker<
          boost::mpl::vector<Lua::NativeTypeWrapper<LUA_TNUMBER> >,
          sf::VideoMode, GetBppFunction,
          boost::mpl::vector<>
            > sigChecker(state);
        lua_pushnumber(state, Lua::get<sf::VideoMode>(state, 1).bitsPerPixel);
        return sigChecker.resultCount;
      }
      
      void pushMetatable(lua_State* state)
      {
        Lua::StackChecker checker(state, 1, "push VideoMode metatable");

        Lua::generateMetatable<sf::VideoMode>(state);

        lua_createtable(state, 0, 3);

        lua_pushcfunction(state, VideoMode::luaGetWidth);
        lua_setfield(state, -2, VideoMode::GetWidthFunction::name.c_str());

        lua_pushcfunction(state, VideoMode::luaGetHeight);
        lua_setfield(state, -2, VideoMode::GetHeightFunction::name.c_str());

        lua_pushcfunction(state, VideoMode::luaGetBpp);
        lua_setfield(state, -2, VideoMode::GetBppFunction::name.c_str());

        lua_setfield(state, -2, "__index");
      }
    }

    void Subsystem::pushFullscreenModes(lua_State* state)
    {
      Lua::StackChecker checker(state, 1, "pushFullscreenModes");

      std::vector<sf::VideoMode> fullScreenModes = sf::VideoMode::getFullscreenModes();

      lua_createtable(state, fullScreenModes.size(), 0);
      // create the metatable
      VideoMode::pushMetatable(state);

      unsigned int currentModeIndex = 1;

      for (sf::VideoMode& currentMode : fullScreenModes)
      {
        lua_pushvalue(state, -1);
        new (Lua::allocate<sf::VideoMode>(state)) sf::VideoMode(currentMode);
        lua_rawseti(state, -3, currentModeIndex++);
      }

      // pop the metatable
      lua_pop(state, 1);
    }
  }
}
