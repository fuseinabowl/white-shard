#ifndef _LUA_H_
#define _LUA_H_

#include <string>
#include <memory>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <deque>
#include <vector>

#include "lua/include.h"
#include "engine/subsystem_declaration.h"
#include "temporary_object.h"
#include "lua/exception.h"

namespace WhiteShard{

  namespace Messaging{
    template <class T>
      class LoadFileMessage;
  }

  class Engine;
  // keep the original lua_State* name because it will be manipulated through the similarly styled
  // lua_<procedure name> lua calls.

  namespace Lua{
    class Message;
    typedef std::unique_ptr<Message> MessagePtr;

    class State;
    typedef std::shared_ptr<State> StatePtr;

    // require this class as an intermediary to prevent messages being sent to invalid memory
    // addresses, potentially causing segfaults
    class State{
      class InitialisationMessage;
     public:
      typedef int precedence;
     private:
      enum {
        PRECEDENCE_UNKNOWN = -1,
        PRECEDENCE_EMPTY = 0
      };

      lua_State* state;
      std::mutex messageMutex; // enables thread safety when accessing the messageQueue
      std::deque<MessagePtr> messageQueue;
      typedef std::lock_guard<std::mutex> QueueLock;

      State(const State&)            = delete;
      State& operator=(const State&) = delete;
      State(State&&)            = delete;
      State& operator=(State&&) = delete;

      std::weak_ptr<State> thisPointer;

      // Handle can construct State objects
      friend class Handle;
      State(StatePtr&, Engine&, precedence);

      friend class ::WhiteShard::Engine;
      void closeState();

      TemporaryCollection* temporaryObjects;

      // used on the global metatable
      // and the subsystems table
      static int preventEngineOverwrite(lua_State*);
      static int preventSubsystemOverwrite(lua_State*);
     public:
      ~State();

      // messaging things here
      void addMessage(MessagePtr&); // transfers ownership to this class
      void addMessage(MessagePtr&&);
      void doMessage() throw (Lua::MessageException);

      operator lua_State*(); // return internal state - dangerous, use only when you know you have a unique lock on the state

      precedence getPrecedence() const;
      void resetPrecedence() noexcept;

      StatePtr getThisPointer() const;

      // file load exceptions
      class LoadFileException;
      class FileAccessError;
      class SyntaxError;
      class MemoryError;

      // internal for initialisation messaging
      void setTemporaryObjects(TemporaryCollection*);
    };

    class Message
    {
     public:
      virtual ~Message() noexcept;
      // here you can do what you want, whether it be sending an actual function call or just populating the stack a bit
      // this allows daisy chaining of message templates, so one template can populate the stack while another manipulates the stack into a callable format and calls it
      virtual void sendMessage(State& recipient) const throw (Lua::MessageException) = 0;
    };

    // gets a c++ string from a lua index
    std::string toString(lua_State*, int index);
  }
}

#include "types.h"
#endif
