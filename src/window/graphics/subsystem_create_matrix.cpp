#include "subsystem.h"
#include <array>
#include "lua_shard.h"
#include <boost/mpl/vector.hpp>
#include "subsystem_functions.h"
#include "matrix.h"

namespace WhiteShard{
  namespace Window{
    namespace Graphics{
      // subsystem functions

      // templated metatable creator
      template <int x, int y>
      inline
        void pushMatrixMetatable(lua_State* state)
      {
        Lua::StackChecker checker(state, 1, "pushMatrixMetatable");
        Lua::generateMetatable<Matrix<x,y> >(state, 0, 5);
        Lua::registerMetatable<Matrix<x,y> >(state);

        // push operators
        Matrix<x,y>::pushAdd(state);
        lua_setfield(state, -2, "__add");

        Matrix<x,y>::pushSubtract(state);
        lua_setfield(state, -2, "__sub");

        // __index table
        lua_createtable(state, 0, 2);

        Matrix<x,y>::pushToTable(state);
        lua_setfield(state, -2, MatrixToTableFunction::name.c_str());

        lua_setfield(state, -2, "__index");

        assert(lua_type(state, -1) == LUA_TTABLE);
      }

      template <int x, int y>
        struct setInverseFunction
      {
        static inline void set(lua_State*)
        {
          // non-square matrices never have an inverse
        }
      };

      template <int x>
        struct setInverseFunction<x,x>
      {
        static inline void set(lua_State* state)
        {
          // square matrices might have an inverse
          Matrix<x,x>::pushInvert(state);
          lua_setfield(state, -2, MatrixInvertFunction::name.c_str());
        }
      };

      template <int x, int y>
        void finaliseMatrixMetatable(lua_State* state)
      {
        Lua::StackChecker checker(state, 0, "finaliseMatrix");

        Lua::getMetatable<Matrix<x,y> >(state);

        Matrix<x,y>::pushMultiply(state);
        lua_setfield(state, -2, "__mul");

        lua_getfield(state, -1, "__index");

        Matrix<x,y>::pushTranspose(state);
        lua_setfield(state, -2, MatrixTransposeFunction::name.c_str());

        setInverseFunction<x,y>::set(state);

        lua_pop(state, 2);
      }

      template <>
        const std::string CreateMatrixFunction::name("createMatrix");

      void Subsystem::pushCreateMatrix(lua_State* state)
      {
        Lua::StackChecker checker(state, 1, "Subsystem::pushCreateMatrix");

        // create all the metatables (1x1 up to 4x4)
        pushMatrixMetatable<1,1>(state);
        pushMatrixMetatable<1,2>(state);
        pushMatrixMetatable<1,3>(state);
        pushMatrixMetatable<1,4>(state);
        pushMatrixMetatable<2,1>(state);
        pushMatrixMetatable<2,2>(state);
        pushMatrixMetatable<2,3>(state);
        pushMatrixMetatable<2,4>(state);
        pushMatrixMetatable<3,1>(state);
        pushMatrixMetatable<3,2>(state);
        pushMatrixMetatable<3,3>(state);
        pushMatrixMetatable<3,4>(state);
        pushMatrixMetatable<4,1>(state);
        pushMatrixMetatable<4,2>(state);
        pushMatrixMetatable<4,3>(state);
        pushMatrixMetatable<4,4>(state);
        
        // need to finalise them to add the transpose and inverse functions
        finaliseMatrixMetatable<1,1>(state);
        finaliseMatrixMetatable<1,2>(state);
        finaliseMatrixMetatable<1,3>(state);
        finaliseMatrixMetatable<1,4>(state);
        finaliseMatrixMetatable<2,1>(state);
        finaliseMatrixMetatable<2,2>(state);
        finaliseMatrixMetatable<2,3>(state);
        finaliseMatrixMetatable<2,4>(state);
        finaliseMatrixMetatable<3,1>(state);
        finaliseMatrixMetatable<3,2>(state);
        finaliseMatrixMetatable<3,3>(state);
        finaliseMatrixMetatable<3,4>(state);
        finaliseMatrixMetatable<4,1>(state);
        finaliseMatrixMetatable<4,2>(state);
        finaliseMatrixMetatable<4,3>(state);
        finaliseMatrixMetatable<4,4>(state);
        lua_pushcclosure(state, Subsystem::luaCreateMatrix, 16);
      }

      template <unsigned char width, unsigned char height>
        void pushMatrix(lua_State* state)
      {
        // get the metatable from the upvalues
        lua_pushnil(state);
        lua_copy(state, lua_upvalueindex((width - 1) * 4 + height), -1);
        Matrix<width, height>& matrix = *(new (Lua::allocate<Matrix<width, height> >(state)) Matrix<width, height>);

        unsigned char currentX = 0;
        for (unsigned char currentY = 0; currentY < height; ++currentY)
        {
          lua_rawgeti(state, 2, currentY + 1);
          for (currentX = 0; currentX < width; ++currentX)
          {
            lua_rawgeti(state, -1, currentX + 1);
            if (lua_isnumber(state, -1))
            {
              (*matrix)(currentX, currentY) = lua_tonumber(state, -1);
            }
            else
            {
              break;
            }
            lua_pop(state, 1);
          }
          lua_pop(state, 1);
        }
      }

      template <unsigned char rowCount>
        void createMatrixWithRowCount(lua_State* state)
      {
        // find the maximum number of columns in the row count
        int currentLength, maxLength = 1;

        for (unsigned char currentRow = 0; currentRow < rowCount; ++currentRow)
        {
          lua_rawgeti(state, 2, currentRow + 1);
          lua_len(state, -1);
          currentLength = lua_tonumber(state, -1);
          lua_pop(state, 2);

          if (currentLength > maxLength)
          {
            maxLength = currentLength;
          }
        }

        switch (maxLength)
        {
        case 1:
          pushMatrix<1, rowCount>(state);
          break;
        case 2:
          pushMatrix<2, rowCount>(state);
          break;
        case 3:
          pushMatrix<3, rowCount>(state);
          break;
        default:
          pushMatrix<4, rowCount>(state);
          break;
        }
      }

      int Subsystem::luaCreateMatrix(lua_State* state)
      {
        // we can't use a matrix here because we might return any of the templated matrices
        Lua::SignatureChecker<
          boost::mpl::vector<>,
          Subsystem*, CreateMatrixFunction,
          boost::mpl::vector<std::vector<std::vector<Lua::NativeTypeWrapper<LUA_TNUMBER> > > >
            > sigChecker(state);

        lua_len(state, 2);
        unsigned int rows = lua_tonumber(state, -1);
        lua_pop(state, 1);

        switch (rows)
        {
        case 0:
          lua_pushnil(state);
          lua_copy(state, lua_upvalueindex(1), -1);
          new (Lua::allocate<Matrix<1,1> >(state)) Matrix<1,1>;
        case 1:
          createMatrixWithRowCount<1>(state);
          break;
        case 2:
          createMatrixWithRowCount<2>(state);
          break;
        case 3:
          createMatrixWithRowCount<3>(state);
          break;
        default:
          createMatrixWithRowCount<4>(state);
          break;
        }

        return 1;
      }
    }
  }
}
