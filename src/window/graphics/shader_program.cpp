#include "shader_program.h"
#include "shader.h"
#include "subsystem.h"
#include <cassert>

namespace WhiteShard{
  namespace Window{
    namespace Graphics{
      class ProgramDestructionCommand : public DestructionCommand
      {
        GLuint programName;
       public:
        ProgramDestructionCommand(SyncObject&, GLuint) noexcept;
        ~ProgramDestructionCommand() noexcept;

        void performDestruction() noexcept;
      };

      ProgramDestructionCommand::ProgramDestructionCommand(SyncObject& syncObject, GLuint programName) noexcept :
        DestructionCommand(syncObject),
        programName(programName)
      {}

      ProgramDestructionCommand::~ProgramDestructionCommand() noexcept
      {}

      void ProgramDestructionCommand::performDestruction() noexcept
      {
        glDeleteProgram(this->programName);
      }

      ShaderProgram::ShaderProgram(ContextLock& contextLock, Subsystem& subsystem, const std::vector<ShaderPtr>& shaders, std::string& infoLog) throw (ShaderLinkException) :
        SyncObject(subsystem),
        programName(glCreateProgram())
      {
        assert(this->programName != 0);

        for (const ShaderPtr& currentShader : shaders)
        {
          Shader::attachToProgram(contextLock, currentShader, this->programName);
        }

        glLinkProgram(this->programName);

        // get the info log
        GLint infoLogLength(0);
        glGetProgramiv(this->programName, GL_INFO_LOG_LENGTH, &infoLogLength);

        std::vector<GLchar> infoLogVector(infoLogLength);
        glGetProgramInfoLog(this->programName, infoLogLength, nullptr, &infoLogVector.front());
        infoLog = std::string(&infoLogVector.front());

        // get the link status
        GLint linkStatus(0);
        glGetProgramiv(this->programName, GL_LINK_STATUS, &linkStatus);

        if (GL_FALSE == linkStatus)
        {
          glDeleteProgram(this->programName);
          throw ShaderLinkException();
        }

        // populate the uniform map
        GLint uniformCount(0);
        glGetProgramiv(this->programName, GL_ACTIVE_UNIFORM_MAX_LENGTH, &uniformCount);
        std::vector<GLchar> currentName(uniformCount);
        glGetProgramiv(this->programName, GL_ACTIVE_UNIFORMS, &uniformCount);

        this->uniformMap.reserve(uniformCount);

        GLenum dataType;
        GLint dataSize;
        for (GLint currentUniform = 0; currentUniform < uniformCount; ++currentUniform)
        {
          glGetActiveUniform(this->programName, currentUniform, currentName.size(), nullptr,&dataSize, &dataType, &currentName.front());

          this->uniformMap.insert({
            std::string(&currentName.front()), 
            Uniform(dataType, glGetUniformLocation(this->programName, &currentName.front()))
              });
        }
      }

      ShaderProgram::~ShaderProgram() noexcept
      {
        this->sendDestructionCommand(DestructionCommandPtr(new ProgramDestructionCommand(*this, this->programName)));
      }
    }
  }
}
