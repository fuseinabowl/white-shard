#include "interstate_message.h"

namespace WhiteShard
{
  namespace Messaging
  {
    const char* NoMessageHandlerException::what() const throw()
    {
      return "message was sent to a state with no message handler!";
    }
  }
}
