#ifndef _ENGINE_H_
#define _ENGINE_H_

#include <string>
#include "lua_shard.h"
#include "messaging/exceptions.h"
#include "subsystem_declaration.h"
#include <thread>
#include <mutex>
#include <memory>
#include <vector>
#include <queue>
#include <set>
#include "lua/exception.h"

#include <SFML/Window.hpp> // required for the clock

/*
   the engine acts as the main point of communication between the threads
   it holds the job pool, so threads come here and ask for their next job
   jobs that are held by the manager are guaranteed not to be in use, so we don't need to lock mutexes when accessing them
   the engine itself is thread safe
   some methods may yield your thread for some time if no states are available for execution (but they will always return valid results)
   to allieviate this, try to increase potential concurrency in the state programming
 */

namespace WhiteShard{
  class SubsystemFactory;
  class SignalHandler; // forward declaration for file "signal.h"

  class Engine{
   private:
    class ThreadFunctor;

    // state collections
    typedef std::vector<Lua::Handle> HandleCollection;
    typedef std::priority_queue<Lua::Handle> HandleQueue;

    sf::Clock timer; 

    std::mutex handleAccess;
    std::condition_variable handleCondition; // if the collection is empty, wait on this. Signal it when you insert elements.
    HandleCollection executableHandles; // all the handles waiting to run
    std::set<Lua::StatePtr> runningStates; // all of the states that are either running or waiting to be run. If this is empty, the engine will close
    SubsystemPtrCollection subsystems;

    // constructor auxiliary methods
    friend class CreateFactoriesMessage;
    typedef int threadCountType;
    std::tuple<std::string, threadCountType> runInitScript(const std::string& fileName) throw(Lua::MessageException);
    void populateSubsystemFactories(lua_State*);

   public:
    struct NoRunningStatesException : public std::exception
    {
      const char* what() const throw();
    };

    // inherently thread safe
    // opens the passed string as the init object
    // and attaches itself to the signal handler for interrupting
    Engine(const std::string&) throw (Lua::MessageException);
    ~Engine() noexcept;

    // mutex prevents copy and move,
    // added dereference for mutex is pointless when Engine has no requirement to move
    Engine(const Engine&)            = delete;
    Engine& operator=(const Engine&) = delete;
    Engine(Engine&&)            = delete;
    Engine& operator=(Engine&&) = delete;

    // enforce thread safety in these
    Lua::Handle getHandle() throw (NoRunningStatesException);
    void returnHandle(Lua::Handle&&);
    const Lua::StatePtr& newHandle(Lua::Handle&&);

    void killHandle(const Lua::State&);

    void attachToState(Lua::State&);
    static int luaGetTime(lua_State*);
    static int luaSetPriorityFunction(lua_State*);
    static int luaStopThread(lua_State*);

    void deactivate();
  };

  class Engine::ThreadFunctor
  {
    Engine* engine;
   public:
    ThreadFunctor(Engine&);
    void operator()();
  };


  // inherit from this to allow use as a subsystem
  class Subsystem
  {
   public:
    virtual ~Subsystem(){};

    // should add a single element onto the stack and returns the desired access name
    // for the subsystem
    virtual std::string pushSubsystem(Lua::State&) = 0;
  };

  class SubsystemFactory
  {
   public:
    virtual ~SubsystemFactory(){};
  };
}

#endif
