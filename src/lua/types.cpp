#include "state.h"

namespace WhiteShard{
  namespace Lua{
    std::string bracketWrap(const std::string& inner)
    {
      std::stringstream total;
      total
        << "[" << inner << "]";
      return total.str();
    }

    std::string angleBracketWrap(const std::string& inner)
    {
      std::stringstream total;
      total
        << "<" << inner << ">";
      return total.str();
    }

    std::string arrowWrap(const std::string& key, const std::string& value)
    {
      std::stringstream total;
      total
        << "<" << key << " -> " << value << ">";
      return total.str();
    }
  }
}
