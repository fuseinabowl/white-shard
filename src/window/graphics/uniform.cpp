#include "uniform.h"
#include "lua_shard.h"
#include "texture.h"
#include <csignal>
#include "matrix.h"

namespace WhiteShard{
  namespace Lua{
    template <>
      const std::string& LuaTraits<Window::Graphics::Uniform>::getName() noexcept
    {
      static const std::string name("uniform");
      return name;
    }
  }

  namespace Window{
    namespace Graphics{
      // exception definition
      UnknownDataTypeException::~UnknownDataTypeException() noexcept
      {}

      const char* UnknownDataTypeException::what() noexcept
      {
        static const char* what("A uniform was created with an unknown data type");
        return what;
      }

      // uniform definition
      Uniform::Uniform() noexcept :
        dataType(GL_NONE), uniformLocation(-1)
      {
      }

      Uniform::Uniform(GLenum dataType, GLuint uniformLocation) throw (UnknownDataTypeException) :
        dataType(dataType), uniformLocation(uniformLocation)
      {
        // ensure that the data type is recognised
        switch(this->dataType)
        {
        // put recognised data types here
        case GL_FLOAT:
        case GL_FLOAT_VEC2:
        case GL_FLOAT_VEC3:
        case GL_FLOAT_VEC4:
        case GL_FLOAT_MAT2:
        case GL_FLOAT_MAT2x3:
        case GL_FLOAT_MAT2x4:
        case GL_FLOAT_MAT3x2:
        case GL_FLOAT_MAT3:
        case GL_FLOAT_MAT3x4:
        case GL_FLOAT_MAT4x2:
        case GL_FLOAT_MAT4x3:
        case GL_FLOAT_MAT4:
        case GL_SAMPLER_2D:
          break;
        default:
          throw UnknownDataTypeException();
          break;
        }
      }

      Uniform::~Uniform() noexcept
      {}

      // upload auxilary functions

      void upload1f(lua_State* state, GLuint uniformLocation) throw (Lua::BadTypenameException)
      {
        Lua::check<Matrix<1,1> >(state, lua_gettop(state));
        glUniform1fv(uniformLocation, 1, Lua::get<Matrix<1,1> >(state, lua_gettop(state))->memptr());
      }

      void upload2f(lua_State* state, GLuint uniformLocation) throw (Lua::BadTypenameException)
      {
        Lua::check<Matrix<2,1> >(state, lua_gettop(state));
        glUniform2fv(uniformLocation, 1, Lua::get<Matrix<2,1> >(state, lua_gettop(state))->memptr());
      }

      void upload3f(lua_State* state, GLuint uniformLocation) throw (Lua::BadTypenameException)
      {
        Lua::check<Matrix<3,1> >(state, lua_gettop(state));
        glUniform3fv(uniformLocation, 1, Lua::get<Matrix<3,1> >(state, lua_gettop(state))->memptr());
      }

      void upload4f(lua_State* state, GLuint uniformLocation) throw (Lua::BadTypenameException)
      {
        Lua::check<Matrix<4,1> >(state, lua_gettop(state));
        glUniform4fv(uniformLocation, 1, Lua::get<Matrix<4,1> >(state, lua_gettop(state))->memptr());
      }

      template <int x, int y>
        void internalUploadMat(GLuint uniformLocation, GLfloat* memptr) noexcept;
#define specialise(width,height) \
      template <> inline \
        void internalUploadMat<width,height>(GLuint uniformLocation, GLfloat* memptr) noexcept\
      {\
        glUniformMatrix ## width ## x ## height ## fv(uniformLocation, 1, GL_FALSE, memptr);\
      }
#define specialiseSquare(size) \
      template <> inline \
        void internalUploadMat<size,size>(GLuint uniformLocation, GLfloat* memptr) noexcept\
      {\
        glUniformMatrix ## size ## fv(uniformLocation, 1, GL_FALSE, memptr);\
      }
      specialiseSquare(2);
      specialise(2,3);
      specialise(2,4);
      specialise(3,2);
      specialiseSquare(3);
      specialise(3,4);
      specialise(4,2);
      specialise(4,3);
      specialiseSquare(4);
#undef specialise
#undef specialiseSquare

      template <int x, int y>
        void uploadMat(lua_State* state, GLuint uniformLocation) throw (Lua::BadTypenameException)
      {
        Lua::check<Matrix<x,y> >(state, lua_gettop(state));
        internalUploadMat<x,y>(uniformLocation, Lua::get<Matrix<x,y> >(state, lua_gettop(state))->memptr());
      }

      void upload2dTexture(lua_State* state, GLuint uniformLocation, UniformUploadBuffer& buffer) throw (Lua::BadTypenameException)
      {
        Lua::check<TexturePtr>(state, lua_gettop(state));

        buffer.pushTexture(std::make_pair(Lua::get<TexturePtr>(state, lua_gettop(state)).get(), uniformLocation));
      }

      void Uniform::upload(lua_State* state, UniformUploadBuffer& buffer) throw (Lua::BadTypenameException)
      {
        switch(this->dataType)
        {
        case GL_FLOAT:
          upload1f(state, this->uniformLocation);
          break;
        case GL_FLOAT_VEC2:
          upload2f(state, this->uniformLocation);
          break;
        case GL_FLOAT_VEC3:
          upload3f(state, this->uniformLocation);
          break;
        case GL_FLOAT_VEC4:
          upload4f(state, this->uniformLocation);
          break;
        case GL_FLOAT_MAT2:
          uploadMat<2,2>(state, this->uniformLocation);
          break;
        case GL_FLOAT_MAT2x3:
          uploadMat<2,3>(state, this->uniformLocation);
          break;
        case GL_FLOAT_MAT2x4:
          uploadMat<2,4>(state, this->uniformLocation);
          break;
        case GL_FLOAT_MAT3x2:
          uploadMat<3,2>(state, this->uniformLocation);
          break;
        case GL_FLOAT_MAT3:
          uploadMat<3,3>(state, this->uniformLocation);
          break;
        case GL_FLOAT_MAT3x4:
          uploadMat<3,4>(state, this->uniformLocation);
          break;
        case GL_FLOAT_MAT4x2:
          uploadMat<4,2>(state, this->uniformLocation);
          break;
        case GL_FLOAT_MAT4x3:
          uploadMat<4,3>(state, this->uniformLocation);
          break;
        case GL_FLOAT_MAT4:
          uploadMat<4,4>(state, this->uniformLocation);
          break;
        case GL_SAMPLER_2D:
          upload2dTexture(state, this->uniformLocation, buffer);
          break;
        default: // should have been caught in the constructor
          std::cerr << "program error in \"uniform.cpp\" - bad upload type in call to Uniform::upload()" << std::endl;
          raise(SIGABRT);
          break;
        }
      }

      void Uniform::pushMetatable(lua_State* state) noexcept
      {
        Lua::generateMetatable<Uniform>(state, 1, 0);
        Lua::registerMetatable<Uniform>(state);
      }
    }
  }
}
