#include "initialisation_message.h"
#include "engine.h"
#include <boost/mpl/vector.hpp>

namespace WhiteShard{
  namespace Lua{
    int luaGetType(lua_State* state)
    {
      // use Lua::getTypename to find the type of the first parameter
      lua_pushstring(state, Lua::getTypename(state, 1).c_str());
     
      return 1;
    }

    State::InitialisationMessage::InitialisationMessage(Engine& parentEngine, State::precedence initialPrecedence) :
      engine(&parentEngine), initialPrecedence(initialPrecedence)
    {}

    void State::InitialisationMessage::sendMessage(State& recipient) const throw(LuaException)
    {
      lua_State* state(recipient);
      Lua::StackChecker checker(state, 0, "State::InitialisationMessage::sendMessage");

      {
        Lua::StackChecker pushTypeChecker(state, 0, "State::InitialisationMessage::sendMessage - push type function");

        // push luaGetType
        lua_pushglobaltable(state);
        lua_pushcfunction(state, luaGetType);
        lua_setfield(state, -2, "type");
        lua_pop(state, 1);
      }

      {
        Lua::StackChecker registryChecker(state, 0, "State::InitialisationMessage::sendMessage - registry set up");
        // first set up the registry
        lua_createtable(state, Registry::MaxRegistryIndex, 0);

        // need to attach it early so the engine push works
        lua_pushnil(state);
        lua_copy(state, -2, -1);
        lua_setfield(state, LUA_REGISTRYINDEX, Registry::WhiteShard.c_str());

        // now can treat it normally
        lua_createtable(state, 0, 0);
        lua_rawseti(state, -2, Registry::Metatables);

        lua_createtable(state, 0, 0);
        lua_rawseti(state, -2, Registry::MessagingLoadCallbacks);

        lua_pushcfunction(state, Lua::nullOperation);
        lua_rawseti(state, -2, Registry::MessagingReceiver);

        lua_pushinteger(state, this->initialPrecedence);
        lua_pushcclosure(state, InitialisationMessage::getPrecedence, 1);
        lua_rawseti(state, -2, Registry::PriorityFunction);

        Lua::generateMetatable<Lua::TemporaryCollection>(state);
        recipient.setTemporaryObjects(new (Lua::allocate<Lua::TemporaryCollection>(state)) Lua::TemporaryCollection());
        lua_rawseti(state, -2, Registry::TemporaryCollection);

        lua_createtable(state, 0, 0);
        lua_rawseti(state, -2, Registry::PointerToUserdataMap);

        lua_pop(state, 1);

        this->engine->attachToState(recipient);
      }

      {
        Lua::StackChecker subsystemChecker(state, 0, "State::InitialisationMessage::sendMessage - subsystem installation");

        // then load all the subsystems into the state
        lua_pushglobaltable(state);
        lua_createtable(state, 0, 2); // metatable
        lua_createtable(state, 0, 1); // __index table

        // get the engine
        lua_getfield(state, LUA_REGISTRYINDEX, Registry::WhiteShard.c_str());
        lua_rawgeti(state, -1, Registry::Engine);
        lua_replace(state, -2);

        lua_setfield(state, -2, "WhiteShard");
        lua_setfield(state, -2, "__index");

        lua_pushcfunction(state, State::preventEngineOverwrite);
        lua_setfield(state, -2, "__newindex");

        lua_setmetatable(state, -2);
        lua_pop(state, 1);
      }
    }

    struct GetPrecedenceFunction
    {
      static const std::string name;
    };
    const std::string GetPrecedenceFunction::name("getInitialPriority");

    int State::InitialisationMessage::getPrecedence(lua_State* state)
    {
      // need a bit of a hack here because getPrecedence isn't called with an object,
      // just with a single number parameter
      SignatureChecker<
        boost::mpl::vector<NativeTypeWrapper<LUA_TNUMBER> >::type,
        NativeTypeWrapper<LUA_TNUMBER>, GetPrecedenceFunction,
        boost::mpl::vector<>::type
          > sigChecker(state);

      lua_copy(state, lua_upvalueindex(1), -1);

      return sigChecker.resultCount;
    }
  }
}
