#include "native_boxes.h"
#include "messaging.h"

#include <cassert>

namespace WhiteShard
{
  namespace Lua{
    template <>
      const std::string& Lua::LuaTraits<Messaging::BoxPtr>::getName() noexcept
    {
      static const std::string name("box");
      return name;
    }
  }

  namespace Messaging
  {
    const std::string Box::metatableBoxField("box");

    void NilBox::extract(lua_State*)
    {}
    void NilBox::deposit(lua_State* state)
    {
      lua_pushnil(state);
    }

    void BooleanBox::extract(lua_State* state)
    {
      this->value = lua_toboolean(state, -1);
    }
    void BooleanBox::deposit(lua_State* state)
    {
      lua_pushboolean(state, this->value);
    }

    void NumberBox::extract(lua_State* state)
    {
      this->value = lua_tonumber(state, -1);
    }
    void NumberBox::deposit(lua_State* state)
    {
      lua_pushnumber(state, this->value);
    }

    void StringBox::extract(lua_State* state)
    {
      // assume that the string is valid as 
      // we have chosen this path based on its type
      this->value = std::string(lua_tostring(state, -1));
    }
    void StringBox::deposit(lua_State* state)
    {
      lua_pushstring(state, this->value.c_str());
    }

    TableBox::TableBox(int boxPtrMetatableIndex) : boxPtrMetatableIndex(boxPtrMetatableIndex)
    {}
    void TableBox::extract(lua_State* state)
    {
      Lua::StackChecker checker(state, 0, "table extract");

      // recursively create boxes for every key-value pair in the table
      this->boxes.reserve(luaL_len(state, -1));

      lua_pushnil(state);
      std::pair<BoxPtr, BoxPtr> currentBoxPtr;

      while(lua_next(state, -2))
      {
        currentBoxPtr.second = toBoxPtr(state, this->boxPtrMetatableIndex);
        lua_pop(state, 1);
        currentBoxPtr.first = toBoxPtr(state, this->boxPtrMetatableIndex);

        this->boxes.push_back(std::move(currentBoxPtr));
      }
    }
    void TableBox::deposit(lua_State* state)
    {
      Lua::StackChecker checker(state, 1, "table deposit");

      // assume all variables are hashes
      lua_createtable(state, 0, this->boxes.size());
      // apply the box pairs
      // use the first value as a key, second as value
      for (std::pair<BoxPtr, BoxPtr>& currentPtr : this->boxes)
      {
        currentPtr.first->deposit(state);
        currentPtr.second->deposit(state);
        lua_settable(state, -3);
      }
    }
  }
}
