#include "lua_shard.h"
#include "window_wrapper.h"
#include "window_wrapper_functions.h"
#include <string>
#include "window/graphics/openGL.h"

namespace WhiteShard{
  namespace Lua{
    template <>
      const std::string& Lua::LuaTraits<Window::WindowWrapperPtr>::getName() noexcept
    {
      static const std::string name("window");
      return name;
    }
  }

  namespace Window{
    WindowAccess WindowWrapper::getAccess()
    {
      return WindowAccess(this->access, this->window);
    }

    void WindowWrapper::pushMetatable(lua_State* state)
    {
      Lua::StackChecker checker(state, 1, "WindowWrapper::pushMetatable");

      Lua::generateMetatable<WindowWrapperPtr>(state, 1, 1);
      Lua::registerMetatable<WindowWrapperPtr>(state);

      // index table
      lua_createtable(state, 0, 13);

      // window rect properties
      lua_pushcfunction(state, WindowWrapper::luaSetWindowPosition);
      lua_setfield(state, -2, FunctionName<setWindowPosition>::name.c_str());

      lua_pushcfunction(state, WindowWrapper::luaGetWindowPosition);
      lua_setfield(state, -2, FunctionName<getWindowPosition>::name.c_str());

      lua_pushcfunction(state, WindowWrapper::luaSetSize);
      lua_setfield(state, -2, FunctionName<setSize>::name.c_str());

      lua_pushcfunction(state, WindowWrapper::luaGetSize);
      lua_setfield(state, -2, FunctionName<getSize>::name.c_str());

      // misc setters
      lua_pushcfunction(state, WindowWrapper::luaSetMouseCursorVisibility);
      lua_setfield(state, -2, FunctionName<setMouseCursorVisibility>::name.c_str());

      lua_pushcfunction(state, WindowWrapper::luaSetVSync);
      lua_setfield(state, -2, FunctionName<setVSync>::name.c_str());

      lua_pushcfunction(state, WindowWrapper::luaSetVisible);
      lua_setfield(state, -2, FunctionName<setVisible>::name.c_str());

      lua_pushcfunction(state, WindowWrapper::luaSetTitle);
      lua_setfield(state, -2, FunctionName<setTitle>::name.c_str());

      lua_pushcfunction(state, WindowWrapper::luaSetKeyRepeatEnabled);
      lua_setfield(state, -2, FunctionName<setKeyRepeatEnabled>::name.c_str());

      lua_pushcfunction(state, WindowWrapper::luaSetJoystickThreshold);
      lua_setfield(state, -2, FunctionName<setJoystickThreshold>::name.c_str());

      WindowWrapper::pushRestartWindow(state);
      lua_setfield(state, -2, FunctionName<restartWindow>::name.c_str());

      WindowWrapper::pushRestartFullscreenWindow(state);
      lua_setfield(state, -2, FunctionName<restartFullscreenWindow>::name.c_str());

      // set as the index table
      lua_setfield(state, -2, "__index");
    }

    WindowAccess::WindowAccess(std::mutex& access, sf::Window& window) :
      windowLock(access), window(&window)
    {}

    sf::Window& WindowAccess::operator*() const
    {
      return *this->window;
    }

    sf::Window* WindowAccess::operator->() const
    {
      return this->window;
    }
  }
}
