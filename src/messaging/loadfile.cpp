#include "messaging.h"
#include <fstream>
#include <cassert>

namespace WhiteShard
{
  namespace Messaging
  {
    // messages to states who want to be notified on the loading's progress
    class LoadStepMessage : public Lua::Message
    {
      std::string functionPath;
      Lua::State* fromState;
      std::ios::streampos currentReadCount;
      Subsystem* manager;
     public:
      LoadStepMessage(std::string functionPath, Lua::State& fromState, std::ios::streampos currentReadCount, Subsystem& manager) :
        functionPath(std::move(functionPath)), fromState(&fromState), currentReadCount(currentReadCount), manager(&manager)
      {}

      ~LoadStepMessage() noexcept
      {}

      void sendMessage(Lua::State& lState) const throw (LoadFileException)
      {
        // resolve the function path
        lua_State* state(lState);
        Lua::StackChecker checker(state, 0, "Messaging's loadfile");

        lua_pushglobaltable(state);
        lua_getfield(state, -2, this->functionPath.c_str());

        // get the state metatable
        Lua::getMetatable<Lua::StatePtr>(state);

        // copyconstruct a pointer to the sender's state ontop of the metatable
        new (Lua::allocate<Lua::StatePtr>(state)) Lua::StatePtr(this->fromState->getThisPointer());

        // add the read count
        lua_pushnumber(state, this->currentReadCount);
        Lua::observePcall(lua_pcall(state, 2, 0, 0), state);
      }
    };

    // loaderSteppers
    void QuietLoaderStepper::operator()(unsigned int, Lua::State&) const
    {}

    LoudLoaderStepper::LoudLoaderStepper(Lua::StatePtr sendTo, std::string functionPath, Subsystem& manager) :
      sendTo(std::move(sendTo)), functionPath(std::move(functionPath)), manager(&manager)
    {}

    void LoudLoaderStepper::operator()(unsigned int read, Lua::State& state) const
    {
      // send a message the the state pointed with the current read value
      this->sendTo->addMessage(Lua::MessagePtr(new LoadStepMessage(this->functionPath, state, read, *this->manager)));
    }
  }
}
