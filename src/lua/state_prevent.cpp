#include "state.h"

namespace WhiteShard{
  namespace Lua{
    int State::preventSubsystemOverwrite(lua_State* state)
    {
      lua_pushstring(state, "You cannot modify the subsystem listing dynamically");
      lua_error(state);
      return 0;
    }

    int State::preventEngineOverwrite(lua_State* state)
    {
      // if you try and overwrite the key WhiteShard then you'll get an error
      if (lua_isstring(state, 2))
      {
        if (std::string(lua_tostring(state, 2)) == Registry::WhiteShard.c_str())
        {
          lua_pushstring(state, "You cannot overwrite the the global reference to WhiteShard");
          lua_error(state);
        }
      }

      // otherwise rawset the original table
      lua_rawset(state, 1);
      return 0;
    }
  }
}

