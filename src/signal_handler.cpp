#include "signal_handler.h"
#include <csignal>
#include <cassert>
#include <iostream>

namespace WhiteShard
{
  SignalHandler* globalSignalHandler;

  SignalHandler::SignalHandler() : interrupted(false)
  {}

  bool SignalHandler::isInterrupted()
  {
    return globalSignalHandler->interrupted;
  }

  void SignalHandler::handleSignal(int 
#ifndef NDEBUG
      signalType
#endif
        )
  {
    assert(SIGINT == signalType);
    std::cout << "\ninterrupt received - interrupt again to force termination\n";
    globalSignalHandler->interrupted = true;

    // set the interrupt to kill the program if it is called again
    std::signal(SIGINT, SIG_DFL);
  }

  void SignalHandler::initialiseGlobalSignalHandler()
  {
    globalSignalHandler = new SignalHandler();
    std::signal(SIGINT, SignalHandler::handleSignal);
  }
}
