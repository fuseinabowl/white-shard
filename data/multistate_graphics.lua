local initialWindow = WhiteShard.window:createWindow(200,200, "multistate graphics test");
local slaveThread = WhiteShard.messaging:createState("data/multistate_graphics_slave.lua");

-- set up shaders
WhiteShard.window.graphics:lockContext(nil);

local vertShader, vertLog = WhiteShard.window.graphics:createShader(WhiteShard.window.graphics.shaderTypes.vertex, "\
#version 330\
layout(location = 0) in vec2 pos;\
out vec2 location;\
const mat3 transform = mat3(vec3(2.0,0.0,-1.0),\
                            vec3(0.0,2.0,-1.0),\
                            vec3(0.0,0.0,1.0));\
\
void main()\
{\
  location = pos;\
  gl_Position = vec4(pos * 2.0 - 1.0, 0.0, 1.0);\
}");

print("vertex:", vertLog);

local colourFragShader, colourFragLog = WhiteShard.window.graphics:createShader(WhiteShard.window.graphics.shaderTypes.fragment, "\
#version 330\
in vec2 location;\
layout(location = 0) out vec4 colour;\
uniform vec3 plusColour;\
\
void main()\
{\
  colour = vec4(location, 0.0,1.0) + vec4(plusColour, 0.0) * (1.0 - length(location * 2.0 - 1.0));\
}");
print("colour frag:", colourFragLog);

local textureFragShader, textureFragLog = WhiteShard.window.graphics:createShader(WhiteShard.window.graphics.shaderTypes.fragment, "\
#version 330\
in vec2 location;\
layout(location = 0) out vec4 colour;\
uniform sampler2D texture;\
\
void main()\
{\
  colour = texture2D(texture, location);\
}");
print("texture frag:", textureFragLog);

local colourShaderProgram, textureShaderProgram;
if vertShader and textureFragShader and colourFragShader then
  local programLog;
  colourShaderProgram, programLog = WhiteShard.window.graphics:createShaderProgram({vertShader, colourFragShader});
  print("colour log:", programLog);
  textureShaderProgram, programLog = WhiteShard.window.graphics:createShaderProgram({vertShader, textureFragShader});
  print("texture log:", programLog);
end;

if colourShaderProgram == nil or textureShaderProgram == nil then
  print("shaders and shaderProgram didn't compile, ending execution");
  slaveThread:killState();
  WhiteShard.messaging:getThisState():killState();
else
  local graphics = WhiteShard.window.graphics;
  local texture = WhiteShard.window.graphics:createTexture(100,100, graphics.wrapBehaviours.clamp, graphics.wrapBehaviours.clamp, graphics.magnificationFilters.nearest);

  -- set the texture uniform to "texture"
  local emptyTable = {};
  local threeTable = {3};
  local tableOfEmptyTable = {{[textureShaderProgram:getUniform("texture")] = texture}};
  textureShaderProgram:draw({[assert(textureShaderProgram:getUniform("texture"))] = texture}, nil, graphics.unitMesh, emptyTable, emptyTable);

  slaveThread:sendMessage(assert(colourShaderProgram), assert(texture));

  -- now do the function registering stuff
  WhiteShard.messaging:setMessageHandler(function ()
    WhiteShard.window.graphics:lockContext(assert(initialWindow));

    textureShaderProgram:draw(emptyTable, nil, graphics.unitMesh, threeTable, tableOfEmptyTable);

    graphics:render();

    slaveThread:sendMessage();
  end);

  local thisState = WhiteShard.messaging:getThisState();
  WhiteShard:setPriorityFunction(function (jobs)
    if jobs < 1 then
      thisState:sendMessage();
    end;
    return 1;
  end);
end;
