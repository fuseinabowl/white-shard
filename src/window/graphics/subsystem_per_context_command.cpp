#include "subsystem.h"
#include <iostream>

namespace WhiteShard{
  namespace Window{
    namespace Graphics{
      void Subsystem::pushPerContextCommand(PerContextCommandPtr&& originalPointer) noexcept
      {
        // lock the context access mutex
        std::unique_lock<std::mutex> contextsLock(this->availableContextAccess);

        // turn the passed unique_ptr into a shared ptr
        std::shared_ptr<PerContextCommand> sharedPointer(std::move(originalPointer));

        // cycle through the contexts, adding a pointer to the passed command
        for (PureContextWrapper* currentContextWrapper : this->activePureContexts)
        {
          currentContextWrapper->pushPerContextCommand(sharedPointer);
        }

        for (auto currentWindowPair : this->activeWindowContexts)
        {
          currentWindowPair.second->pushPerContextCommand(sharedPointer);
        }
      }
    }
  }
}
