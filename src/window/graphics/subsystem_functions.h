#ifndef _GRAPHICS_FUNCTIONS_H_
#define _GRAPHICS_FUNCTIONS_H_

#include <string>

namespace WhiteShard{
  namespace Window{
    namespace Graphics{
      template <int value>
        struct FunctionName
      {
        static const std::string name;
      };

      typedef FunctionName<0> CreateShaderFunction;
      typedef FunctionName<1> CreateShaderProgramFunction;
      typedef FunctionName<2> CreateTextureFunction;
      typedef FunctionName<3> LoadTextureFunction;
      typedef FunctionName<4> EnableFunction;
      typedef FunctionName<5> DisableFunction;
      typedef FunctionName<6> ContextLockFunction;
      typedef FunctionName<7> HintExtraContextsFunction;
      typedef FunctionName<10> RenderFunction;
      typedef FunctionName<11> CreateMatrixFunction;

      typedef FunctionName<8> ShaderTypeMap;
      typedef FunctionName<9> EnableCodeMap;
    }
  }
}

#endif
