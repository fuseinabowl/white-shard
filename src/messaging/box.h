#ifndef _MESSAGING_BOX_H_
#define _MESSAGING_BOX_H_

#include <string>
#include <memory>

class lua_State;

namespace WhiteShard{
  namespace Messaging{
    class Box; // this class should be implemented by classes that can move userdata between two different states 
    typedef std::unique_ptr<Box> BoxPtr;
    class LoadFileException;

    class Box
    {
     public:
      virtual ~Box();

      // extract the topmost element (assume it's the correct type)
      // do not delete the element when extracting it though
      virtual void extract(lua_State*) = 0;
      void extract(lua_State*, int index);

      // place a copy of the last extracted element on top of the stack
      virtual void deposit(lua_State*) = 0;

      static const std::string metatableBoxField;

      template <class T>
        static int allocateTemplateBox(lua_State*);
     private:
    };

    BoxPtr toBoxPtr(lua_State*, int boxPtrMetatableIndex); // extracts the value at the top into a BoxPtr

    template <class T>
      class BoxTraits; // "::type : public Box" to specialise over a specific box 
  }
}

#include "template_box_fwd.h"
#endif
