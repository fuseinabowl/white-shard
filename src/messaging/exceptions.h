#ifndef _MESSAGING_EXCEPTIONS_H_
#define _MESSAGING_EXCEPTIONS_H_

#include <string>
#include "lua/exception.h"

namespace WhiteShard
{
  namespace Messaging
  {
    struct LoadFileException : public Lua::MessageException
    {
      LoadFileException();
      virtual ~LoadFileException() noexcept;
      virtual const char* what() const noexcept = 0;
    };

    class FileAccessException : public LoadFileException
    {
      std::string exceptionString;
     public:
      FileAccessException(const std::string& filename);
      ~FileAccessException() throw();
      const char* what() const throw();
    };

    class CompileMemoryException : public LoadFileException
    {
      std::string exceptionString;
     public:
      CompileMemoryException(const std::string& filename, const std::string& luaErrorString);
      ~CompileMemoryException() throw();
      const char* what() const throw();
    };

    class SyntaxException : public LoadFileException
    {
      std::string exceptionString;
     public:
      SyntaxException(const std::string& filename, const std::string& luaErrorString);
      ~SyntaxException() throw();
      const char* what() const throw();
    };
  }
}

#endif
