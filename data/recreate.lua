local initialWindow = WhiteShard.window:createWindow(100,100, "test window");
local handler = WhiteShard.window:createEventHandler();
handler:setHandler("keyPressed", function()
  initialWindow:resize(200,100, "changed name");
  handler:setHandler("keyPressed", function()
    WhiteShard.messaging:getThisState():killState();
  end);
end);

WhiteShard:setPriorityFunction(function ()
  WhiteShard.messaging:getThisState():sendMessage();
  return 1;
end);

WhiteShard.messaging:registerMessageHandler(function ()
  handler:handleEvents(initialWindow);
end);
