#include "temporary_object.h"
#include "state.h"

namespace WhiteShard{
  namespace Lua{
    template<>
      const std::string& LuaTraits<TemporaryCollection>::getName() noexcept
    {
      static const std::string name("temp_collection");
      return name;
    };

    TemporaryObject::~TemporaryObject() noexcept
    {}

    TemporaryCollection::~TemporaryCollection() noexcept
    {}

    TemporaryCollection::CollectionType& TemporaryCollection::operator*() noexcept
    {
      return this->objectMap;
    }

    TemporaryCollection::CollectionType* TemporaryCollection::operator->() noexcept
    {
      return &this->objectMap;
    }
  }
}
