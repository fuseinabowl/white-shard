#include "subsystem.h"
#include <boost/mpl/vector.hpp>
#include "interstate_message.h"

namespace WhiteShard{
  namespace Messaging{
    void Subsystem::pushSendMessage(lua_State* state, int subsystemIndex)
    {
      Lua::StackChecker checker(state, 1, "Messaging::pushSendMessage");

      subsystemIndex = lua_absindex(state, subsystemIndex);

      // add the subsystem to the upvalues
      lua_pushnil(state);
      lua_copy(state, subsystemIndex, -1);
      // need the box pointer metatable
      Lua::generateMetatable<BoxPtr>(state);
      // push the closure
      lua_pushcclosure(state, Subsystem::luaSendMessage, 2);
    }

    struct SendMessageFunction
    {
      static const std::string& name;
    };
    const std::string& SendMessageFunction::name(Subsystem::sendMessage);

    int Subsystem::luaSendMessage(lua_State* state)
    {
      Lua::SignatureChecker<
        boost::mpl::vector<>::type,
        Lua::StatePtr, SendMessageFunction,
        boost::mpl::vector<>::type // takes any number of arguments, with most arguments being allowed
          > sigChecker(state);

      // get the state to send to and the parent subsystem 
      Lua::StatePtr& sendTo(Lua::get<Lua::StatePtr>(state, 1));
      Subsystem& messageParent(*Lua::get<SubsystemLuaPtr>(state, lua_upvalueindex(1)));

      std::vector<BoxPtr> parameters;
      BoxPtr currentPtr;

      // need a nil for copying all values to top of stack for slightly more efficient boxing
      lua_pushnil(state); 

      // start at 2 because 1 is the 'self' parameter
      // remember that there is the auxilary nil element above the parameters
      for (int stackIndex = 2; stackIndex < lua_gettop(state); ++stackIndex)
      {
        // try to move all the elements one by one to boxes that allow transportation to a different state
        lua_copy(state, stackIndex, -1);

        parameters.push_back(Messaging::toBoxPtr(state, lua_upvalueindex(2)));
      }

      // get rid of the aux stack position
      lua_pop(state, 1); 

      sendTo->addMessage(Lua::MessagePtr(new InterstateMessage(messageParent, std::move(parameters))));

      return 0;
    }
  }
}
