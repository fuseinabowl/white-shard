#ifndef _MESSAGING_NATIVE_BOXES_H_
#define _MESSAGING_NATIVE_BOXES_H_

// this file defines movement boxes for native lua types

#include "messaging.h"
#include <utility>
#include <vector>

namespace WhiteShard
{
  namespace Messaging
  {
    class NilBox : public Box
    {
     public:
      void extract(lua_State*);
      void deposit(lua_State*);
    };

    class BooleanBox : public Box
    {
      int value;
     public:
      void extract(lua_State*);
      void deposit(lua_State*);
    };

    class NumberBox : public Box
    {
      lua_Number value;
     public:
      void extract(lua_State*);
      void deposit(lua_State*);
    };

    class StringBox : public Box
    {
      std::string value;
     public:
      void extract(lua_State*);
      void deposit(lua_State*);
    };

    class TableBox : public Box
    {
      int boxPtrMetatableIndex;
      std::vector<std::pair<BoxPtr, BoxPtr> > boxes;
     public:
      TableBox(int boxPtrMetatableIndex);
      void extract(lua_State*);
      void deposit(lua_State*);
    };
  }
}

#endif
