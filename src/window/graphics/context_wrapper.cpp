#include "context_wrapper.h"
#include "subsystem.h"
#include <iostream>

namespace WhiteShard{
  namespace Window{
    namespace Graphics{
      ContextWrapperBase::ContextWrapperBase(const Subsystem& subs) noexcept :
        subsystem(&subs),
        textureUnits(subs.getTextureUnits(), {nullptr, 0}),
        currentUnitIterator(textureUnits.begin()),
        currentProgram(0)
      {
      }

      ContextWrapperBase::~ContextWrapperBase() noexcept
      {
      }

      void ContextWrapperBase::initialise(GLsizei initialWidth, GLsizei initialHeight) noexcept
      {
        this->viewportWidth = this->contextWidth = initialWidth;
        this->viewportHeight = this->contextHeight = initialHeight;
        glViewport(0,0, initialWidth, initialHeight);

        glGenFramebuffers(1, &this->FBOName);
      }

      void ContextWrapperBase::pushPerContextCommand(const std::shared_ptr<PerContextCommand>& command) noexcept
      {
        std::unique_lock<std::mutex> commandsLock(this->commandsAccess);

        this->commands.push_back(command);
      }

      void ContextWrapperBase::freeTexture(const Texture* texPtr) noexcept
      {
        std::unique_lock<std::mutex> cacheLock(this->cacheAccess);
        auto texIterator = this->textureUnitLocations.find(texPtr);
        // if texPtr is actually included in this cache...
        if (texIterator != this->textureUnitLocations.end())
        {
          // reset the texture unit's texture pointer to nullptr
          // so that it does not try to remove it from the textureUnitLocations 
          // map when it is replaced
          texIterator->second->first = nullptr;
          // make the texture unit this is bound to have low importance (it will be freed on the next pass guaranteed)
          texIterator->second->second = 0;
          // now erase the entry from the textureUnitLocations
          this->textureUnitLocations.erase(texIterator);
        }
      }

      void ContextWrapperBase::setSize(GLsizei setWidth, GLsizei setHeight) noexcept
      {
        this->contextWidth = setWidth;
        this->contextHeight = setHeight;
      }
    }
  }
}
