#ifndef _LUA_TYPES_H_
#define _LUA_TYPES_H_

#include <boost/type_traits/is_convertible.hpp>
#include <sstream>

namespace WhiteShard{
  namespace Lua{
    // overridable class for C++ object/Lua object interface
    template <class T>
      struct LuaTraits
    {
      static const std::string& getName() noexcept;
    };

    // allow for optional parameters
    template <class T>
      class Optional;
    template <class T>
      struct LuaTraits<Optional<T> >
    {
      static const std::string& getName() noexcept;
    };
    std::string bracketWrap(const std::string&);
    template <class T>
      const std::string& LuaTraits<Optional<T> >::getName() noexcept
    {
      static const std::string name(bracketWrap(LuaTraits<T>::getName()));
      return name;
    };

    // allow for vectors and maps
    template <class T, class Alloc>
      struct LuaTraits<std::vector<T, Alloc> >
    {
      static const std::string& getName() noexcept;
    };
    std::string angleBracketWrap(const std::string&);
    template <class T, class Alloc>
      const std::string& LuaTraits<std::vector<T, Alloc> >::getName() noexcept
    {
      static const std::string name(angleBracketWrap(LuaTraits<T>::getName()));
      return name;
    };

    template <class Key, class Value, class Hash, class Pred, class Alloc>
      struct LuaTraits<std::unordered_map<Key, Value, Hash, Pred, Alloc> >
    {
      static const std::string& getName() noexcept;
    };
    std::string arrowWrap(const std::string&, const std::string&);
    template <class Key, class Value, class Hash, class Pred, class Alloc>
      const std::string& LuaTraits<std::unordered_map<Key, Value, Hash, Pred, Alloc> >::getName() noexcept
    {
      static const std::string name(arrowWrap(LuaTraits<Key>::getName(), LuaTraits<Value>::getName()));
      return name;
    }

    template <class T>
      struct GetReturnTraits
    {
      typedef T& type;
    };

    template <class T, class Alloc>
      struct GetReturnTraits<std::vector<T, Alloc> >
    {
      typedef std::vector<T, Alloc> type;
    };

    template <class Key, class Value, class Hash, class Pred, class Alloc>
      struct GetReturnTraits<std::unordered_map<Key, Value, Hash, Pred, Alloc> >
    {
      typedef std::unordered_map<Key, Value, Hash, Pred, Alloc> type;
    };

    // allocates enough memory for the T object
    // then sets the table at the top of the stack as the metatable for
    // the object (this is left up to the user to ensure this)
    // 
    // !!! IT DOES NOT CONSTRUCT THE OBJECT !!!
    template <class T>
      T* allocate(lua_State*); 

    // set the metatable for a specific object
    // this registers it in the lua state, so you can access it with
    // the getter. The metatable must be at the top of the stack
    template <class T>
      void registerMetatable(lua_State*);

    // get the metatable for the given type T
    template <class T>
      void getMetatable(lua_State*);

    // if the object is userdata, returns it casted to T* (dangerous, does no stack manipulation)
    // during debug, it does actually check the userdata is of the correct type
    template <class T>
      typename GetReturnTraits<T>::type get(lua_State*, int index) noexcept;

    // if check fails, it throws this
    class BadTypenameException : public std::exception
    {
      std::string exceptionMessage;

      std::string expectedTypename;
      std::string badTypename;
      int index;
     public:
      BadTypenameException(std::string, std::string, int index);
      ~BadTypenameException() noexcept;
      const char * what() const noexcept;

      const std::string& getExpectedTypename() const noexcept;
      const std::string& getBadTypename() const noexcept;
      int getIndex() const noexcept;
    };

    // checks that the object retrieved uses the correct typename for T
    // throws an exception if bad typename
    template <class T>
      void check(lua_State*, int index) throw (BadTypenameException);
    template <class T>
      bool checkBool(lua_State*, int index) noexcept;
    
    // produces a basic metatable for T
    // this includes the typename and the garbage collection function
    // you can specify how many extra array and hash elements you want,
    // but the function will always generate enough to make the default table
    template <class T>
      void generateMetatable(lua_State*, int arraySize = 0, int hashSize = 0);

    // the user should not try to call garbageCollect independently
    // it will be called as the object is released from Lua
    // the function gets the object (using get) and calls the object's destructor
    template <class T>
      int garbageCollect(lua_State*);

    // uses c++ == to check equality
    // safe to use with invalid types too
    template <class T>
      int equality(lua_State*);
  }
}

#include "types_fwd.h"

#endif
