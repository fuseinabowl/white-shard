/* this file holds the normal entry point for the program.
   it begins the engine using the INIT_SCRIPT_NAME macro found in config.h

   running valgrind on this will tell you you've possibly lost as many blocks as you
   have command line arguments (including the program name). This is typically three
   when you specify your init file
*/

#include "config.h"
#include "main.h"
#include "lua_shard.h"
#include "engine.h"
#include <string>
#include <vector>
#include <algorithm>
#include <iostream>
#include "signal_handler.h"
#include "lua/exception.h"

#include <SFML/Graphics.hpp>
#include "window/graphics/openGL.h"

int main(int argc, char** argv)
{
  std::cout << "WhiteShard engine - version " << VERSION << std::endl;
#ifndef NDEBUG
  std::cout << "debug version" << std::endl;
#endif
  // start up the signal handler
  WhiteShard::SignalHandler::initialiseGlobalSignalHandler();

  // extract the arguments into a vector of strings
  std::vector<std::string> arguments;
  for(int i = 0; i < argc; ++i)
  {
    arguments.push_back(std::string(*(argv++)));
  }
  // allow the arguments to override the init script name
  std::string initScriptName
#ifdef INIT_SCRIPT_NAME
    (INIT_SCRIPT_NAME)
#endif
      ;
  auto initIterator = std::find(arguments.rbegin(), arguments.rend(), "-init");
  if (arguments.rend() != initIterator && arguments.rbegin() != initIterator)
  {
    initScriptName = *(--initIterator);
  }

  // get into the engine
  if(!initScriptName.empty())
  {
    try
    {
      WhiteShard::Engine engine(initScriptName);
    }
    catch (WhiteShard::Messaging::FileAccessException& ex)
    {
      std::cerr << "invalid init file: " << ex.what() << std::endl;
      return PROGRAM_EXIT_NO_INIT_FILE;
    }
    catch (WhiteShard::Lua::MessageException& ex)
    {
      std::cerr << "critical engine failure: " << ex.what() << std::endl;
      return PROGRAM_EXIT_CRITICAL_ENGINE_FAILURE;
    }
  }
  else
  {
    std::cerr << "no init file specified\n";
    return PROGRAM_EXIT_NO_INIT_FILE;
  }
  return PROGRAM_EXIT_SUCCESS;
}
