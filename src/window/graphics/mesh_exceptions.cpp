#include "mesh.h"

#include <sstream>

namespace WhiteShard{
  namespace Window{
    namespace Graphics{
      ElementOutOfRangeException::ElementOutOfRangeException(int submesh, int entry) noexcept :
        submesh(submesh), entry(entry)
      {}

      ElementOutOfRangeException::~ElementOutOfRangeException() noexcept
      {}

      const char* ElementOutOfRangeException::what() const noexcept
      {
        std::stringstream outStream;
        outStream 
          << "One of the submesh indices was out of range\nThe element at "
          << "(" << this->submesh << ", " << this->entry << ")"
          << " pointed to an element out of the range of vertices available";
        return outStream.str().c_str();
      }

      InconsistentDataListingSizeException::InconsistentDataListingSizeException(int inconsistentListing) noexcept :
        inconsistentListing(inconsistentListing)
      {}

      InconsistentDataListingSizeException::~InconsistentDataListingSizeException() noexcept
      {}

      const char* InconsistentDataListingSizeException::what() const noexcept
      {
        std::stringstream outStream;
        outStream
          << "a data listing was not the same size as the first data listing. All data listings should be the same size\nThe inconsistent listing was "
          << this->inconsistentListing;
        return outStream.str().c_str();
      }

      InvalidDataListingTypeException::InvalidDataListingTypeException(int listingIndex) noexcept :
        listingIndex(listingIndex)
      {}

      InvalidDataListingTypeException::~InvalidDataListingTypeException() noexcept
      {}

      const char* InvalidDataListingTypeException::what() const noexcept
      {
        std::stringstream outStream;
        outStream
          << "An unrecognised or invalid data type was used with a data listing\nThe data listing index is "
          << this->listingIndex;
        return outStream.str().c_str();
      }

      IncompleteVertexException::IncompleteVertexException(int listingIndex, int vertexIndex) noexcept :
        listingIndex(listingIndex), vertexIndex(vertexIndex)
      {}

      IncompleteVertexException::~IncompleteVertexException() noexcept
      {}

      const char* IncompleteVertexException::what() const noexcept
      {
        std::stringstream outStream;
        outStream
          << "the vertex "
          << "(" << this->listingIndex << ", " << this->vertexIndex << ")"
          << " did not have enough data to fill its data type";
        return outStream.str().c_str();
      }
    }
  }
}
