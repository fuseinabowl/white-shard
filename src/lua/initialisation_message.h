#ifndef _INITIALISATION_MESSAGE_H_
#define _INITIALISATION_MESSAGE_H_

#include "handle.h"
#include "state.h"

namespace WhiteShard{
  namespace Lua{
    class State::InitialisationMessage : public Message
    {
      Engine* engine;
      State::precedence initialPrecedence;

     public:
      static int getPrecedence(lua_State*);
      InitialisationMessage(Engine&, State::precedence);

      void sendMessage(State&) const throw(LuaException);
    };
  }
}

#endif
