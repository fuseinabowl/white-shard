#include "engine.h"
#include <thread>
#include <algorithm>
#include <iostream>
#include <cassert>
#include <limits>

#include "signal_handler.h"
#include "messaging/exceptions.h"
// implementation file for the threading facilities in the engine
// this involves retrieving handles and reinserting handles

namespace WhiteShard
{
  const char* Engine::NoRunningStatesException::what() const throw()
  {
    return "no more running states in the engine. This exception should be caught internally by engine!";
  }

  Engine::ThreadFunctor::ThreadFunctor(Engine& engine) :
    engine(&engine)
  {};

  void Engine::ThreadFunctor::operator()()
  {
    Lua::Handle currentExecutableHandle;

    try{
      while(!SignalHandler::isInterrupted())
      {
        // get the most important handle
        currentExecutableHandle = std::move(this->engine->getHandle());

        // perform a single message on it
        try
        {
          (*currentExecutableHandle)->doMessage();

          // return the handle to the engine
          this->engine->returnHandle(std::move(currentExecutableHandle));
        }
        catch(Lua::MessageException& exception)
        {
          std::cerr << exception.what() << std::endl << "fatal error, killing state\n";
          this->engine->killHandle(**currentExecutableHandle);
        }
      }

      // clear the states so that other threads waiting on Handles will be able to leave early
      this->engine->deactivate();
    }
    catch (Engine::NoRunningStatesException&)
    {} // this provides a thread safe way to exit the loop
       // it keeps the return type of getHandle clean while
       // preventing interference between a call to a hypothetical
       // checkRunningStatesEmpty() and the getHandle()

#ifndef NDEBUG
    std::cout << "thread finishing\n";
#endif
  };

  Lua::Handle Engine::getHandle() throw(Engine::NoRunningStatesException)
  {
    // we need it in a collection so that the condition variable can unlock it
    std::unique_lock<std::mutex> handleLock(this->handleAccess);

    // ensure there's a handle in the collection
    while (this->executableHandles.empty())
    {
      if (this->runningStates.empty())
      {
        throw NoRunningStatesException(); // the executable handles are not guaranteed to ever be repopulated
        // if they are repopulated, the engine will run at reduced capacity
      }
      else
      {
        this->handleCondition.wait(handleLock);
      }
    }

    // do a traversal, storing the highest precedence state
    auto highestPrecedenceStateIterator = this->executableHandles.begin();
    int highestPrecedenceValue = std::numeric_limits<int>::min();
    int currentPrecedenceValue;

    for (auto currentIterator = this->executableHandles.begin(); currentIterator != this->executableHandles.end(); ++currentIterator)
    {
      currentPrecedenceValue = (**currentIterator)->getPrecedence();
      if (currentPrecedenceValue > highestPrecedenceValue)
      {
        highestPrecedenceStateIterator = currentIterator;
        highestPrecedenceValue = currentPrecedenceValue;
      }
    }

    auto returnHandle(std::move(*highestPrecedenceStateIterator));
    this->executableHandles.erase(highestPrecedenceStateIterator);

    return std::move(returnHandle);
  }

  void Engine::returnHandle(Lua::Handle&& handle)
  {
    std::lock_guard<std::mutex> collectionLock(this->handleAccess);
    // check if the new handle is still in the running states
    const Lua::StatePtr& handleState(*handle);
    if (this->runningStates.find(handleState) != this->runningStates.end())
    {
      // move the passed value into the executable states
      this->executableHandles.push_back(std::move(handle));
      // notify any threads waiting on an executable
      this->handleCondition.notify_one();
    }
  }

  const Lua::StatePtr& Engine::newHandle(Lua::Handle&& inputHandle)
  {
    std::lock_guard<std::mutex> collectionLock(this->handleAccess);
    const Lua::StatePtr& returnPointer(*inputHandle);

    if (!this->runningStates.empty())
    {
      this->runningStates.insert(*inputHandle);
      this->executableHandles.push_back(std::move(inputHandle));
    }

    return returnPointer;
  }

  class HandleStateComparer
  {
    const Lua::State* statePtr;
   public:
    HandleStateComparer(const Lua::State& state) : statePtr(&state)
    {}
    bool operator()(const Lua::Handle& handle)
    {
      return &**handle == statePtr;
    }
  };

  class StateComparer
  {
    const Lua::State* statePtr;
   public:
    StateComparer(const Lua::State& state) : statePtr(&state)
    {}
    bool operator()(const Lua::StatePtr& comparisonPointer)
    {
      return &*comparisonPointer == statePtr;
    }
  };

  void Engine::killHandle(const Lua::State& state)
  {
    std::lock_guard<std::mutex> collectionLock(this->handleAccess);

    // find the state's handle in the collections
    auto executableDeletionIterator = find_if(this->executableHandles.begin(), this->executableHandles.end(), HandleStateComparer(state));
    if (executableDeletionIterator != this->executableHandles.end())
    {
      this->executableHandles.erase(executableDeletionIterator);
    }

    auto runnableDeletionIterator = find_if(this->runningStates.begin(), this->runningStates.end(), StateComparer(state));
    if (runnableDeletionIterator != this->runningStates.end())
    {
      this->runningStates.erase(runnableDeletionIterator);
    }

    this->handleCondition.notify_all();
  }

  void Engine::deactivate()
  {
    std::lock_guard<std::mutex> stateLock(this->handleAccess);
    this->executableHandles.clear();
    this->runningStates.clear();
    this->handleCondition.notify_all();
  }
}
