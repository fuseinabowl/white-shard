#ifndef _TEXTURE_H_
#define _TEXTURE_H_

#include "window/graphics/openGL.h"
#include "sync_object.h"
#include <memory>

class lua_State;

namespace WhiteShard{
  namespace Window{
    namespace Graphics{
      class Texture;
      typedef std::shared_ptr<Texture> TexturePtr;

      class BadFilenameException : public std::exception
      {
        std::string errorString;
       public:
        BadFilenameException(const std::string& filename) noexcept;
        ~BadFilenameException() noexcept;
        const char* what() noexcept;
      };

      class Texture : public SyncObject
      {
        GLuint texName;
        GLsizei
          width,
          height;
        static void setUpParameters(GLenum sWrap, GLenum tWrap, GLenum minificationFilter, GLenum magnificationFilter, GLfloat anisotropicFilter) noexcept; // assumes texture is bound
       public:
        // the magnification filter is linear if the minification filter is linear of any sort (linear, linear_mipmap_nearest or linear_mipmap_linear)
        // load texture from file
        Texture(Subsystem&, ContextLock&,
            const std::string& fileName,
            GLenum sWrap, GLenum tWrap, GLenum minificationFilter, GLenum magnificationFilter, GLfloat anisotropicFilter) throw (BadFilenameException);
        // create blank texture
        Texture(Subsystem&, ContextLock&,
            GLsizei width, GLsizei height,
            GLenum sWrap, GLenum tWrap, GLenum minificationFilter, GLenum magnificationFilter, GLfloat anisotropicFilter) noexcept;

        ~Texture() noexcept;

        GLuint getTexName() const noexcept;
        std::pair<GLsizei, GLsizei> getSize() const noexcept;

        static int luaChangeState(lua_State*);
        static int luaRecreateMipmaps(lua_State*);
      };
    }
  }
}

#endif
