local initialWindow = WhiteShard.window:createWindow(600, 600, "mipmap test");
local graphics = WhiteShard.window.graphics;
graphics:lockContext(initialWindow);

local vertShader, vertLog = graphics:createShader(WhiteShard.window.graphics.shaderTypes.vertex, "\
#version 330\
\
layout(location = 0) in vec2 inPos;\
uniform vec4 locationParams;\
out vec2 pos;\
\
void main()\
{\
  gl_Position = vec4(((inPos * locationParams.xy + locationParams.zw) * 2.0 - 1.0), 0.0, 1.0);\
  pos = inPos;\
}\
");

if (not vertShader) then
  print(vertLog);
else
  print("vert success\n", vertLog);
end;

local fragShader, fragLog = graphics:createShader(WhiteShard.window.graphics.shaderTypes.fragment, "\
#version 330\
\
in vec2 pos;\
layout (location = 0) out vec4 colour;\
uniform sampler2D image;\
uniform sampler2D otherImage;\
uniform vec4 locationParams;\
uniform float bloom;\
const mat4 tempBloomMat = mat4(vec4(0.7, -0.1, 0.4, 0.0),\
                               vec4(-0.1, 0.7, 0.4, 0.0),\
                               vec4(-0.2, -0.1, 1.3, 0.0),\
                               vec4(0.0, 0.0, 0.0, 1.0));\
/*const mat4 bloomMat = mat4(transpose(tempBloomMat));*/\
\
void main()\
{\
  colour = texture2D(otherImage, pos) + bloom * transpose(tempBloomMat) * texture2D(image, pos, +5.0);\
}\
");

if (not fragShader) then
  print(fragLog);
else
  print("frag success\n", fragLog);
end;

local shader, shaderLog = WhiteShard.window.graphics:createShaderProgram({vertShader, fragShader});

if (not shader) then
  print(shaderLog);
else
  print("shader success\n", shaderLog);
end;

local basicFrag, basicFragLog = graphics:createShader(WhiteShard.window.graphics.shaderTypes.fragment, "\
#version 330\
\
in vec2 pos;\
layout(location = 0) out vec4 colour;\
uniform sampler2D image;\
\
void main()\
{\
  colour = texture2D(image, pos, +3);\
}\
");

print("basicFrag:", basicFragLog);

local basicShader, basicShaderLog = graphics:createShaderProgram({vertShader, basicFrag});
print("basicShader:", basicShaderLog);


print("loading texture");
local northernLights = WhiteShard.window.graphics:loadTexture("../../Pictures/Northern_Lights.jpg", 
  WhiteShard.window.graphics.wrapTypes.mirroredRepeated, WhiteShard.window.graphics.wrapTypes.repeated,
  WhiteShard.window.graphics.magnificationFilters.nearest, WhiteShard.window.graphics.minificationFilters.linearMipmapLinear,
  WhiteShard.window.graphics.anisotropicFilters[8]);
  
local northernLights2 = WhiteShard.window.graphics:loadTexture("../../Pictures/Northern_Lights.jpg", 
  WhiteShard.window.graphics.wrapTypes.mirroredRepeated, WhiteShard.window.graphics.wrapTypes.repeated,
  WhiteShard.window.graphics.magnificationFilters.nearest, WhiteShard.window.graphics.minificationFilters.linearMipmapLinear,
  WhiteShard.window.graphics.anisotropicFilters[1]);

local imageUniform = shader:getUniform("image");
local otherImageUniform = shader:getUniform("otherImage");
local positionUniform = shader:getUniform("locationParams");
local bloomUniform = shader:getUniform("bloom");

local basicShaderImage = basicShader:getUniform("image");
local basicShaderPositionUniform = basicShader:getUniform("locationParams");

local northernPosition = graphics:createMatrix({{1.0,-1.0, 0.0,1.0}});

local unitMesh = WhiteShard.window.graphics.unitMesh;

print("finished initialisation");

local handler = WhiteShard.window:createEventHandler();

handler:setHandler("closed", function()
  WhiteShard.messaging:getThisThread():kill()
end);

local secondary = WhiteShard.window.graphics:createTexture(256,256, nil,nil, nil,WhiteShard.window.graphics.minificationFilters.linearMipmapNearest);
local generateMipmap = true;
handler:setHandler("keyPressed", function()
  generateMipmap = not generateMipmap;
end);

for k in pairs(WhiteShard.window.graphics.wrapTypes) do
  print(k);
end;

do
  local timeStep = 1/60;
  local nextTime = WhiteShard:getTime() + timeStep;
  WhiteShard:setPriorityFunction(function (jobs)
    if nextTime < WhiteShard:getTime() then
      WhiteShard.messaging:getThisThread():sendMessage();
      nextTime = nextTime + timeStep;
      return jobs + 1;
    else
      return jobs;
    end;
  end);
end;

local time = 0;
WhiteShard.messaging:setMessageHandler(function ()
  WhiteShard.window.graphics:lockContext(initialWindow);
  time = time + 1;
  local bloomUpload = WhiteShard.window.graphics:createMatrix({{math.sin(time * 0.01)}});
  handler:handleEvents(initialWindow);
  shader:draw({}, secondary, unitMesh, {3}, {
    {[imageUniform] = northernLights, [otherImageUniform] = northernLights2, [positionUniform] = northernPosition, [bloomUniform] = bloomUpload}
      });
  if generateMipmap then
    secondary:generateMipmaps();
  end;
  basicShader:draw({}, nil, unitMesh, {3}, {
    {[basicShaderImage] = secondary, [basicShaderPositionUniform] = northernPosition}
      });
  WhiteShard.window.graphics:render();
end);
