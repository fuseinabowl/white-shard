#include "messaging.h"
#include "lua_shard.h"
#include <boost/mpl/vector.hpp>

namespace WhiteShard
{
  namespace Lua{
    template <>
      const std::string& Lua::LuaTraits<Messaging::SubsystemFactory>::getName() noexcept
    {
      static const std::string name("messaging_factory");
      return name;
    }
  }

  namespace Messaging
  {
    SubsystemFactory::SubsystemFactory(SubsystemPtrCollection& subsystems, Engine& engine) :
      subsystems(&subsystems), engine(&engine)
    {}

    void SubsystemFactory::createMetatable(lua_State* state) noexcept
    {
      Lua::StackChecker checker(state, 1, "Messaging::Factory::createMetatable");

      Lua::generateMetatable<SubsystemFactory>(state, 0, 1);
      lua_pushcfunction(state, SubsystemFactory::luaCall);
      lua_setfield(state, -2, "__call");
    }

    struct CallName
    {
      static const std::string name;
    };
    const std::string CallName::name("operator()");

    int SubsystemFactory::luaCall(lua_State* state)
    {
      Lua::SignatureChecker<
        boost::mpl::vector<>,
        SubsystemFactory, CallName,
        boost::mpl::vector<>
          > sigChecker(state);

      SubsystemFactory& factory(Lua::get<SubsystemFactory>(state, 1));

      factory.subsystems->push_back(WhiteShard::SubsystemPtr(new Subsystem(*factory.engine)));

      return 0;
    }
  }
}
