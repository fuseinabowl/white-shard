#ifndef _MESSAGING_LOADFILE_H_
#define _MESSAGING_LOADFILE_H_

#include <fstream>
#include <memory>
#include "exceptions.h"
#include "lua/state.h"

namespace WhiteShard{
  class Engine;

  namespace Messaging{
    class LoadFileMessageBase : public Lua::Message
    {
      std::string filename;
      unsigned int loadStep;
     protected:
      std::unique_ptr<std::fstream> getFile() const throw (FileAccessException);
      bool readStep(std::fstream&, std::fstream::streampos, char*&) const noexcept;
      void finalise(std::vector<char>&, Lua::State&) const throw (Lua::LuaException);
     public:
      LoadFileMessageBase(std::string, unsigned int loadStep) noexcept;
      ~LoadFileMessageBase() noexcept;
    };

    template <class T>
      class LoadFileMessage : public LoadFileMessageBase
    {
      T templateData;
     public:
      LoadFileMessage(std::string, unsigned int = UINT_MAX, T = T()); // most general constructor
      ~LoadFileMessage() noexcept;
      void sendMessage(Lua::State&) const throw(Lua::MessageException);
    };

    class QuietLoaderStepper
    {
     public:
      QuietLoaderStepper() = default;
      void operator()(unsigned int, Lua::State&) const;
    };

    class LoudLoaderStepper
    {
      Lua::StatePtr sendTo;
      std::string functionPath;
      Subsystem* manager;
     public:
      LoudLoaderStepper(Lua::StatePtr, std::string, Subsystem&);
      void operator()(unsigned int, Lua::State&) const;
    };

    typedef LoadFileMessage<QuietLoaderStepper> QuietLoadFileMessage;
    typedef LoadFileMessage<LoudLoaderStepper> LoudLoadFileMessage;

  }
}

#include "loadfile_message_fwd.h"

#endif
