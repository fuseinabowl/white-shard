#include "shader_type.h"
#include <string>
#include "lua_shard.h"

namespace WhiteShard{
  namespace Lua{
    template <>
      const std::string& LuaTraits<Window::Graphics::ShaderType>::getName() noexcept
    {
      static const std::string name("shader_type");
      return name;
    }
  }

  namespace Window{
    namespace Graphics{
      ShaderType::ShaderType() noexcept :
        type(GL_NONE)
      {}

      ShaderType::ShaderType(GLenum type) noexcept :
        type(type)
      {}
    }
  }
}
