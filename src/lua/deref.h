#ifndef _LUA_DEREF_H_
#define _LUA_DEREF_H_

#include "state.h"

struct lua_State;

namespace WhiteShard{
  namespace Lua{
    // use unsigned ints here so I don't have to do negative checking,
    // but it warns the programmer that the stackIndex should always be positive
    // and they should check that themselves
    template <class T>
      void deref(lua_State* state, unsigned int stackIndex, T dereferenceIndex);

    template <>
      inline void deref<char*>(lua_State* state, unsigned int stackIndex, char* dereferenceIndex)
    {
      lua_getfield(state, stackIndex, dereferenceIndex);
    }

    template <>
      inline void deref<const std::string&>(lua_State* state, unsigned int stackIndex, const std::string& dereferenceIndex)
    {
      deref(state, stackIndex, dereferenceIndex.c_str());
    }

    template <>
      inline void deref<lua_Number>(lua_State* state, unsigned int stackIndex, lua_Number dereferenceIndex)
    {
      lua_pushnumber(state, dereferenceIndex);
      lua_gettable(state, stackIndex);
    }
  }
}

#endif
