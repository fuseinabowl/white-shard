#include "state.h"
#include "checker.h"

namespace WhiteShard{
  namespace Lua{
#ifndef NDEBUG
    StackChecker::~StackChecker()
    {
      if (lua_gettop(this->state) != this->expectedStackSize)
      {
        std::cerr << "stack checker failed for identifier " << this->identifier << "\n\tExpected size was " << this->expectedStackSize << " but actual size was " << lua_gettop(this->state) << std::endl;
        raise(SIGABRT);
      }
    }
#else
    StackChecker::~StackChecker()
    {}
#endif
  }
}
