#ifndef _MESSAGING_LOADFILE_MESSAGE_FWD_
#define _MESSAGING_LOADFILE_MESSAGE_FWD_

#include <fstream>
#include <iostream>
#include <memory>
#include "lua/temporary_object.h"

namespace WhiteShard
{
  namespace Messaging
  {
    template <class T>
      LoadFileMessage<T>::LoadFileMessage(std::string filename, unsigned int loadStep, T templateData) :
        LoadFileMessageBase(std::move(filename), loadStep),
        templateData(std::move(templateData))
    {}

    template <class T>
      LoadFileMessage<T>::~LoadFileMessage() noexcept
    {}

    template <class T>
      void LoadFileMessage<T>::sendMessage(Lua::State& sendTo) const throw(Lua::MessageException)
    {
      std::unique_ptr<std::fstream> inputFile = this->getFile();

      // how big is the file? Create a buffer the size of the file
      std::fstream::streampos endPos = inputFile->tellg();
      std::vector<char> buffer(endPos);
      char* currentPosition(&*buffer.begin());
      inputFile->seekg(0);

      while (this->readStep(*inputFile, endPos, currentPosition))
      {
        // ask the template object if they want to do anything
        this->templateData(inputFile->tellg(), sendTo);
      }
      this->templateData(inputFile->tellg(), sendTo);

      this->finalise(buffer, sendTo);
    }
  }
}

#endif
