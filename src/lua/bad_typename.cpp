#include "lua_shard.h"
#include <string>
#include <iostream>

namespace WhiteShard{
  namespace Lua{
    BadTypenameException::BadTypenameException(std::string expectedType, std::string valueType, int index) :
      expectedTypename(expectedType), badTypename(valueType), index(index)
    {
      assert(expectedType != valueType);
      this->exceptionMessage
        .append("Bad typename exception in a lua check call - expected type of \"")
        .append(expectedType)
        .append("\" and got a value of type \"")
        .append(valueType)
        .append("\"");
    }

    BadTypenameException::~BadTypenameException() throw ()
    {}

    const char * BadTypenameException::what() const throw()
    {
      return this->exceptionMessage.c_str();
    }

    const std::string& BadTypenameException::getBadTypename() const throw()
    {
      return this->badTypename;
    }

    const std::string& BadTypenameException::getExpectedTypename() const throw()
    {
      return this->expectedTypename;
    }

    int BadTypenameException::getIndex() const throw()
    {
      return this->index;
    }
  }
}
