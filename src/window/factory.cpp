#include "factory.h"
#include "lua_shard.h"
#include <boost/mpl/vector.hpp>
#include "subsystem.h"

namespace WhiteShard{
  namespace Lua{
    template <>
      const std::string& Lua::LuaTraits<Window::Factory>::getName() noexcept
    {
      static const std::string name("window_factory");
      return name;
    }
  }

  namespace Window{
    Factory::Factory(SubsystemPtrCollection& subsystems) noexcept :
      subsystems(&subsystems)
    {}

    void Factory::createMetatable(lua_State* state) noexcept
    {
      Lua::StackChecker checker(state, 1, "Window::Factory::createMetatable");

      Lua::generateMetatable<Window::Factory>(state, 0, 1);
      lua_pushcfunction(state, Factory::createSubsystem);
      lua_setfield(state, -2, "__call");
    }

    struct FactoryCall
    {
      FactoryCall() = delete;
      static const std::string name;
    };
    const std::string FactoryCall::name("operator()");

    int Factory::createSubsystem(lua_State* state)
    {
      Lua::SignatureChecker<
        boost::mpl::vector<>::type,
        Factory, FactoryCall,
        boost::mpl::vector<
          Lua::Optional<Lua::NativeTypeWrapper<LUA_TNUMBER> >
            >::type
          > sigChecker(state);
      Factory& factory(Lua::get<Factory>(state, 1));

      int numberOfContexts = 0;
      if (1 == lua_isnumber(state, 2))
      {
        numberOfContexts = lua_tointeger(state, 2);
      }

      try
      {
        factory.subsystems->push_back(WhiteShard::SubsystemPtr(new Window::Subsystem(numberOfContexts)));
      }
      catch (Graphics::SubsystemConstructorException& ex)
      {
        lua_pushstring(state, ex.what());
        lua_error(state);
      }

      return 0;
    }
  }
}
