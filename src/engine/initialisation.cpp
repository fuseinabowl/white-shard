#include "engine.h"
#include "lua_shard.h"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <tuple>
#include <cassert>
#include "main.h"
#include "messaging.h"
#include <csignal>

namespace WhiteShard{
  Engine::Engine(const std::string& initFileName) throw (Lua::MessageException)
  {
    // run the init script - it should populate the subsystems and extract the first script to run
    // if it fails, an appropriate error message is returned to the user and the program exits
    std::string firstScript;
    int threadsToSpawn(1);

    std::tie(firstScript, threadsToSpawn) = this->runInitScript(initFileName);

    // create the first script and use it as the master script
    Lua::Handle firstHandle(1, *this);
    (*firstHandle)->addMessage(Lua::MessagePtr(new Messaging::QuietLoadFileMessage(firstScript)));

    // have to force the firstHandle into the array rather than call newHandle()
    // as there are currently no states in the engine, so newHandle will reject the handle
    this->runningStates.insert(*firstHandle);
    this->executableHandles.push_back(std::move(firstHandle));

    // try to spawn the number of threads specified by the initscript
    std::vector<std::thread> runningThreads;

    // need to lock early so that the other threads don't try to step
    // the master state
    std::unique_lock<std::mutex> handleLock(this->handleAccess);

    // this thread of execution will join the others when it finishes initialisation 
    // so the prefix operator ensures that exactly `threadsToSpawn' number of threads will
    // be spawned
    ThreadFunctor mainFunctor(*this);
    while (--threadsToSpawn > 0)
    {
      runningThreads.push_back(std::thread(ThreadFunctor(*this)));
    }

    // free all the locks so that the engine can start
    handleLock.unlock();

    // begin main loop in this thread too (returns only at end of program)
    mainFunctor(); 

    // join all threads before exit
    for (std::thread& currentThread : runningThreads)
    {
      currentThread.join();
    }
  }

  Engine::~Engine() noexcept
  {
    // ensure the data members are destroyed in the correct order
    this->runningStates.clear();
    this->executableHandles.clear();
    this->subsystems.clear();
  }

  class CreateFactoriesMessage : public Lua::Message
  {
    Engine* engine;
   public:
    CreateFactoriesMessage(Engine& engine) : engine(&engine)
    {}
    void sendMessage(Lua::State& state) const noexcept
    {
      engine->populateSubsystemFactories(state);
    }
  };

  std::tuple<std::string, Engine::threadCountType> Engine::runInitScript(const std::string& initFileName) throw (Lua::MessageException)
  {
    // open a lua state
    Lua::Handle initScriptHandle(1, *this);
    (*initScriptHandle)->addMessage(Lua::MessagePtr(new CreateFactoriesMessage(*this)));

    Lua::StatePtr initScriptStatePtr = *initScriptHandle;
    lua_State* initScriptState = *initScriptStatePtr;

    // these should never throw exceptions
    initScriptStatePtr->doMessage(); // load up the registry
    initScriptStatePtr->doMessage(); // create the factories (the message passed to the constructor)

    // this might throw exceptions - let it propogate to the caller
    initScriptStatePtr->addMessage(Lua::MessagePtr(new Messaging::QuietLoadFileMessage(initFileName, UINT_MAX, Messaging::QuietLoaderStepper())));
    initScriptStatePtr->doMessage();

    Lua::StackChecker checker(initScriptState, 0, "Engine::runInitScript()");

    // extract the global variable 'firstScript' (should be a string)
    lua_pushglobaltable(initScriptState);
    lua_getfield(initScriptState, 1, "firstScript");
    if (1 != lua_isstring(initScriptState, 2))
    {
      // if the init script doesn't populate or populates `firstScript' with anything other than a string, fail
      std::cerr << "`firstScript' was populated badly in \"" << initFileName 
        << "\" - should be populated with a string containing the name of the first script file to load\n\twas populated with type: "
        << lua_typename(initScriptState, lua_type(initScriptState, -1)) << "\n";

      exit(PROGRAM_EXIT_BAD_FIRST_SCRIPT_POPULATE);
    }

    lua_getfield(initScriptState, 1, "threads");
    if (1 != lua_isnumber(initScriptState, 3))
    {
      // if the init script doesn't populate or populates `firstScript' with anything other than a string, fail
      std::cerr << "`threads' was populated badly in \"" << initFileName 
        << "\" - should be populated with an int containing the number of threads to spawn\n\twas populated with type: "
        << lua_typename(initScriptState, lua_type(initScriptState, 3)) << "\n";

      exit(PROGRAM_EXIT_BAD_FIRST_SCRIPT_POPULATE);
    }

    Engine::threadCountType threads(lua_tonumber(initScriptState, 3));
    const char* firstFilenameCharStar(lua_tostring(initScriptState, 2));
    std::string firstFilename(nullptr != firstFilenameCharStar ? firstFilenameCharStar : "");

    lua_pop(initScriptState, 3);
    return std::make_tuple(firstFilename, threads);
  }
}
