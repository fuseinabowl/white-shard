#ifndef _MESSAGING_H_
#define _MESSAGING_H_

/*
   this file deals with exposing messaging and threading facilities to the lua states
*/

#include <memory>
#include <map>
#include <exception>
#include "engine.h"
#include "exceptions.h"
#include "box.h"

namespace WhiteShard
{
  namespace Messaging
  {
    class SubsystemFactory;
    class Subsystem;
    typedef Subsystem* SubsystemLuaPtr;

    class SubsystemFactory
    {
      SubsystemPtrCollection* subsystems;
      Engine* engine;
     public:
      SubsystemFactory(SubsystemPtrCollection&, Engine&);

      static void createMetatable(lua_State*);
      static int luaCall(lua_State*);
    };

    template <class T>
      class LoadFileMessage : public Lua::Message
    {
      Engine* engine;
      std::string filename;
      unsigned int loadStep;
      T templateData;
     public:
      LoadFileMessage(Engine&, std::string, unsigned int = UINT_MAX, T = T()); // most general constructor - will just move them into fields on construct
      void sendMessage(Lua::State&) const throw(LoadFileException, Lua::LuaException);
    };

    class QuietLoaderStepper
    {
     public:
      QuietLoaderStepper() = default;
      void operator()(unsigned int, Lua::State&) const;
    };

    class LoudLoaderStepper
    {
      Lua::StatePtr sendTo;
      std::string functionPath;
      Subsystem* manager;
     public:
      LoudLoaderStepper(Lua::StatePtr, std::string, Subsystem&);
      void operator()(unsigned int, Lua::State&) const;
    };

    BoxPtr toBoxPtr(lua_State*, int boxPtrMetatableIndex); // extracts the value at the top into a BoxPtr

    typedef LoadFileMessage<QuietLoaderStepper> QuietLoadFileMessage;
    typedef LoadFileMessage<LoudLoaderStepper> LoudLoadFileMessage;

    template <class T>
      class BoxTraits;

    template <class Object, class Subsystem>
      class TemplateBox : public Box
    {
      Object object;
      Subsystem subsystem;

      TemplateBox() = default;
     public:
      void extract(lua_State*);
      void deposit(lua_State*);

      static int allocateBox(lua_State*);
    };
  }
}

#include "loadfile_message_fwd.h"
#include "template_box_fwd.h"

#endif
