#include "destructor_thread.h"
#include <SFML/Graphics.hpp>

namespace WhiteShard{
  namespace Window{
    namespace Graphics{
      ObjectDestructor::ObjectDestructor() noexcept :
        tearDown(false),
        destructionThread(destructionThreadBody, 
            std::ref(this->destructionCommands),
            std::ref(this->destructionCommandsAccess),
            std::ref(this->destructionCommandsCondition),
            std::ref(this->tearDown)
              )
      {
      }

      ObjectDestructor::~ObjectDestructor() noexcept
      {
        this->tearDown = true;
        this->destructionCommandsCondition.notify_all();
        this->destructionThread.join();
      }

      void ObjectDestructor::destructionThreadBody(
        std::vector<DestructionCommandPtr>& commandList,
        std::mutex& commandAccess,
        std::condition_variable& commandCondition,
        bool& tearDown
          ) noexcept
      {
        std::unique_lock<std::mutex> commandLock(commandAccess);
        DestructionCommandPtr currentCommand(nullptr);
        sf::Context context;

        while (!tearDown)
        {
          if (commandList.empty())
          {
            // need to modify the state in order to continue execution
            commandCondition.wait(commandLock);
          }
          else
          {
            // do a destruction command
            currentCommand = std::move(commandList.back());
            commandList.pop_back();
            commandLock.unlock();

            (*currentCommand)();
            currentCommand.reset();

            commandLock.lock();
          }
        }

        // no commands can be added anymore
        // finish all commmands in the list
        for (DestructionCommandPtr& destructionCommand : commandList)
        {
          (*destructionCommand)();
        }
      }

      void ObjectDestructor::pushDestructionCommand(DestructionCommandPtr&& commandPtr) noexcept
      {
        std::unique_lock<std::mutex> destructionCommandsLock(this->destructionCommandsAccess);
        this->destructionCommands.push_back(std::move(commandPtr));
        this->destructionCommandsCondition.notify_one();
      }
    }
  }
}
