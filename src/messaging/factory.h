#ifndef _MESSAGING_H_
#define _MESSAGING_H_

/*
   this file deals with exposing messaging and threading facilities to the lua states
*/

#include <memory>
#include <map>
#include <exception>
#include "engine.h"
#include "exceptions.h"
#include "box.h"

namespace WhiteShard
{
  namespace Messaging
  {
    class SubsystemFactory;
    class Subsystem;
    typedef Subsystem* SubsystemLuaPtr;

    class SubsystemFactory
    {
      SubsystemPtrCollection* subsystems;
      Engine* engine;
     public:
      SubsystemFactory(SubsystemPtrCollection&, Engine&);

      static void createMetatable(lua_State*) noexcept;
      static int luaCall(lua_State*);
    };
  }
}

#endif
