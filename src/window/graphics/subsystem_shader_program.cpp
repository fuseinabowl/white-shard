#include "subsystem.h"
#include "shader_program.h"
#include <boost/mpl/vector.hpp>
#include "subsystem_functions.h"
#include "registry.h"

namespace WhiteShard{
  namespace Window{
    namespace Graphics{
      template <>
        const std::string CreateShaderProgramFunction::name("createShaderProgram");

      ShaderLinkException::~ShaderLinkException() noexcept
      {}

      const char* ShaderLinkException::what() noexcept
      {
        static const char* what("ShaderProgram failed to link");
        return what;
      }

      void Subsystem::pushCreateShaderProgram(lua_State* state)
      {
        Lua::StackChecker checker(state, 1, "Subsystem::pushCreateShaderProgram");

        // need:
        //  shader_program metatable
        ShaderProgram::pushMetatable(state);
        
        //  context lock
        lua_getfield(state, LUA_REGISTRYINDEX, Registry::WhiteShard.c_str());
        lua_rawgeti(state, -1, Registry::ContextLock);
        lua_replace(state, -2);

        // push the closure
        lua_pushcclosure(state, Subsystem::luaCreateShaderProgram, 2);
      }

      int Subsystem::luaCreateShaderProgram(lua_State* state)
      {
        Lua::SignatureChecker<
          boost::mpl::vector<
            Lua::Optional<ShaderProgramPtr>, // nil if program fails to link
            Lua::NativeTypeWrapper<LUA_TSTRING> // info log of the program
              >::type,
          Graphics::SubsystemPtr, CreateShaderProgramFunction,
          boost::mpl::vector<
            std::vector<ShaderPtr> // shaders in the program
              >::type
            > sigChecker(state);

        std::string infoLog;

        // get the context lock
        ContextLockPtr& contextLock(Lua::get<ContextLockPtr>(state, lua_upvalueindex(2)));
        ContextLock::ensurePointerValid(CreateShaderProgramFunction::name, contextLock, state);

        // get the metatable on the stack
        lua_pushnil(state);
        lua_copy(state, lua_upvalueindex(1), -1);
        try
        {
          new (Lua::allocate<ShaderProgramPtr>(state)) ShaderProgramPtr(new ShaderProgram(
                *contextLock,
                *Lua::get<SubsystemPtr>(state, 1),
                Lua::get<std::vector<ShaderPtr> >(state, 2),
                infoLog
                  ));

          contextLock->constructorLock(Lua::get<ShaderProgramPtr>(state, lua_absindex(state, -1)));
        }
        catch (ShaderLinkException)
        {
          lua_pop(state, 1); // pop the metatable
          lua_pushnil(state);
        }

        // push the info log whether or not you've got a valid shader program
        lua_pushstring(state, infoLog.c_str());

        return sigChecker.resultCount;
      }
    }
  }
}
