#ifndef _MESH_H_
#define _MESH_H_

#include <memory>
#include <exception>
#include <stdexcept>
#include "window/graphics/openGL.h"
#include <vector>
#include <thread>
#include "sync_object.h"

class lua_State;

namespace WhiteShard{
  namespace Window{
    namespace Graphics{
      class Mesh;
      typedef std::shared_ptr<Mesh> MeshPtr;

      class ContextLock;
      typedef std::unique_ptr<ContextLock> ContextLockPtr;

      struct SubmeshListing
      {
        GLenum mode;
        std::vector<GLushort> elementListing;
      };

      struct DataListing
      {
        GLenum dataType;
        std::vector<std::vector<GLfloat> > dataListing;
      };

      // exceptions
      struct ElementOutOfRangeException : std::exception
      {
        int submesh, entry;
        ElementOutOfRangeException(int submesh, int entry) noexcept;
        ~ElementOutOfRangeException() noexcept;
        const char* what() const noexcept;
      };

      struct InconsistentDataListingSizeException : std::exception
      {
        int inconsistentListing;
        InconsistentDataListingSizeException(int inconsistentListing) noexcept;
        ~InconsistentDataListingSizeException() noexcept;
        const char* what() const noexcept;
      };

      struct InvalidDataListingTypeException : std::exception
      {
        int listingIndex;
        InvalidDataListingTypeException(int listingIndex) noexcept;
        ~InvalidDataListingTypeException() noexcept;
        const char* what() const noexcept;
      };

      struct IncompleteVertexException : std::exception
      {
        int listingIndex, vertexIndex;
        IncompleteVertexException(int listingIndex, int vertexIndex) noexcept;
        ~IncompleteVertexException() noexcept;
        const char* what() const noexcept;
      };

      // mesh class
      class Mesh : public SyncObject
      {
        struct Submesh
        {
          GLenum mode;
          GLsizei count;
          const void* indices;
        };
        typedef std::vector<Submesh> SubmeshCollection;

        struct DataChannel
        {
          GLvoid* address;
          GLchar size;
        };

        GLuint stride;
        GLuint
          bufferNames[2];
        std::vector<DataChannel> channels;
        SubmeshCollection submeshes;
        std::mutex meshAccess;
       public:
        Mesh(const std::vector<DataListing>& types, const std::vector<SubmeshListing>&, Subsystem&, ContextLock&) throw(
            ElementOutOfRangeException,
            InconsistentDataListingSizeException,
            InvalidDataListingTypeException,
            IncompleteVertexException
              );
        ~Mesh() noexcept;

        static void activate(const MeshPtr&, ContextLock&) noexcept;
        void drawSubmesh(unsigned int submeshIndex, unsigned int instances) throw (std::out_of_range);

        static void pushMetatable(lua_State*);
      };
    }
  }
}

#endif
