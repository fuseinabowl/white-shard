#include "subsystem.h"
#include "mesh.h"
#include "lua_shard.h"

namespace WhiteShard{
  namespace Window{
    namespace Graphics{
      void Subsystem::pushUnitMesh(lua_State* state)
      {
        Lua::StackChecker checker(state, 1, "Subsystem::pushUnitMesh");

        // upload the mesh to lua
        // get the metatable (also registers it)
        Mesh::pushMetatable(state);

        new (Lua::allocate<MeshPtr>(state)) MeshPtr (this->unitMesh);
      }
    }
  }
}
