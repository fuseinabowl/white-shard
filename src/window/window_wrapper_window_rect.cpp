#include "window_wrapper.h"
#include "window_wrapper_functions.h"
#include "lua_shard.h"
#include <boost/mpl/vector.hpp>

namespace WhiteShard{
  namespace Window{
    template <>
      const std::string FunctionName<setWindowPosition>::name("setPosition");
    template <>
      const std::string FunctionName<getWindowPosition>::name("getPosition");
    template <>
      const std::string FunctionName<setSize>::name("setSize");
    template <>
      const std::string FunctionName<getSize>::name("getSize");

    int WindowWrapper::luaSetWindowPosition(lua_State* state)
    {
      Lua::SignatureChecker<
        boost::mpl::vector<>::type,
        WindowWrapperPtr, FunctionName<setWindowPosition>,
        boost::mpl::vector<Lua::NativeTypeWrapper<LUA_TNUMBER>, Lua::NativeTypeWrapper<LUA_TNUMBER> >::type
          > sigChecker(state);

      Lua::get<WindowWrapperPtr>(state, 1)->getAccess()->setPosition(sf::Vector2i(lua_tointeger(state, 2), lua_tointeger(state, 3)));

      return sigChecker.resultCount;
    }

    int WindowWrapper::luaGetWindowPosition(lua_State* state)
    {
      Lua::SignatureChecker<
        boost::mpl::vector<Lua::NativeTypeWrapper<LUA_TNUMBER>, Lua::NativeTypeWrapper<LUA_TNUMBER> >::type,
        WindowWrapperPtr, FunctionName<getWindowPosition>,
        boost::mpl::vector<>::type
          > sigChecker(state);

      sf::Vector2i position(Lua::get<WindowWrapperPtr>(state, 1)->getAccess()->getPosition());

      lua_pushinteger(state, position.x);
      lua_pushinteger(state, position.y);

      return sigChecker.resultCount;
    }

    int WindowWrapper::luaSetSize(lua_State* state)
    {
      Lua::SignatureChecker<
        boost::mpl::vector<>::type,
        WindowWrapperPtr, FunctionName<setSize>,
        boost::mpl::vector<Lua::NativeTypeWrapper<LUA_TNUMBER>, Lua::NativeTypeWrapper<LUA_TNUMBER> >::type
          > sigChecker(state);

      Lua::get<WindowWrapperPtr>(state, 1)->getAccess()->setSize(sf::Vector2u(lua_tointeger(state, 2), lua_tointeger(state, 3)));

      return sigChecker.resultCount;
    }

    int WindowWrapper::luaGetSize(lua_State* state)
    {
      Lua::SignatureChecker<
        boost::mpl::vector<Lua::NativeTypeWrapper<LUA_TNUMBER>, Lua::NativeTypeWrapper<LUA_TNUMBER> >::type,
        WindowWrapperPtr, FunctionName<getSize>,
        boost::mpl::vector<>::type
          > sigChecker(state);

      sf::Vector2u size(Lua::get<WindowWrapperPtr>(state, 1)->getAccess()->getSize());

      lua_pushinteger(state, size.x);
      lua_pushinteger(state, size.y);

      return sigChecker.resultCount;
    }
  }
}
