#include "lua_shard.h"
#include "handle.h"
#include "../engine.h"
#include <cassert>
#include <iostream>
#include "initialisation_message.h"

namespace WhiteShard
{
  namespace Lua
  {
    // Handle static initialisations
    const std::string Handle::subsystems("subsystems");

    // Handle methods
    Handle::Handle(State::precedence precedence, Engine& engine) noexcept : state(nullptr)
    {
      // set up the state
      // this is really unsafe way of doing it but allows a weak pointer to
      // be stored in the Lua::State while returning a valid shared pointer by reference
      new Lua::State(this->state, engine, precedence);
      assert(nullptr != this->state);
    }

    Handle::Handle() noexcept : state(nullptr)
    {
    }

    Handle::Handle(Handle&& rhs) noexcept :
      state(std::move(rhs.state))
    {
      assert(nullptr == rhs.state.get());
    }

    Handle& Handle::operator=(Handle&& rhs) noexcept
    {
      this->state = std::move(rhs.state);

      assert(nullptr == rhs.state.get());

      return *this;
    }
    
    Handle::~Handle() noexcept
    {
      if (nullptr != this->state)
      {
        this->state->closeState();
      }
    }

    const StatePtr& Handle::operator*() const
    {
      return this->state;
    }
    const StatePtr* Handle::operator->() const
    {
      return &this->state;
    }

    int Handle::subsystemIndex(lua_State* state)
    {
      // check for subsystems, otherwise return nil
      lua_pop(state, 2);
      lua_pushnil(state);
      
      assert(1 == lua_gettop(state));
      return 1;
    }

    int Handle::subsystemNewIndex(lua_State* state)
    {
      lua_rawset(state, 1); // uses the top two values to set a field in [1] (of type table)
      lua_pop(state, 1); // pops the newly populated table off the stack

      assert(0 == lua_gettop(state));
      return 0;
    }
  }
}
