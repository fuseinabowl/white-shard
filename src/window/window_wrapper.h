#ifndef _WINDOW_WRAPPER_H_
#define _WINDOW_WRAPPER_H_

#include <memory>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <SFML/Window.hpp>
#include <cassert>

class lua_State;

namespace WhiteShard{
  namespace Window{
    class WindowWrapper;
    typedef std::shared_ptr<WindowWrapper> WindowWrapperPtr;

    class WindowAccess;

    class WindowWrapper
    {
      std::mutex access;
      sf::Window window;

     public:
      template <class ... ConstructorParams>
        WindowWrapper(ConstructorParams...);
      
      WindowAccess getAccess();

      static void pushMetatable(lua_State*);

      static int luaSetWindowPosition(lua_State*);
      static int luaGetWindowPosition(lua_State*);
      static int luaSetSize(lua_State*);
      static int luaGetSize(lua_State*);

      static int luaSetMouseCursorVisibility(lua_State*);
      static int luaSetWindowStyle(lua_State*);
      static int luaSetVSync(lua_State*);
      static int luaSetVisible(lua_State*);
      static int luaSetTitle(lua_State*);
      static int luaSetKeyRepeatEnabled(lua_State*);
      static int luaSetJoystickThreshold(lua_State*);

      static void pushRestartWindow(lua_State*);
      static int luaRestartWindow(lua_State*);
      static void pushRestartFullscreenWindow(lua_State*);
      static int luaRestartFullscreenWindow(lua_State*);
    };

    class WindowAccess
    {
      std::unique_lock<std::mutex> windowLock;
      sf::Window* window;
     public:
      WindowAccess(std::mutex&, sf::Window&);

      WindowAccess(const WindowAccess&) = delete;
      WindowAccess& operator=(const WindowAccess&) = delete;
      WindowAccess(WindowAccess&&) = default;
      WindowAccess& operator=(WindowAccess&&) = default;

      sf::Window& operator*() const;
      sf::Window* operator->() const;
    };

    template <class ... ConstructorParams>
      WindowWrapper::WindowWrapper(ConstructorParams ... constructorParamInstances) :
        window(constructorParamInstances...)
    {
      // unlock the window's context so it can safely be locked in other threads
#ifdef NDEBUG
      window.setActive(false);
#else
      assert(window.setActive(false));
#endif
    }
  }
}

#endif
