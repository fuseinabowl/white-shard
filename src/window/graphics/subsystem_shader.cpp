#include "subsystem.h"
#include "data_definition.h"
#include "shader.h"
#include "lua_shard.h"
#include "lua/checker.h"
#include <boost/mpl/vector.hpp>
#include "subsystem_functions.h"

namespace WhiteShard{
  namespace Window{
    namespace Graphics{
      template <>
        const std::string CreateShaderFunction::name("createShader");

      void Subsystem::pushCreateShader(lua_State* state)
      {
        Lua::StackChecker checker(state, 1, "Graphics::pushCreateShader");

        // the shader metatable is the first upvalue
        Shader::pushMetatable(state);

        // the context lock ptr is the second upvalue
        lua_getfield(state, LUA_REGISTRYINDEX, Registry::WhiteShard.c_str());
        lua_rawgeti(state, -1, Registry::ContextLock);
        lua_replace(state, -2);

        // push the closure itself
        lua_pushcclosure(state, Subsystem::luaCreateShader, 2);
      }

      int Subsystem::luaCreateShader(lua_State* state)
      {
        Lua::SignatureChecker<
          boost::mpl::vector<Lua::Optional<ShaderPtr>, Lua::NativeTypeWrapper<LUA_TSTRING> >::type,
          SubsystemPtr, CreateShaderFunction,
          boost::mpl::vector<ShaderType, Lua::NativeTypeWrapper<LUA_TSTRING> >::type
            > sigChecker(state);

        ContextLockPtr& contextLock(Lua::get<ContextLockPtr>(state, lua_upvalueindex(2)));
        ContextLock::ensurePointerValid(CreateShaderFunction::name, contextLock, state);

        std::string infoLog;

        // put the metatable on the stack
        lua_pushnil(state);
        lua_copy(state, lua_upvalueindex(1), -1);

        try
        {
          new (Lua::allocate<ShaderPtr>(state)) ShaderPtr
          (
            new Shader
            (
              Lua::get<ShaderType>(state, 2),
              std::string(lua_tostring(state, 3)),
              infoLog,
              Lua::get<SubsystemPtr>(state, 1)
            )
          );
          contextLock->constructorLock(Lua::get<ShaderPtr>(state, -1));
        }
        catch (ShaderCompilationException&)
        {
          lua_pop(state, 1); // get rid of the metatable
          lua_pushnil(state);
        }

        lua_pushstring(state, infoLog.c_str());

        return sigChecker.resultCount;
      }
    }
  }
}
