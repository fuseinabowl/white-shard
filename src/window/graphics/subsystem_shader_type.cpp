#include "subsystem.h"
#include "shader_type.h"
#include "subsystem_functions.h"
#include "lua/checker.h"

namespace WhiteShard{
  namespace Window{
    namespace Graphics{
      template <>
        const std::string ShaderTypeMap::name("shaderTypes");

      void pushShaderType(lua_State* state, GLenum type)
      {
        Lua::StackChecker checker(state, 1, "pushShaderType");
        assert(lua_gettop(state) > 1);
        lua_pushnil(state);
        lua_copy(state, -2, -1);

        new (Lua::allocate<ShaderType>(state)) ShaderType(type);
      }

      void Subsystem::pushShaderTypes(lua_State* state)
      {
        Lua::StackChecker checker(state, 1, "Graphics::Subsystem::pushShaderTypeMap");

        lua_createtable(state, 0, 0); // base table
        lua_createtable(state, 0, 2); // metatable
        lua_createtable(state, 0, 3); // index table

        // create the metatable
        Lua::generateMetatable<ShaderType>(state);
        Lua::registerMetatable<ShaderType>(state);

        // now copy it as we need it
        // setfield uses -3 instead of the normal -2
        // because the metatable is at the top
        pushShaderType(state, GL_FRAGMENT_SHADER);
        lua_setfield(state, -3, "fragment");

        pushShaderType(state, GL_VERTEX_SHADER);
        lua_setfield(state, -3, "vertex");

        pushShaderType(state, GL_GEOMETRY_SHADER);
        lua_setfield(state, -3, "geometry");

        // get rid of the metatable
        lua_pop(state, 1);

        lua_setfield(state, -2, "__index");

        // TODO add the do not modify function to __newindex

        lua_setmetatable(state, -2);
      }
    }
  }
}
