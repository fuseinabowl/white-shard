for x, y in ipairs(WhiteShard.window.fullscreenModes) do
  io.write("mode ", x, ":\n\t", y:getWidth(), "x", y:getWidth(), ", " , y:getBpp(), "\n");
end;

local initialWindow = WhiteShard.window:createWindow(100, 100, "window");
local handler = WhiteShard.window:createEventHandler();
handler:setHandler("keyPressed", function ()
  initialWindow:resizeFullscreen(WhiteShard.window.fullscreenModes[2], "fullscreenWindow");
  handler:setHandler("keyPressed", function ()
    WhiteShard.messaging:getThisState():killState();
  end);
end);

WhiteShard:setPriorityFunction(function ()
  WhiteShard.messaging:getThisState():sendMessage();
  return 1;
end);

WhiteShard.messaging:registerMessageHandler(function ()
  handler:handleEvents(initialWindow);
end);
