#ifndef _TEMPORARY_OBJECT_H_
#define _TEMPORARY_OBJECT_H_

// provides access to a bounded collection object that holds unique_ptrs to objects 
// that should be deleted when the collection is cleared

#include <memory>
#include <unordered_map>

namespace WhiteShard{
  namespace Lua{
    class TemporaryObject;
    typedef std::unique_ptr<TemporaryObject> TemporaryObjectPtr;

    struct TemporaryObject
    {
      virtual ~TemporaryObject() noexcept;
    };

    class TemporaryCollection
    {
     public:
      typedef std::unordered_map<void*, TemporaryObjectPtr> CollectionType;
     private:
      CollectionType objectMap;
     public:
      ~TemporaryCollection() noexcept;

      CollectionType& operator*() noexcept;
      CollectionType* operator->() noexcept;
    };
  }
}

#endif
