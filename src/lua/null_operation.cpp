#include "null_operation.h"
#include <iostream>

namespace WhiteShard{
  namespace Lua{
    int nullOperation(lua_State*)
    {
      std::cerr << "message handler was not filled before the state received a message!\n";
      return 0;
    }
  }
}
