#include "subsystem.h"
#include "lua_shard.h"
#include "lua/checker.h"
#include "registry.h"
#include <boost/mpl/vector.hpp>

namespace WhiteShard{
  namespace Window{
    const std::string CreateWindowFunction::name("createWindow");
    const std::string CreateFullscreenWindowFunction::name("createFullscreenWindow");

    void Subsystem::pushCreateWindow(lua_State* state)
    {
      Lua::StackChecker checker(state, 1, "Window::Subsystem::pushCreateWindow");

      // push the metatable (it should have been registered earlier in the subsystem pushSubsystem)
      Lua::getMetatable<WindowWrapperPtr>(state);

      // get the contextLock
      lua_getfield(state, LUA_REGISTRYINDEX, Registry::WhiteShard.c_str());
      lua_rawgeti(state, -1, Registry::ContextLock);

      lua_remove(state, -2);

      lua_pushcclosure(state, Window::Subsystem::luaCreateWindow, 2);
    }

    int Subsystem::luaCreateWindow(lua_State* state)
    {
      Lua::SignatureChecker<
        boost::mpl::vector<WindowWrapperPtr>::type,
        Window::SubsystemLuaPtr, CreateWindowFunction,
        boost::mpl::vector<
          Lua::NativeTypeWrapper<LUA_TNUMBER>, 
          Lua::NativeTypeWrapper<LUA_TNUMBER>,
          Lua::Optional<Lua::NativeTypeWrapper<LUA_TSTRING> >
            >::type
          > sigChecker(state);

      // get the string if it exists
      std::string title;
      if (1 == lua_isstring(state, 4))
      {
        title.assign(lua_tostring(state, 4));
      }

      // try to construct the window wrapper
      lua_pushnil(state);
      lua_copy(state, lua_upvalueindex(1), -1);

      new (Lua::allocate<WindowWrapperPtr>(state)) WindowWrapperPtr(new WindowWrapper(
            sf::VideoMode(lua_tonumber(state, 2), lua_tonumber(state, 3)),
            title,
            sf::Style::Default,
            sf::ContextSettings(0, 0, 0, 3, 3)
              ));

      // add it to the graphics subsystem for locking
      Lua::get<Window::SubsystemLuaPtr>(state, 1)->graphicsSubsystem.addWindowContext(Lua::get<WindowWrapperPtr>(state, -1));

      // relock the ContextLock if it exists
      // as creating the window will unlock any context that was previously locked
      Graphics::ContextLockPtr& contextLock(Lua::get<Window::Graphics::ContextLockPtr>(state, lua_upvalueindex(2)));
      if (nullptr != contextLock)
      {
        contextLock->relock();
      }

      return sigChecker.resultCount;
    }

    void Subsystem::pushCreateFullscreenWindow(lua_State* state)
    {
      Lua::StackChecker checker(state, 1, "Window::Subsystem::pushCreateFullscreenWindow");

      // push the metatable (it should have been registered earlier in the subsystem pushSubsystem)
      Lua::getMetatable<WindowWrapperPtr>(state);

      // get the contextLock
      lua_getfield(state, LUA_REGISTRYINDEX, Registry::WhiteShard.c_str());
      lua_rawgeti(state, -1, Registry::ContextLock);

      lua_remove(state, -2);

      lua_pushcclosure(state, Window::Subsystem::luaCreateFullscreenWindow, 2);
    }

    int Subsystem::luaCreateFullscreenWindow(lua_State* state)
    {
      Lua::SignatureChecker<
        boost::mpl::vector<WindowWrapperPtr>::type,
        Window::SubsystemLuaPtr, CreateFullscreenWindowFunction,
        boost::mpl::vector<
          sf::VideoMode,
          Lua::Optional<Lua::NativeTypeWrapper<LUA_TSTRING> >
            >::type
          > sigChecker(state);

      // get the string if it exists
      std::string title;
      if (1 == lua_isstring(state, 3))
      {
        title.assign(lua_tostring(state, 3));
      }

      // try to construct the window wrapper
      lua_pushnil(state);
      lua_copy(state, lua_upvalueindex(1), -1);

      new (Lua::allocate<WindowWrapperPtr>(state)) WindowWrapperPtr(new WindowWrapper(
            Lua::get<sf::VideoMode>(state, 2),
            title,
            sf::Style::Fullscreen,
            sf::ContextSettings(0, 0, 0, 3, 3)
              ));

      // add it to the graphics subsystem for locking
      Lua::get<Window::SubsystemLuaPtr>(state, 1)->graphicsSubsystem.addWindowContext(Lua::get<WindowWrapperPtr>(state, -1));

      // relock the ContextLock if it exists
      // as creating the window will unlock any context that was previously locked
      Graphics::ContextLockPtr& contextLock(Lua::get<Window::Graphics::ContextLockPtr>(state, lua_upvalueindex(2)));
      if (nullptr != contextLock)
      {
        contextLock->relock();
      }

      return sigChecker.resultCount;
    }

  }
}
