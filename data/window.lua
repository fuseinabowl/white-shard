local WhiteShard = WhiteShard;
local messaging = WhiteShard.messaging;
local window = WhiteShard.window;
local graphics = window.graphics;
local initialWindow;
local eventHandler = window:createEventHandler();

local function createInitialWindow()
  initialWindow = window:createFullscreenWindow(WhiteShard.window.fullscreenModes[1], "initialWindow");
end;

-- do the graphics stuff
local uniformVec, offsetUniform;
local function createShaders()
  graphics:lockContext(nil);
  local vertexShader, errorString = graphics:createShader(graphics.shaderTypes.vertex, 
  "#version 330\
  layout(location = 0) in vec2 inVec;\
  out vec2 location;\
  uniform vec2 offsetUniform;\
  void main(){\
    gl_Position = vec4((inVec.xy + offsetUniform) * 2.0 - vec2(1.0, 1.0) , 0.0, 1.0);\
    location = inVec.xy;\
  }");
  local fragmentShader, fragError = graphics:createShader(graphics.shaderTypes.fragment, 
  "#version 330\
  uniform mat4 uniformVec;\
  uniform vec2 offsetUniform;\
  in vec2 location;\
  layout(location = 0) out vec4 colour;\
  void main(){\
    colour = vec4(location, 1.0, 1.0) * uniformVec[0];\
  }");
  io.write(errorString, "\n");
  io.write(fragError, "\n");

  local linkError;
  shaderProgram, linkError = graphics:createShaderProgram({vertexShader, fragmentShader});
  io.write(linkError, "\n");
  uniformVec = shaderProgram:getUniform("uniformVec");
  offsetUniform = shaderProgram:getUniform("offsetUniform");
end;

-- event handler stuff
local function createEventHandler()
  local function boolToString(bool)
    if (bool) then
      return "true";
    else
      return "false";
    end
  end;
  local colourTarget = graphics:createMatrix({{1,1,1,1},
                                              {1,1,1,1},
                                              {0,0,0,0},
                                              {0,0,0,0}});
  local colourUpload = graphics:createMatrix({{1,1,1,1},
                                              {0,0,0,0},
                                              {0,0,0,0},
                                              {0,0,0,0}});
  local colourEase = graphics:createMatrix({{0.1,0,0,0},
                                            {0,0.1,0,0},
                                            {0,0,0.1,0},
                                            {0,0,0,0.1}});
  local offsetTarget = graphics:createMatrix({{0,0}});
  local offsetUpload = graphics:createMatrix({{0,0}});
  local offsetEase = graphics:createMatrix({{0.1,0},
                                            {0,0.1}});
  local colouredUniforms = {[uniformVec] = colourUpload, [offsetUniform] = offsetUpload};

  local black = graphics:createMatrix({{0.1,0,0,0},
                                       {0,0.1,0,0},
                                       {0,0,0.1,0},
                                       {0,0,0,0.1}});
  local noOffset = graphics:createMatrix({{0,0}});
  local blankUniforms = {[uniformVec] = black, [offsetUniform] = noOffset};

  local function keyPress(keyCode, shift, alt, control)
    io.write("keypress detected! Key code was ", keyCode, "\n");
    io.write("\tmodifiers were ", boolToString(alt), ", ", boolToString(control), ", ", boolToString(shift),".\n");

    colourTarget = graphics:createMatrix({{keyCode * 0.1, 0.5, 0.5, 1.0},
                                          {0,0,0,0},
                                          {0,0,0,0},
                                          {0,0,0,0}});
    offsetTarget = graphics:createMatrix({{keyCode * 0.05, math.sin(keyCode * 0.2) * 0.5 - 0.25}});

    if keyCode == -1 then
      WhiteShard.messaging:getThisThread():kill();
    end;
  end;
  local function close()
    print("closing state");
    messaging:getThisThread():kill();
    print("state closed");
  end;
  local function resized(x, y)
    io.write("resized to size (",x,", ",y,")\n");
  end;

  eventHandler:setHandler("keyPressed", keyPress);
  eventHandler:setHandler("closed", close);
  eventHandler:setHandler("resized", resized);

  local lockContext = graphics.lockContext;
  local offsetUploadTable, colourUploadTable;
  local function update()
    lockContext(graphics, initialWindow);
    eventHandler:handleEvents(initialWindow);

    colourUpload = colourUpload - colourEase * (colourUpload - colourTarget);
    offsetUpload = offsetUpload - offsetEase * (offsetUpload - offsetTarget);

    colouredUniforms[uniformVec] = colourUpload;
    colouredUniforms[offsetUniform] = offsetUpload;

    shaderProgram:draw({}, nil, graphics.unitMesh, {3, 3}, {blankUniforms, colouredUniforms});
    graphics:render();
  end;
  messaging:setMessageHandler(update);
end;

local function setGetPriority()
  local getTime = WhiteShard.getTime;
  local stepSize = 1/60;
  local nextStep = getTime(WhiteShard) + stepSize;
  local thisThread = messaging:getThisThread();
  local sendMessage = thisThread.sendMessage;
  local function getPriority(tasks)
    if tasks > 0 then
      return tasks;
    else
      if getTime(WhiteShard) > nextStep then
        nextStep = nextStep + stepSize;
        sendMessage(thisThread);

        return 1;
      else
        return;
      end
    end
  end;
  WhiteShard:setPriorityFunction(getPriority);
end;

-- set the get priority immediately, otherwise messaged will not be sent to this state at all
-- and it will deadlock
setGetPriority();

messaging:setMessageHandler(
  function ()
    createInitialWindow();
    messaging:setMessageHandler(
      function ()
        createShaders();
        messaging:setMessageHandler(
          function ()
            createEventHandler();
            -- the above function resets with the messaging subsystem, you do
            -- not need to do it again
          end
            );
      end
        );
  end
    );
--WhiteShard:stop();
