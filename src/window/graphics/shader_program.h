#ifndef _SHADER_PROGRAM_H_
#define _SHADER_PROGRAM_H_

#include <memory>
#include <thread>
#include "window/graphics/openGL.h"
#include <vector>
#include <string>
#include "uniform.h"
#include "context_wrapper.h"

class lua_State;

namespace WhiteShard{
  namespace Window{
    namespace Graphics{
      class Subsystem;

      class Shader;
      typedef std::shared_ptr<Shader> ShaderPtr;

      class ShaderProgram;
      typedef std::shared_ptr<ShaderProgram> ShaderProgramPtr;

      struct ShaderLinkException : public std::exception
      {
        ~ShaderLinkException() noexcept;
        const char* what() noexcept;
      };

      class ShaderProgram : public SyncObject
      {
        GLuint programName;
        std::mutex programAccess;
        GLsync syncObject;

        std::unordered_map<std::string, Uniform> uniformMap;
       public:
        ShaderProgram(ContextLock&, Subsystem&, const std::vector<ShaderPtr>&, std::string& infoLog) throw (ShaderLinkException);
        ~ShaderProgram() noexcept;

        static void pushMetatable(lua_State*);
        static void pushGetUniform(lua_State*);
        static void pushDraw(lua_State*);

        static int luaGetUniform(lua_State*);
        static int luaDraw(lua_State*);
      };

      struct GetUniformFunction
      {
        static const std::string name;
      };

      struct DrawFunction
      {
        static const std::string name;
      };
    }
  }
}

#endif
