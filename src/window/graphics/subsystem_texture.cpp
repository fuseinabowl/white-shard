#include "subsystem.h"
#include "subsystem_functions.h"
#include "texture.h"
#include "lua_shard.h"
#include <boost/mpl/vector.hpp>
#include "openGL.h"

namespace WhiteShard{
  namespace Window{
    namespace Graphics{
      template <int id> class EnumHolder
      {
        GLenum value;
       public:
        EnumHolder(GLenum) noexcept;
        ~EnumHolder() noexcept;
        const GLenum& operator*() const noexcept;
        const GLenum* get() const noexcept;
      };

      template <int id>
        EnumHolder<id>::EnumHolder(GLenum value) noexcept :
          value(value)
      {
      }

      template <int id>
        EnumHolder<id>::~EnumHolder() noexcept
      {
      }

      template <int id>
        const GLenum& EnumHolder<id>::operator*() const noexcept
      {
        return this->value;
      }

      template <int id>
        const GLenum* EnumHolder<id>::get() const noexcept
      {
        return &this->value;
      }

      typedef EnumHolder<0> WrapType;
      typedef EnumHolder<1> MagnificationFilter;
      typedef EnumHolder<2> MinificationFilter;

      // specialise for the boxes
      // they are all basically the same but need different registry gets
      template <int enumValue>
        struct RegistryIndex
      {
        enum {
          value = -1
        };
      };

      template <>
        struct RegistryIndex<0>
      {
        enum {
          value = Registry::TextureWrapTypes
        };
      };

      template <>
        struct RegistryIndex<1>
      {
        enum {
          value = Registry::TextureMagnificationFilters
        };
      };

      template <>
        struct RegistryIndex<2>
      {
        enum {
          value = Registry::TextureMinificationFilters
        };
      };

      class AnisotropicFilter
      {
        GLfloat anisotropy;
       public:
        AnisotropicFilter(GLfloat) noexcept;
        ~AnisotropicFilter() noexcept;
        const GLfloat& operator*() const noexcept;
        const GLfloat* get() const noexcept;
      };

      AnisotropicFilter::AnisotropicFilter(GLfloat value) noexcept :
        anisotropy(value)
      {
      }

      AnisotropicFilter::~AnisotropicFilter() noexcept
      {
      }

      const GLfloat& AnisotropicFilter::operator*() const noexcept
      {
        return this->anisotropy;
      }

      const GLfloat* AnisotropicFilter::get() const noexcept
      {
        return &this->anisotropy;
      }
    }
  }

  namespace Lua{
    template <>
      const std::string& LuaTraits<Window::Graphics::WrapType>::getName() noexcept
    {
      static const std::string name("wrap_type");
      return name;
    }

    template <>
      const std::string& LuaTraits<Window::Graphics::MagnificationFilter>::getName() noexcept
    {
      static const std::string name("magnification_filter");
      return name;
    }

    template <>
      const std::string& LuaTraits<Window::Graphics::MinificationFilter>::getName() noexcept
    {
      static const std::string name("minification_filter");
      return name;
    }

    template <>
      const std::string& LuaTraits<Window::Graphics::AnisotropicFilter>::getName() noexcept
    {
      static const std::string name("anisotropic_filter");
      return name;
    }
  }

  namespace Messaging{
    template <int enumValue>
      struct BoxTraits<Window::Graphics::EnumHolder<enumValue> >
    {
      class type : public Box
      {
        GLenum value;
       public:
        ~type() noexcept
        {
        }

        void extract(lua_State* state)
        {
          this->value = *Lua::get<Window::Graphics::EnumHolder<enumValue> >(state, lua_gettop(state));
        }

        void deposit(lua_State* state) noexcept
        {
          lua_getfield(state, LUA_REGISTRYINDEX, Registry::WhiteShard.c_str());
          lua_rawgeti(state, -1, Window::Graphics::RegistryIndex<enumValue>::value);
          lua_rawgeti(state, -1, this->value);
          lua_insert(state, -3);
          lua_pop(state, 2);
        }
      };
    };

    template <>
      struct BoxTraits<Window::Graphics::AnisotropicFilter>
    {
      class type : public Box
      {
        GLfloat value;
       public:
        ~type() noexcept
        {
        }

        void extract(lua_State* state)
        {
          this->value = *Lua::get<Window::Graphics::AnisotropicFilter>(state, lua_gettop(state));
        }

        void deposit(lua_State* state) noexcept
        {
          lua_getfield(state, LUA_REGISTRYINDEX, Registry::WhiteShard.c_str());
          lua_rawgeti(state, -1, Registry::TextureAnisotropicFilters);
          lua_rawgeti(state, -1, this->value);
          lua_insert(state, -3);
          lua_pop(state, 2);
        }
      };
    };
  }

  namespace Window{
    namespace Graphics{
      void Subsystem::freeTexture(const Texture* texPtr) noexcept
      {
        std::unique_lock<std::mutex> wrapperAccess(this->availableContextAccess);
        for (auto curWrapper : this->activeWindowContexts)
        {
          curWrapper.second->freeTexture(texPtr);
        } 

        for (PureContextWrapper* curWrapper : this->activePureContexts)
        {
          curWrapper->freeTexture(texPtr);
        }
      }
      
      void Subsystem::pushMagnificationFilters(lua_State* state)
      {
        Lua::StackChecker checker(state, 1, "pushMagnificationFilters");

        lua_createtable(state, 0, 2); // user table
        lua_createtable(state, 0, 2); // messaging table

        // create the filter type metatable
        Lua::generateMetatable<MagnificationFilter>(state);
        Lua::registerMetatable<MagnificationFilter>(state);
        // create two references to the metatable
        lua_pushvalue(state, -1);

        new (Lua::allocate<MagnificationFilter>(state)) MagnificationFilter(GL_LINEAR);
        lua_pushvalue(state, -1);
        lua_rawseti(state, -4, GL_LINEAR);
        lua_setfield(state, -4, "linear");
        
        new (Lua::allocate<MagnificationFilter>(state)) MagnificationFilter(GL_NEAREST);
        lua_pushvalue(state, -1);
        lua_rawseti(state, -3, GL_NEAREST);
        lua_setfield(state, -3, "nearest");

        // put the messaging table into the registry
        lua_getfield(state, LUA_REGISTRYINDEX, Registry::WhiteShard.c_str());
        lua_insert(state, -2);
        lua_rawseti(state, -2, Registry::TextureMagnificationFilters);
        lua_pop(state, 1);
      }

      void Subsystem::pushAnisotropicFilters(lua_State* state, const Subsystem& subsystem)
      {
        Lua::StackChecker checker(state, 1, "pushAnisotropicFilters");

        lua_createtable(state, 0, 0); // user table

        // we need the 1.0 anisotropic filter so we can replace higher order filters
        // when calling setFilters()
        // however, we don't want to call an unused extension, so only expose
        // the 1.0 filter if anisotropic filtering is enabled
        if (subsystem.maxAnisotropicFiltering > 1.0)
        {
          Lua::generateMetatable<AnisotropicFilter>(state);
          Lua::registerMetatable<AnisotropicFilter>(state);

          GLfloat i;
          int filtersPushed = 0;
          for (i = 1.0f; i < subsystem.maxAnisotropicFiltering; i *= 2)
          {
            lua_pushvalue(state, -1);
            new (Lua::allocate<AnisotropicFilter>(state)) AnisotropicFilter(i);
            // -3 because the metatable is at -2
            lua_rawseti(state, -3, static_cast<int>(i));

            ++filtersPushed;
          }
          
          lua_pop(state, 1); // pop the metatable
        }

        lua_pushvalue(state, -1); // copy the filters table

        // put the filters table in the registry
        lua_getfield(state, LUA_REGISTRYINDEX, Registry::WhiteShard.c_str());
        lua_insert(state, -2);
        lua_rawseti(state, -2, Registry::TextureAnisotropicFilters);
        lua_pop(state, 1); // get rid of the registry table
      }

      void Subsystem::pushMinificationFilters(lua_State* state)
      {
        Lua::StackChecker checker(state, 1, "pushMinificationFilters");

        lua_createtable(state, 0, 6); // the user's filters table
        lua_createtable(state, 0, 6); // the messaging filters table

        Lua::generateMetatable<MinificationFilter>(state);
        Lua::registerMetatable<MinificationFilter>(state);
        // create 6 metatable references
        for (unsigned char i = 0; i < 5; ++i)
        {
          lua_pushvalue(state, -1);
        }

        new (Lua::allocate<MinificationFilter>(state)) MinificationFilter(GL_LINEAR);
        lua_pushvalue(state, -1);
        lua_rawseti(state, -8, GL_LINEAR);
        lua_setfield(state, -8, "linear");
        new (Lua::allocate<MinificationFilter>(state)) MinificationFilter(GL_NEAREST);
        lua_pushvalue(state, -1);
        lua_rawseti(state, -7, GL_NEAREST);
        lua_setfield(state, -7, "nearest");
        new (Lua::allocate<MinificationFilter>(state)) MinificationFilter(GL_LINEAR_MIPMAP_LINEAR);
        lua_pushvalue(state, -1);
        lua_rawseti(state, -6, GL_LINEAR_MIPMAP_LINEAR);
        lua_setfield(state, -6, "linearMipmapLinear");
        new (Lua::allocate<MinificationFilter>(state)) MinificationFilter(GL_NEAREST_MIPMAP_LINEAR);
        lua_pushvalue(state, -1);
        lua_rawseti(state, -5, GL_NEAREST_MIPMAP_LINEAR);
        lua_setfield(state, -5, "nearestMipmapLinear");
        new (Lua::allocate<MinificationFilter>(state)) MinificationFilter(GL_LINEAR_MIPMAP_NEAREST);
        lua_pushvalue(state, -1);
        lua_rawseti(state, -4, GL_LINEAR_MIPMAP_NEAREST);
        lua_setfield(state, -4, "linearMipmapNearest");
        new (Lua::allocate<MinificationFilter>(state)) MinificationFilter(GL_NEAREST_MIPMAP_NEAREST);
        lua_pushvalue(state, -1);
        lua_rawseti(state, -3, GL_NEAREST_MIPMAP_NEAREST);
        lua_setfield(state, -3, "nearestMipmapNearest");

        // put the messaging reference table into the registry
        lua_getfield(state, LUA_REGISTRYINDEX, Registry::WhiteShard.c_str());
        lua_insert(state, -2);
        lua_rawseti(state, -2, Registry::TextureMinificationFilters);
        lua_pop(state, 1); // get rid of the white shard registry table
      }

      void Subsystem::pushWrapTypes(lua_State* state)
      {
        Lua::StackChecker checker(state, 1, "pushWrapTypes");

        lua_createtable(state, 0, 3);
        lua_createtable(state, 0, 3);

        Lua::generateMetatable<WrapType>(state);
        Lua::registerMetatable<WrapType>(state);

        // create 3 copies of the metatable reference
        for (unsigned char i = 0; i < 2; ++i)
        {
          lua_pushvalue(state, -1);
        }

        // only edge clamp is allowed because the engine does not support border colouring
        new (Lua::allocate<WrapType>(state)) WrapType(GL_CLAMP_TO_EDGE);
        lua_pushvalue(state, -1);
        lua_rawseti(state, -5, GL_CLAMP_TO_EDGE);
        lua_setfield(state, -5, "clamp");
        new (Lua::allocate<WrapType>(state)) WrapType(GL_REPEAT);
        lua_pushvalue(state, -1);
        lua_rawseti(state, -4, GL_REPEAT);
        lua_setfield(state, -4, "repeated");
        new (Lua::allocate<WrapType>(state)) WrapType(GL_MIRRORED_REPEAT);
        lua_pushvalue(state, -1);
        lua_rawseti(state, -3, GL_MIRRORED_REPEAT);
        lua_setfield(state, -3, "mirroredRepeated");

        // put the messaging reference table in the registry
        lua_getfield(state, LUA_REGISTRYINDEX, Registry::WhiteShard.c_str());
        lua_insert(state, -2);
        lua_rawseti(state, -2, Registry::TextureWrapTypes);
        lua_pop(state, 1);
      }

      struct ChangeStateFunction
      {
        static const std::string name;
      };
      const std::string ChangeStateFunction::name("setFilters");

      struct RecreateMipmapsFunction
      {
        static const std::string name;
      };
      const std::string RecreateMipmapsFunction::name("generateMipmaps");

      void Subsystem::pushLoadTexture(lua_State* state)
      {
        Lua::StackChecker checker(state, 1, "pushLoadTexture");

        lua_getfield(state, LUA_REGISTRYINDEX, Registry::WhiteShard.c_str());
        lua_rawgeti(state, -1, Registry::ContextLock);
        lua_remove(state, -2);

        // we need the texture metatable
        // always call pushLoadTexture before pushCreateTexture
        Lua::generateMetatable<TexturePtr>(state, 1, 1);
        Lua::registerMetatable<TexturePtr>(state);

        lua_createtable(state, 0, 1);

        // get the context lock for the closure
        lua_getfield(state, LUA_REGISTRYINDEX, Registry::WhiteShard.c_str());
        lua_rawgeti(state, -1, Registry::ContextLock);
        lua_remove(state, -2);
        lua_pushvalue(state, -1);
        // push the closure
        lua_pushcclosure(state, Texture::luaChangeState, 1);
        lua_setfield(state, -3, ChangeStateFunction::name.c_str());

        lua_pushcclosure(state, Texture::luaRecreateMipmaps, 1);
        lua_setfield(state, -2, RecreateMipmapsFunction::name.c_str());

        lua_setfield(state, -2, "__index");

        lua_pushcclosure(state, Subsystem::luaLoadTexture, 2);
      }

      template <>
        const std::string LoadTextureFunction::name("loadTexture");

      int Subsystem::luaLoadTexture(lua_State* state)
      {
        Lua::SignatureChecker<
          boost::mpl::vector<Lua::Optional<TexturePtr> >::type,
          Graphics::SubsystemPtr, LoadTextureFunction,
          boost::mpl::vector<
            // file name
              Lua::NativeTypeWrapper<LUA_TSTRING>, 
            // sWrap and tWrap parameters
              Lua::Optional<WrapType>, Lua::Optional<WrapType>, 
            // texture get filters
              Lua::Optional<MagnificationFilter>, Lua::Optional<MinificationFilter>,
              Lua::Optional<AnisotropicFilter>
                >::type
            > sigChecker(state);

        ContextLockPtr& contextLockPtr = Lua::get<ContextLockPtr>(state, lua_upvalueindex(1));
        ContextLock::ensurePointerValid(CreateTextureFunction::name, contextLockPtr, state);
        ContextLock& contextLock = *contextLockPtr;

        // get all the parameters
        GLenum 
          sWrap = GL_REPEAT,
          tWrap = GL_REPEAT,
          magFilter = GL_LINEAR,
          minFilter = GL_LINEAR_MIPMAP_NEAREST;
        GLfloat
          anisotropicFilter = 1.0f;

        if (!lua_isnone(state, 3))
        {
          if (!lua_isnil(state, 3))
          {
            sWrap = *Lua::get<WrapType>(state, 3);
          }

          if (!lua_isnone(state, 4))
          {
            if (!lua_isnil(state, 4))
            {
              tWrap = *Lua::get<WrapType>(state, 4);
            }

            if (!lua_isnone(state, 5))
            {
              if (!lua_isnil(state, 5))
              {
                magFilter = *Lua::get<MagnificationFilter>(state, 5);
              }

              if (!lua_isnone(state, 6))
              {
                if (!lua_isnil(state, 6))
                {
                  minFilter = *Lua::get<MinificationFilter>(state, 6);
                }

                if (!lua_isnoneornil(state, 7))
                {
                  anisotropicFilter = *Lua::get<AnisotropicFilter>(state, 7);
                }
              }
            }
          }
        }

        TexturePtr newTexture;
        try
        {
          newTexture.reset(new Texture(*Lua::get<Subsystem*>(state, 1), contextLock, std::string(lua_tostring(state, 2)), 
                sWrap, tWrap, minFilter, magFilter, anisotropicFilter));
        }
        catch (BadFilenameException& ex)
        {
          lua_pushstring(state, ex.what());
          lua_error(state);
        }

        contextLock.constructorLock(newTexture);

        // texture was created successfully
        lua_pushnil(state);
        lua_copy(state, lua_upvalueindex(2), -1);
        new (Lua::allocate<TexturePtr>(state)) TexturePtr(std::move(newTexture));

        return sigChecker.resultCount;
      }

      template <>
        const std::string CreateTextureFunction::name("createTexture");

      void Subsystem::pushCreateTexture(lua_State* state)
      {
        Lua::StackChecker checker(state, 1, "pushCreateTexture");

        // get the contextLockPtr
        lua_getfield(state, LUA_REGISTRYINDEX, Registry::WhiteShard.c_str());
        lua_rawgeti(state, -1, Registry::ContextLock);
        lua_remove(state, -2);

        Lua::getMetatable<TexturePtr>(state);

        lua_pushcclosure(state, Subsystem::luaCreateTexture, 2);
      }

      int Subsystem::luaCreateTexture(lua_State* state)
      {
        Lua::SignatureChecker<
          boost::mpl::vector<Lua::Optional<TexturePtr> >::type,
          Subsystem*, CreateTextureFunction,
          boost::mpl::vector<
            Lua::NativeTypeWrapper<LUA_TNUMBER>, Lua::NativeTypeWrapper<LUA_TNUMBER>,
          // sWrap and tWrap parameters
            Lua::Optional<WrapType>, Lua::Optional<WrapType>, 
          // texture get filters
            Lua::Optional<MagnificationFilter>, Lua::Optional<MinificationFilter>,
            Lua::Optional<AnisotropicFilter>
              >::type
            > sigChecker(state);

        ContextLockPtr& contextLockPtr = Lua::get<ContextLockPtr>(state, lua_upvalueindex(1));
        ContextLock::ensurePointerValid(CreateTextureFunction::name, contextLockPtr, state);
        ContextLock& contextLock = *contextLockPtr;

        Subsystem& subsystem = *Lua::get<Subsystem*>(state, 1);

        // get all the parameters
        GLuint
          width = lua_tonumber(state, 2),
          height = lua_tonumber(state, 3);
        GLenum 
          sWrap = GL_REPEAT,
          tWrap = GL_REPEAT,
          magFilter = GL_LINEAR,
          minFilter = GL_LINEAR_MIPMAP_NEAREST;
        GLfloat
          anisotropicFilter = 1.0f;

        if (!lua_isnone(state, 4))
        {
          if (!lua_isnil(state, 4))
          {
            sWrap = *Lua::get<WrapType>(state, 4);
          }

          if (!lua_isnone(state, 5))
          {
            if (!lua_isnil(state, 5))
            {
              tWrap = *Lua::get<WrapType>(state, 5);
            }

            if (!lua_isnone(state, 6))
            {
              if (!lua_isnil(state, 6))
              {
                magFilter = *Lua::get<MagnificationFilter>(state, 6);
              }

              if (!lua_isnone(state, 7))
              {
                if (!lua_isnil(state, 7))
                {
                  minFilter = *Lua::get<MinificationFilter>(state, 7);
                }

                if (!lua_isnoneornil(state, 8))
                {
                  anisotropicFilter = *Lua::get<AnisotropicFilter>(state, 8);
                }
              }
            }
          }
        }

        lua_pushnil(state);
        lua_copy(state, lua_upvalueindex(2), -1);
        TexturePtr& luaPointer = *(new (Lua::allocate<TexturePtr>(state)) TexturePtr(new Texture(subsystem, contextLock, width, height, sWrap, tWrap, minFilter, magFilter, anisotropicFilter)));

        contextLock.constructorLock(luaPointer);

        return sigChecker.resultCount;
      }

      int Texture::luaChangeState(lua_State* state)
      {
        Lua::SignatureChecker<
          boost::mpl::vector<>::type,
          TexturePtr, ChangeStateFunction,
          boost::mpl::vector<
            Lua::Optional<WrapType>, Lua::Optional<WrapType>,
            Lua::Optional<MagnificationFilter>, Lua::Optional<MinificationFilter>,
            Lua::Optional<AnisotropicFilter>
              >
            > sigChecker(state);

        // get the texture in question into the openGL binding through the contextLock
        ContextLockPtr& contextPtr = Lua::get<ContextLockPtr>(state, lua_upvalueindex(1));
        ContextLock::ensurePointerValid(ChangeStateFunction::name, contextPtr, state);
        ContextLock& context = *contextPtr;

        TexturePtr& texture = Lua::get<TexturePtr>(state, 1);
        auto value = context.cacheTextures({texture.get()});
        glActiveTexture(GL_TEXTURE0 + value.front());

        if (!lua_isnoneornil(state, 2))
        {
          // upload the wrap type
          glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, *Lua::get<WrapType>(state, 2));
        }

        if (!lua_isnoneornil(state, 3))
        {
          // upload the wrap type
          glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, *Lua::get<WrapType>(state, 3));
        }

        if (!lua_isnoneornil(state, 4))
        {
          // upload the magnification filter
          glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, *Lua::get<MagnificationFilter>(state, 4));
        }

        if (!lua_isnoneornil(state, 5))
        {
          // upload the minification filter
          glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, *Lua::get<MinificationFilter>(state, 5));
        }

        if (!lua_isnoneornil(state, 6))
        {
          // upload the anisotropic filter
          glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, *Lua::get<AnisotropicFilter>(state, 6));
        }

        return sigChecker.resultCount;
      }

      int Texture::luaRecreateMipmaps(lua_State* state)
      {
        Lua::SignatureChecker<
          boost::mpl::vector<>,
          TexturePtr, RecreateMipmapsFunction,
          boost::mpl::vector<>
            > sigChecker(state);

        ContextLockPtr& contextPtr(Lua::get<ContextLockPtr>(state, lua_upvalueindex(1)));
        ContextLock::ensurePointerValid(RecreateMipmapsFunction::name, contextPtr, state);

        glActiveTexture(GL_TEXTURE0 + contextPtr->cacheTextures({Lua::get<TexturePtr>(state, 1).get()}).front());
        glGenerateMipmap(GL_TEXTURE_2D);

        return sigChecker.resultCount;
      }
    }
  }
}
