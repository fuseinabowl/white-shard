#include "shader.h"
#include "subsystem.h"
#include "lua_shard.h"
#include <boost/mpl/vector.hpp>

namespace WhiteShard{
  namespace Lua{
    template <>
      const std::string& Lua::LuaTraits<Window::Graphics::ShaderPtr>::getName() noexcept
    {
      static const std::string name("shader");
      return name;
    }
  }

  namespace Window{
    namespace Graphics{

#ifndef NDEBUG
      struct DestroyShaderFunction
      {
        static const std::string name;
      };
      const std::string DestroyShaderFunction::name("<gc metamethod>");
#endif

      void Shader::pushMetatable(lua_State* state)
      {
        Lua::StackChecker checker(state, 1, "Graphics::Shader::pushMetatable");

        Lua::generateMetatable<ShaderPtr>(state);
        Lua::registerMetatable<ShaderPtr>(state);
      }
    }
  }
}
