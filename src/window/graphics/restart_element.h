#ifndef _RESTART_ELEMENT_H_
#define _RESTART_ELEMENT_H_

#include <limits>
#include "window/graphics/openGL.h"

namespace WhiteShard{
  namespace Window{
    namespace Graphics{
      enum {
        RESTART_INDEX = std::numeric_limits<GLushort>::max()
      };
    }
  }
}

#endif
