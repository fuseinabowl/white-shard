#ifndef _MATRIX_H_
#error "this file needs to be included in \"matrix.h\""
#endif

#include <boost/mpl/vector.hpp>
#include "lua_shard.h"

namespace WhiteShard{
  namespace Window{
    namespace Graphics{
      // naming function for lua userdata
      template <int x, int y>
        std::string createMatrixName()
      {
        std::stringstream nameStream;
        nameStream
          << "matrix"
          << x << "x" << y;
        return nameStream.str();
      }
    }
  }
  
  namespace Lua{
    template <int x, int y>
      struct LuaTraits<Window::Graphics::Matrix<x,y> >
    {
      static const std::string& getName() noexcept
      {
        static const std::string name(Window::Graphics::createMatrixName<x,y>());
        return name;
      }
    };
  }

  namespace Window{
    namespace Graphics{
      // function definitions

      template <int x, int y>
        Matrix<x,y>::Matrix() noexcept
      {
      }

      template <int x, int y>
        Matrix<x,y>::Matrix(arma::fmat::fixed<x,y>&& fromMatrix) noexcept :
          internalMatrix(std::move(fromMatrix))
      {
      }

      template <int x, int y>
        Matrix<x,y>::~Matrix() noexcept
      {
      }

      template <int x, int y>
        arma::fmat::fixed<x,y>& Matrix<x,y>::operator*() noexcept
      {
        return this->internalMatrix;
      }

      template <int x, int y>
        const arma::fmat::fixed<x,y>& Matrix<x,y>::operator*() const noexcept
      {
        return this->internalMatrix;
      }

      template <int x, int y>
        arma::fmat::fixed<x,y>* Matrix<x,y>::operator->() noexcept
      {
        return &this->internalMatrix;
      }

      template <int x, int y>
        const arma::fmat::fixed<x,y>* Matrix<x,y>::operator->() const noexcept
      {
        return &this->internalMatrix;
      }

      // Matrix lua functions
      struct MatrixAddFunction
      {
        static const std::string name;
      };

      template <int x, int y>
        void Matrix<x,y>::pushAdd(lua_State* state)
      {
        Lua::StackChecker checker(state, 1, "Matrix::pushAdd");
        Lua::getMetatable<Matrix<x,y> >(state);
        lua_pushcclosure(state, Matrix<x,y>::luaAdd, 1);
      }

      template <int x, int y>
        int Matrix<x,y>::luaAdd(lua_State* state)
      {
        // required because of boost's macro usage in mpl::vector
        typedef Matrix<x,y> CurrentMatrix;
        Lua::SignatureChecker<
          boost::mpl::vector<CurrentMatrix>,
          CurrentMatrix, MatrixAddFunction,
          boost::mpl::vector<CurrentMatrix>
            > sigChecker(state);

        lua_pushnil(state);
        lua_copy(state, lua_upvalueindex(1), -1);
        new (Lua::allocate<CurrentMatrix>(state)) CurrentMatrix(Lua::get<CurrentMatrix>(state, 1).internalMatrix + Lua::get<CurrentMatrix>(state, 2).internalMatrix);

        return sigChecker.resultCount;
      }

      struct MatrixSubtractFunction
      {
        static const std::string name;
      };

      template <int x, int y>
        void Matrix<x,y>::pushSubtract(lua_State* state)
      {
        Lua::StackChecker checker(state, 1, "Matrix::pushSubtract");
        Lua::getMetatable<Matrix<x,y> >(state);
        lua_pushcclosure(state, Matrix<x,y>::luaSubtract, 1);
      }

      template <int x, int y>
        int Matrix<x,y>::luaSubtract(lua_State* state)
      {
        typedef Matrix<x,y> CurrentMatrix;
        Lua::SignatureChecker<
          boost::mpl::vector<CurrentMatrix>,
          CurrentMatrix, MatrixSubtractFunction,
          boost::mpl::vector<CurrentMatrix>
            > sigChecker(state);

        lua_pushnil(state);
        lua_copy(state, lua_upvalueindex(1), -1);
        new (Lua::allocate<CurrentMatrix>(state)) CurrentMatrix(Lua::get<CurrentMatrix>(state, 1).internalMatrix + -Lua::get<CurrentMatrix>(state, 2).internalMatrix);

        return sigChecker.resultCount;
      }

      struct MatrixMultiplyFunction
      {
        static const std::string name;
      };

      template <int x, int y>
        void Matrix<x,y>::pushMultiply(lua_State* state)
      {
        Lua::StackChecker checker(state, 1, "Matrix::pushMultiply");
        Lua::getMetatable<Matrix<x,1> >(state);
        Lua::getMetatable<Matrix<x,2> >(state);
        Lua::getMetatable<Matrix<x,3> >(state);
        Lua::getMetatable<Matrix<x,4> >(state);
        lua_pushcclosure(state, Matrix<x,y>::luaMultiply, 4);
      }

      template <int x1, int y1x2, int y2>
        void doMultiply(lua_State* state)
      {
        Lua::StackChecker checker(state, 1, "doMultiply");
        lua_pushnil(state);
        lua_copy(state, lua_upvalueindex(y2), -1);
        new (Lua::allocate<Matrix<x1,y2> >(state)) Matrix<x1,y2>(*Lua::get<Matrix<x1,y1x2> >(state, 1) * *(Lua::get<Matrix<y1x2,y2> >(state, 2)));
      }

      extern const std::string matrixName;

      template <int x, int y>
        int Matrix<x,y>::luaMultiply(lua_State* state)
      {
        typedef Matrix<x,y> CurrentMatrix;
        Lua::SignatureChecker< // can't use most functions here because multiply is 
          boost::mpl::vector<>, // a very diverse function
          CurrentMatrix, MatrixMultiplyFunction,
          boost::mpl::vector<>
            > sigChecker(state);

        // find what the other matrix is
        // make sure it has as many rows as this matrix has columns
        std::string parameterTypename = Lua::getTypename(state, 2);
        if (parameterTypename.substr(0, matrixName.length()) != matrixName || parameterTypename[parameterTypename.length() - 3] != static_cast<char>(x + '0'))
        {
          std::stringstream desiredTypeStream;
          desiredTypeStream << matrixName << y << "x" << "?";
          Lua::BadTypenameException ex(desiredTypeStream.str(), parameterTypename, 2);
          lua_pushstring(state, ex.what());
          lua_error(state);
        }
        else
        {
          // we have a good parameter
          char otherMatrixY = parameterTypename[parameterTypename.length() - 1];
          otherMatrixY -= '0'; // turn it into a zero-at-zero representation
          assert(otherMatrixY > 0 && otherMatrixY <= 4);

          switch (otherMatrixY)
          {
          case 1:
            doMultiply<x,y,1>(state);
            break;
          case 2:
            doMultiply<x,y,2>(state);
            break;
          case 3:
            doMultiply<x,y,3>(state);
            break;
          case 4:
            doMultiply<x,y,4>(state);
            break;
          default:
            assert(false); // this assert should have already been caught by the earlier assert
            break;
          }
        }

        return 1;
      }

      struct MatrixInvertFunction
      {
        static const std::string name;
      };

      template <int x,int y>
        void Matrix<x,y>::pushInvert(lua_State* state)
      {
        Lua::StackChecker checker(state, 1, "Matrix::pushInvert");
        Lua::getMetatable<Matrix<x,y> >(state);
        lua_pushcclosure(state, Matrix<x,y>::luaInvert, 1);
      }

      template <int x,int y>
        int Matrix<x,y>::luaInvert(lua_State* state)
      {
        typedef Matrix<x,y> CurrentMatrix;
        Lua::SignatureChecker<
          boost::mpl::vector<CurrentMatrix>,
          CurrentMatrix, MatrixInvertFunction,
          boost::mpl::vector<>
            > sigChecker(state);

        lua_pushnil(state);
        lua_copy(state, lua_upvalueindex(1), -1);
        new (Lua::allocate<CurrentMatrix>(state)) CurrentMatrix(arma::inv(Lua::get<CurrentMatrix>(state, 1).internalMatrix));

        return 1;
      }

      struct MatrixTransposeFunction
      {
        static const std::string name;
      };

      template <int x, int y>
        void Matrix<x,y>::pushTranspose(lua_State* state)
      {
        Lua::StackChecker checker(state, 1, "Matrix::pushTranspose");
        Lua::getMetatable<Matrix<y,x> >(state); // notice that it is y,x not x,y
        lua_pushcclosure(state, Matrix<x,y>::luaTranspose, 1);
      }

      template <int x, int y>
        int Matrix<x,y>::luaTranspose(lua_State* state)
      {
        typedef Matrix<x,y> SourceMatrix;
        typedef Matrix<y,x> DestinationMatrix;
        Lua::SignatureChecker<
          boost::mpl::vector<DestinationMatrix>,
          SourceMatrix, MatrixTransposeFunction,
          boost::mpl::vector<>
            > sigChecker(state);

        lua_pushnil(state);
        lua_copy(state, lua_upvalueindex(1), -1);
        new (Lua::allocate<DestinationMatrix>(state)) DestinationMatrix(Lua::get<SourceMatrix>(state, 1)->t());

        return sigChecker.resultCount;
      }

      // helper functions

      struct MatrixToTableFunction
      {
        static const std::string name;
      };

      template <int x, int y>
        void Matrix<x,y>::pushToTable(lua_State* state)
      {
        Lua::StackChecker checker(state, 1, "Matrix::pushToTable");
        lua_pushcfunction(state, (Matrix<x,y>::luaToTable));
      }

      template <int x, int y>
        int Matrix<x,y>::luaToTable(lua_State* state)
      {
        typedef Matrix<x,y> CurrentMatrix;
        Lua::SignatureChecker<
          boost::mpl::vector<Lua::NativeTypeWrapper<LUA_TTABLE> >,
          CurrentMatrix, MatrixToTableFunction,
          boost::mpl::vector<>
            > sigChecker(state);

        // output the matrix row by row into separate tables
        CurrentMatrix& matrix(Lua::get<CurrentMatrix>(state, 1));
        lua_createtable(state, y, 0);
        for (unsigned char currentY = 0; currentY < y; ++currentY)
        {
          lua_createtable(state, x, 0);

          for (unsigned char currentX = 0; currentX < x; ++currentX)
          {
            lua_pushnumber(state, matrix.internalMatrix(currentX, currentY));
            lua_rawseti(state, -2, currentX + 1);
          }

          lua_rawseti(state, -2, currentY + 1);
        }

        return sigChecker.resultCount;
      }
    }
  }
}

