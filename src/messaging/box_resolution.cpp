#ifndef _MESSAGING_BOX_RESOLUTION_H_
#define _MESSAGING_BOX_RESOLUTION_H_

// defines a function that can turn some lua objects into C++ storage wrappers (boxes) that
// can be used to transfer data between lua states

#include "messaging.h"
#include "native_boxes.h"
#include "lua_shard.h"
#include <cassert>

namespace WhiteShard
{
  namespace Messaging
  {
    BoxPtr toBoxPtr(lua_State* state, int boxPtrMetatableIndex)
    {
      Lua::StackChecker checker(state, 0, "Messaging's toBoxPtr");
      boxPtrMetatableIndex = lua_absindex(state, boxPtrMetatableIndex);

      BoxPtr returnPtr;
      switch (lua_type(state, -1))
      {
      case LUA_TNIL:
        returnPtr = BoxPtr(new NilBox());
        break;
      case LUA_TBOOLEAN:
        returnPtr = BoxPtr(new BooleanBox());
        break;
      case LUA_TNUMBER:
        returnPtr = BoxPtr(new NumberBox());
        break;
      case LUA_TSTRING:
        returnPtr = BoxPtr(new StringBox());
        break;
      case LUA_TUSERDATA:
        // get the box getter function
        // userdata should always have a metatable
        lua_getmetatable(state, -1);
        assert(1 == lua_istable(state, -1));
        lua_rawgeti(state, -1, Registry::Box);

        if (1 == lua_iscfunction(state, -1))
        {
          // get the boxPtr metatable
          lua_pushnil(state);
          lua_copy(state, boxPtrMetatableIndex, -1);

          // now perform the call
          assert(1 == lua_iscfunction(state, -2));
          if (LUA_OK != lua_pcall(state, 1, 1, 0))
          {
            lua_replace(state, -2);
            lua_error(state);
          }

          returnPtr = std::move(Lua::get<BoxPtr>(state, -1));
          lua_pop(state, 2);
        }
        else
        {
          lua_pop(state, 1);
          lua_getfield(state, -1, "__tname");
          const char* typeCharStar = lua_tostring(state, -1);

          std::string errorString("attempted to move a userdata value without a box, was of type \"");
          errorString
            .append(nullptr != typeCharStar ? typeCharStar : "")
            .append("\"");

          // need to pop this late otherwise there exists a possibility for lua to reclaim the string
          lua_pop(state, 2);
          lua_pushstring(state, errorString.c_str());
          lua_error(state);
        }
        break;
      case (LUA_TTABLE):
        returnPtr = BoxPtr(new TableBox(boxPtrMetatableIndex));
        break;
      default:
        lua_pushstring(state, "attempted to move a native type without a box, don't try to move functions or lua threads");
        lua_error(state);
        break;
      }
      returnPtr->extract(state);
      
      return returnPtr;
    }
  }
}

#endif
