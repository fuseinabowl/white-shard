#include "subsystem.h"
#include <boost/mpl/vector.hpp>

namespace WhiteShard{
  namespace Messaging{
    void Subsystem::pushKillState(lua_State* state)
    {
      // get the engine pointer
      lua_getfield(state, LUA_REGISTRYINDEX, Registry::WhiteShard.c_str());
      lua_rawgeti(state, -1, Registry::Engine);
      // put it at the top of the stack, removing all elements that are no longer needed
      lua_replace(state, -2);
      // push the closure
      lua_pushcclosure(state, Subsystem::luaKillState, 1);
    }

    struct KillStateFunction
    {
      static const std::string& name;
    };
    const std::string& KillStateFunction::name(Subsystem::killState);

    int Subsystem::luaKillState(lua_State* state)
    {
      Lua::SignatureChecker<
        boost::mpl::vector<>::type,
        Lua::StatePtr, KillStateFunction,
        boost::mpl::vector<>::type
          > sigChecker(state);

      Lua::StatePtr& statePtr = Lua::get<Lua::StatePtr>(state, 1);

      // get rid of all parameters and variables
      lua_settop(state, 0);

      Lua::get<Engine*>(state, lua_upvalueindex(1))->killHandle(*statePtr);

      assert(lua_gettop(state) == 0);
      return 0;
    }
  }
}
