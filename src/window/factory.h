#ifndef _WINDOW_FACTORY_H_
#define _WINDOW_FACTORY_H_

#include "engine.h"
#include "engine/subsystem_declaration.h"

class lua_State;

namespace WhiteShard{
  namespace Window{
    class Factory : public WhiteShard::SubsystemFactory
    {
      SubsystemPtrCollection* subsystems;
     public:
      Factory(SubsystemPtrCollection&) noexcept;

      static void createMetatable(lua_State*) noexcept;
      static int createSubsystem(lua_State*);
    };
  }
}

#endif
