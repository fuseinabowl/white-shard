#include "exceptions.h"

namespace WhiteShard
{
  namespace Messaging
  {
    LoadFileException::LoadFileException() :
      Lua::MessageException()
    {}

    LoadFileException::~LoadFileException() noexcept
    {}

    // FileAccessException
    const int FILE_ACCESS_EXCEPTION = 5;

    FileAccessException::FileAccessException(const std::string& filename) :
      LoadFileException()
    {
      this->exceptionString
        .append("could not open file \"")
        .append(filename)
        .append("\"");
    }

    FileAccessException::~FileAccessException() throw()
    {}

    const char* FileAccessException::what() const throw()
    {
      return this->exceptionString.c_str();
    }

    // MemoryException

    CompileMemoryException::CompileMemoryException(const std::string& filename, const std::string& luaErrorString) :
      LoadFileException()
    {
      this->exceptionString
        .append("loading file \"")
        .append(filename)
        .append("\" - memory error\nLua message:\n")
        .append(luaErrorString);
    }

    CompileMemoryException::~CompileMemoryException() throw()
    {}

    const char* CompileMemoryException::what() const throw()
    {
      return this->exceptionString.c_str();
    }

    // SyntaxException

    SyntaxException::SyntaxException(const std::string& filename, const std::string& luaErrorString) :
      LoadFileException()
    {
      this->exceptionString
        .append("loading file \"")
        .append(filename)
        .append("\" - syntax error\nLua message:\n")
        .append(luaErrorString);
    }

    SyntaxException::~SyntaxException() throw()
    {}

    const char* SyntaxException::what() const throw()
    {
      return this->exceptionString.c_str();
    }
  }
}
