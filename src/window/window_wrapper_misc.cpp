#include "window_wrapper.h"
#include "window_wrapper_functions.h"
#include "lua_shard.h"
#include <boost/mpl/vector.hpp>
#include "window/graphics/openGL.h"
#include "window/graphics/context_wrapper.h"

namespace WhiteShard{
  namespace Window{
    template <>
      const std::string FunctionName<setMouseCursorVisibility>::name("setMouseVisibility");
    template <>
      const std::string FunctionName<setVSync>::name("setVSync");
    template <>
      const std::string FunctionName<setVisible>::name("setVisible");
    template <>
      const std::string FunctionName<setTitle>::name("setTitle");
    template <>
      const std::string FunctionName<setKeyRepeatEnabled>::name("setKeyRepeat");
    template <>
      const std::string FunctionName<setJoystickThreshold>::name("setJoystickThreshold");
    template <>
      const std::string FunctionName<restartWindow>::name("resize");

    int WindowWrapper::luaSetMouseCursorVisibility(lua_State* state)
    {
      Lua::SignatureChecker<
        boost::mpl::vector<>::type,
        WindowWrapperPtr, FunctionName<setMouseCursorVisibility>,
        boost::mpl::vector<Lua::NativeTypeWrapper<LUA_TBOOLEAN> >::type
          > sigChecker(state);

      Lua::get<WindowWrapperPtr>(state, 1)->getAccess()->setMouseCursorVisible(1 == lua_toboolean(state, 2));

      return sigChecker.resultCount;
    }

    int WindowWrapper::luaSetVSync(lua_State* state)
    {
      Lua::SignatureChecker<
        boost::mpl::vector<>::type,
        WindowWrapperPtr, FunctionName<setVSync>,
        boost::mpl::vector<Lua::NativeTypeWrapper<LUA_TBOOLEAN> >::type
          > sigChecker(state);

      Lua::get<WindowWrapperPtr>(state, 1)->getAccess()->setVerticalSyncEnabled(1 == lua_toboolean(state, 2));

      return sigChecker.resultCount;
    }

    int WindowWrapper::luaSetVisible(lua_State* state)
    {
      Lua::SignatureChecker<
        boost::mpl::vector<>::type,
        WindowWrapperPtr, FunctionName<setVisible>,
        boost::mpl::vector<Lua::NativeTypeWrapper<LUA_TBOOLEAN> >::type
          > sigChecker(state);

      Lua::get<WindowWrapperPtr>(state, 1)->getAccess()->setVisible(1 == lua_toboolean(state, 2));

      return sigChecker.resultCount;
    }

    int WindowWrapper::luaSetTitle(lua_State* state)
    {
      Lua::SignatureChecker<
        boost::mpl::vector<>::type,
        WindowWrapperPtr, FunctionName<setTitle>,
        boost::mpl::vector<Lua::NativeTypeWrapper<LUA_TSTRING> >::type
          > sigChecker(state);

      Lua::get<WindowWrapperPtr>(state, 1)->getAccess()->setTitle(lua_tostring(state, 2));

      return sigChecker.resultCount;
    }

    int WindowWrapper::luaSetKeyRepeatEnabled(lua_State* state)
    {
      Lua::SignatureChecker<
        boost::mpl::vector<>::type,
        WindowWrapperPtr, FunctionName<setKeyRepeatEnabled>,
        boost::mpl::vector<Lua::NativeTypeWrapper<LUA_TBOOLEAN> >::type
          > sigChecker(state);

      Lua::get<WindowWrapperPtr>(state, 1)->getAccess()->setKeyRepeatEnabled(1 == lua_toboolean(state, 2));

      return sigChecker.resultCount;
    }

    int WindowWrapper::luaSetJoystickThreshold(lua_State* state)
    {
      Lua::SignatureChecker<
        boost::mpl::vector<>::type,
        WindowWrapperPtr, FunctionName<setJoystickThreshold>,
        boost::mpl::vector<Lua::NativeTypeWrapper<LUA_TNUMBER> >::type
          > sigChecker(state);

      Lua::get<WindowWrapperPtr>(state, 1)->getAccess()->setJoystickThreshold(lua_tonumber(state, 2));

      return sigChecker.resultCount;
    }

    void WindowWrapper::pushRestartWindow(lua_State* state)
    {
      Lua::StackChecker checker(state, 1, "pushRestartWindow");

      // get the context lock
      lua_getfield(state, LUA_REGISTRYINDEX, Registry::WhiteShard.c_str());
      lua_rawgeti(state, -1, Registry::ContextLock);
      lua_remove(state, -2);

      lua_pushcclosure(state, WindowWrapper::luaRestartWindow, 1);
    }

    int WindowWrapper::luaRestartWindow(lua_State* state)
    {
      Lua::SignatureChecker<
        boost::mpl::vector<>::type,
        WindowWrapperPtr, FunctionName<restartWindow>,
        boost::mpl::vector<
          Lua::NativeTypeWrapper<LUA_TNUMBER>, Lua::NativeTypeWrapper<LUA_TNUMBER>,
          Lua::Optional<Lua::NativeTypeWrapper<LUA_TSTRING> >
            >
          > sigChecker(state);

      unsigned int 
        width = lua_tonumber(state, 2), 
        height = lua_tonumber(state, 3);
      std::string windowTitle;
      const char* windowTitleChar = lua_tostring(state, 4);
      if (nullptr != windowTitleChar)
      {
        windowTitle = windowTitleChar;
      }

      WindowAccess window = std::move(Lua::get<WindowWrapperPtr>(state, 1)->getAccess());
      window->create(
          sf::VideoMode(width, height), 
          windowTitle, sf::Style::Default, 
          sf::ContextSettings(0, 0, 0, 3, 3));
      window->setActive(false);

      Graphics::ContextLockPtr& contextLock = Lua::get<Graphics::ContextLockPtr>(state, lua_upvalueindex(1));
      if (nullptr != contextLock)
      {
        contextLock->relock();
      }

      return sigChecker.resultCount;
    }

    template <>
      const std::string FunctionName<restartFullscreenWindow>::name("resizeFullscreen");

    void WindowWrapper::pushRestartFullscreenWindow(lua_State* state)
    {
      Lua::StackChecker checker(state, 1, "pushRestartFullscreenWindow");

      lua_getfield(state, LUA_REGISTRYINDEX, Registry::WhiteShard.c_str());
      lua_rawgeti(state, -1, Registry::ContextLock);
      lua_remove(state, -2);

      lua_pushcclosure(state, WindowWrapper::luaRestartFullscreenWindow, 1);
    }

    int WindowWrapper::luaRestartFullscreenWindow(lua_State* state)
    {
      Lua::SignatureChecker<
        boost::mpl::vector<>,
        WindowWrapperPtr, FunctionName<restartFullscreenWindow>,
        boost::mpl::vector<sf::VideoMode, Lua::Optional<Lua::NativeTypeWrapper<LUA_TSTRING> > >
          > sigChecker(state);

      std::string windowTitle;
      const char* windowTitleChar = lua_tostring(state, 3);

      if (nullptr != windowTitleChar)
      {
        windowTitle = windowTitleChar;
      }

      WindowAccess window(std::move(Lua::get<WindowWrapperPtr>(state, 1)->getAccess()));
      window->create(Lua::get<sf::VideoMode>(state, 2), windowTitle, sf::Style::Fullscreen, sf::ContextSettings(0, 0, 0, 3, 3));
      window->setActive(false);

      Graphics::ContextLockPtr& contextLock(Lua::get<Graphics::ContextLockPtr>(state, lua_upvalueindex(1)));

      if (nullptr != contextLock)
      {
        contextLock->relock();
      }

      return sigChecker.resultCount;
    }
  }
}
