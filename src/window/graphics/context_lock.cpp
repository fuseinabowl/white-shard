#include "lua_shard.h"
#include "context_wrapper.h"
#include <algorithm>
#include "subsystem.h"

namespace WhiteShard{
  namespace Lua{
    template <>
      const std::string& LuaTraits<Window::Graphics::ContextLockPtr>::getName() noexcept
    {
      static const std::string name("context_lock");
      return name;
    }
  }

  namespace Window{
    namespace Graphics{

      const unsigned char CACHE_MAX_IMPORTANCE = 1;

      // ContextFreer methods

      ContextFreer::ContextFreer(ContextLockPtr& lockPtr) noexcept :
        contextLockPtrToNullify(&lockPtr)
      {
      }

      ContextFreer::~ContextFreer() noexcept
      {
        if (this->contextLockPtrToNullify != nullptr)
        {
          this->contextLockPtrToNullify->reset();
        }
      }

      ContextFreer::ContextFreer(ContextFreer&& rhs) noexcept :
        contextLockPtrToNullify(rhs.contextLockPtrToNullify)
      {
        rhs.contextLockPtrToNullify = nullptr;
      }

      ContextFreer& ContextFreer::operator=(ContextFreer&& rhs) noexcept
      {
        this->contextLockPtrToNullify = rhs.contextLockPtrToNullify;
        rhs.contextLockPtrToNullify = nullptr;

        return *this;
      }

      // contextLock methods
      
      ContextLock::ContextLock() noexcept
      {
      }

      ContextLock::~ContextLock() noexcept
      {
        this->readLocks.clear();
        this->writeLocks.clear();
      }

      // context entry commands
      void ContextLock::doPerContextCommands() noexcept
      {
        auto& contextWrapper = this->getContextWrapper();
        std::unique_lock<std::mutex> commandsLock(contextWrapper.commandsAccess);

        for (const std::shared_ptr<PerContextCommand>& commandPtr : contextWrapper.commands)
        {
          commandsLock.unlock();
          (*commandPtr)(*this);
          commandsLock.lock();
        }

        contextWrapper.commands.clear();
      }

      // accessing methods

      void ContextLock::readLock(const std::shared_ptr<const SyncObject>& syncTarget) noexcept
      {
        if (this->readLocks.find(syncTarget.get()) == this->readLocks.end() && this->writeLocks.find(syncTarget.get()) == this->writeLocks.end())
        {
          // if the mutex is not already locked by this context/thread pair, lock it
          this->readLocks.insert(std::make_pair(syncTarget.get(), ReadSyncLock(syncTarget)));
        }
      }

      void ContextLock::writeLock(const std::shared_ptr<SyncObject>& syncTarget) noexcept
      {
        if (this->writeLocks.find(syncTarget.get()) == this->writeLocks.end())
        {
          auto readIterator = this->readLocks.find(syncTarget.get());

          if (this->readLocks.find(syncTarget.get()) == this->readLocks.end())
          {
            this->writeLocks.insert(std::make_pair(syncTarget.get(), WriteSyncLock(syncTarget)));
          }
          else
          {
            // upgrade the read lock to a write lock
            // first, move the read lock into a write lock, effectively upgrading it
            assert(readIterator->first == syncTarget.get());
            this->writeLocks.insert(std::make_pair(syncTarget.get(), WriteSyncLock(std::move(readIterator->second))));
            // then remove the read lock from the read lock collection
            this->readLocks.erase(readIterator);
          }
        }
      }

      void ContextLock::constructorLock(const std::shared_ptr<SyncObject>& syncTarget) noexcept
      {
        this->writeLocks.insert(std::make_pair(syncTarget.get(), WriteSyncLock(syncTarget, WriteSyncLock::Constructor_t())));
      }

      GLuint ContextLock::getFBOName() const noexcept
      {
        return this->getContextWrapper().FBOName;
      }

      // VAO management
      //
      // NoVAOException method definitions
      NoVAOException::~NoVAOException() noexcept
      {
      }

      const char* NoVAOException::what() const noexcept
      {
        return "programming error - this should be handled internally by Mesh";
      }

      // ContextLock methods again
      // TODO make ContextLock manage binding VAOs and creating and deleting them
      GLuint ContextLock::getVAOName(const Mesh& mesh) const throw (NoVAOException)
      {
        auto findIterator = this->getContextWrapper().meshVAOs.find(&mesh);

        if (findIterator == this->getContextWrapper().meshVAOs.end())
        {
          throw NoVAOException();
        }

        return findIterator->second;
      }

      void ContextLock::registerVAOName(const Mesh& mesh, GLuint VAOName) const noexcept
      {
        assert(this->getContextWrapper().meshVAOs.find(&mesh) == this->getContextWrapper().meshVAOs.end());

        this->getContextWrapper().meshVAOs.insert(std::make_pair(&mesh, VAOName));
      }

      void ContextLock::deregisterVAOName(const Mesh& mesh) const noexcept
      {
        ContextWrapperBase& contextWrapper (this->getContextWrapper());

        assert(contextWrapper.meshVAOs.find(&mesh) != contextWrapper.meshVAOs.end());

        contextWrapper.meshVAOs.erase(contextWrapper.meshVAOs.find(&mesh));
      }

      // Texture to TextureUnit methods

      // exception definition
      UnsupportedNumberOfTexturesException::UnsupportedNumberOfTexturesException(GLuint texturesPassedIn, GLuint maxTextures) noexcept
      {
        std::stringstream errorStream;
        errorStream
          << "the program attempted to cache more textures than are supported by the hardware." << std::endl 
          << texturesPassedIn << " were cached but the hardware only supports " << maxTextures;
        errorMessage = errorStream.str();
      }

      const char* UnsupportedNumberOfTexturesException::what() const noexcept
      {
        return this->errorMessage.c_str();
      }

      UnsupportedNumberOfTexturesException::~UnsupportedNumberOfTexturesException() noexcept
      {
      }

      std::vector<GLuint> ContextLock::cacheTextures(const std::vector<const Texture*>& texturePtrs) throw (UnsupportedNumberOfTexturesException)
      {
        auto& contextWrapper = this->getContextWrapper();

        if (texturePtrs.size() > contextWrapper.subsystem->getTextureUnits())
        {
          throw UnsupportedNumberOfTexturesException(texturePtrs.size(), contextWrapper.subsystem->getTextureUnits());
        }

        std::vector<GLuint> outputLocations(texturePtrs.size());

        // separate elements that are already cached from those which need to be cached
        std::vector<std::pair<const Texture*, std::vector<GLuint>::iterator> > toBeCached;
        std::vector<GLuint>::iterator outputIterator = outputLocations.begin();

        std::unordered_map<const Texture*, std::vector<ContextWrapperBase::TextureUnit>::iterator>::iterator findIterator;

        std::unique_lock<std::mutex> cacheLock(contextWrapper.cacheAccess);

        for (const Texture* curTex : texturePtrs)
        {
          findIterator = contextWrapper.textureUnitLocations.find(curTex);

          if (findIterator != contextWrapper.textureUnitLocations.end())
          {
            // the texture is currently cached
            // reset the cache importance value
            findIterator->second->second = CACHE_MAX_IMPORTANCE;
            // populate the output vector with the texture unit (the distance between the 
            // start of the vector and the iterator stored in the map)
            *outputIterator = std::distance(contextWrapper.textureUnits.begin(), findIterator->second);
          }
          else
          {
            // the texture has not been cached yet
            toBeCached.push_back(std::make_pair(curTex, outputIterator));
          }

          ++outputIterator;
        }

        // now cache all the uncached but required textures
        // first check to see if there are any unused units (these will not show up in the
        // textureUnitLocations field)
        auto currentTexIter = toBeCached.begin();
        auto& currentUnitIter = contextWrapper.currentUnitIterator;

        bool cached;
        if (currentTexIter != toBeCached.end())
        {
          do
          {
            cached = false;
            do
            {
              if (0 == currentUnitIter->second)
              {
                // the unit can be replaced
                // first, remove the texture that's currently in there
                if (nullptr != currentUnitIter->first)
                {
                  contextWrapper.textureUnitLocations.erase(contextWrapper.textureUnitLocations.find(currentUnitIter->first));
                }

                // now insert the new texture
                contextWrapper.textureUnitLocations.insert(std::make_pair(currentTexIter->first, currentUnitIter));
                // set the unit stats
                currentUnitIter->first = currentTexIter->first;
                currentUnitIter->second = CACHE_MAX_IMPORTANCE;

                // set the output so that the caller knows which unit to use
                *currentTexIter->second = std::distance(contextWrapper.textureUnits.begin(), currentUnitIter);

                // finally bind the texture to an OpenGL texture unit
                glActiveTexture(GL_TEXTURE0 + *currentTexIter->second);
                glBindTexture(GL_TEXTURE_2D, currentTexIter->first->getTexName());

                cached = true;
              }
              else
              {
                // decrement the importance and continue
                --(currentUnitIter->second);
              }

              // use the vector as a cyclic vector
              if (++currentUnitIter == contextWrapper.textureUnits.end())
              {
                currentUnitIter = contextWrapper.textureUnits.begin();
              }
            }
            while(!cached);
          }
          while (++currentTexIter != toBeCached.end());
        }

        return std::move(outputLocations);
      }

      void ContextLock::activateProgram(GLuint programName) noexcept
      {
        ContextWrapperBase& contextWrapper = this->getContextWrapper();
        if (contextWrapper.currentProgram != programName)
        {
          glUseProgram(programName);
          contextWrapper.currentProgram = programName;
        }
      }

      inline void internalSetViewport(GLsizei& viewWidth, GLsizei& viewHeight, GLsizei inWidth, GLsizei inHeight)
      {
        if (viewWidth != inWidth || viewHeight != inHeight)
        {
          glViewport(0, 0, inWidth, inHeight);
          viewWidth = inWidth;
          viewHeight = inHeight;
        }
      }

      void ContextLock::setViewport(Texture& texture) noexcept
      {
        ContextWrapperBase& contextWrapper = this->getContextWrapper();
        auto texSize = texture.getSize();
        internalSetViewport(contextWrapper.viewportWidth, contextWrapper.viewportHeight, texSize.first, texSize.second);
      }

      void ContextLock::setViewport() noexcept
      {
        ContextWrapperBase& contextWrapper = this->getContextWrapper();
        internalSetViewport(contextWrapper.viewportWidth, contextWrapper.viewportHeight, contextWrapper.contextWidth, contextWrapper.contextHeight);
      }

      void ContextLock::ensurePointerValid(const std::string& functionName, const ContextLockPtr& contextLock, lua_State* state)
      {
        if (nullptr == contextLock.get())
        {
          std::stringstream errorStream;
          errorStream <<
            "error in \"" << functionName << "\" - no context was locked before the function was called" << std::endl;
          lua_pushstring(state, errorStream.str().c_str());
          lua_error(state);
        }
      }
    }
  }
}
