local initialWindow = WhiteShard.window:createWindow(100, 100, "tex test");
local graphics = WhiteShard.window.graphics;
graphics:lockContext(initialWindow);

local vertShader, vertLog = graphics:createShader(WhiteShard.window.graphics.shaderTypes.vertex, "\
#version 330\
\
layout(location = 0) in vec2 inPos;\
uniform vec4 locationParams;\
out vec2 pos;\
\
void main()\
{\
  gl_Position = vec4(((inPos * locationParams.xy + locationParams.zw) * 2.0 - 1.0), 0.0, 1.0);\
  pos = inPos;\
}\
");

if (not vertShader) then
  print(vertLog);
else
  print("vert success\n", vertLog);
end;

local fragShader, fragLog = graphics:createShader(WhiteShard.window.graphics.shaderTypes.fragment, "\
#version 330\
\
in vec2 pos;\
layout (location = 0) out vec4 colour;\
uniform sampler2D image;\
uniform vec4 locationParams;\
\
void main()\
{\
  colour = texture2D(image, pos);\
}\
");

if (not fragShader) then
  print(fragLog);
else
  print("frag success\n", fragLog);
end;

local shader, shaderLog = WhiteShard.window.graphics:createShaderProgram({vertShader, fragShader});

if (not shader) then
  print(shaderLog);
else
  print("shader success\n", shaderLog);
end;

print("loading texture");
local northernLights = WhiteShard.window.graphics:loadTexture("../../Pictures/Northern_Lights.jpg", 
  WhiteShard.window.graphics.wrapTypes.mirroredRepeated, WhiteShard.window.graphics.wrapTypes.repeated,
  WhiteShard.window.graphics.magnificationFilters.nearest, WhiteShard.window.graphics.minificationFilters.linear,
  WhiteShard.window.graphics.anisotropicFilters[8]);

local imageUniform = shader:getUniform("image");
local positionUniform = shader:getUniform("locationParams");

local northernPosition = graphics:createMatrix({{1.0,-1.0, 0.0,1.0}});

local unitMesh = WhiteShard.window.graphics.unitMesh;

print("finished initialisation");

local handler = WhiteShard.window:createEventHandler();
local secondaryTex = WhiteShard.window.graphics:createTexture(1024,1024, nil,nil, WhiteShard.window.graphics.magnificationFilters.nearest, WhiteShard.window.graphics.minificationFilters.linearMipmapLinear, WhiteShard.window.graphics.anisotropicFilters[8.0]);

handler:setHandler("closed", function()
  WhiteShard.messaging:getThisThread():kill()
end);

local func;
do
  local curRes = 1;

  func = function ()
    curRes = curRes + 1;

    print(curRes);

    secondaryTex = WhiteShard.window.graphics:createTexture(512,512, nil,nil, WhiteShard.window.graphics.magnificationFilters.nearest,WhiteShard.window.graphics.minificationFilters.nearest, WhiteShard.window.graphics.anisotropicFilters[currentAnisotropy]);
  end;
  handler:setHandler("keyPressed", func);
end;

do
  local timeStep = 1/60;
  local nextTime = WhiteShard:getTime() + timeStep;
  WhiteShard:setPriorityFunction(function (jobs)
    if nextTime < WhiteShard:getTime() then
      WhiteShard.messaging:getThisThread():sendMessage();
      nextTime = nextTime + timeStep;
      return jobs + 1;
    else
      return jobs;
    end;
  end);
end;

WhiteShard.messaging:setMessageHandler(function ()
  WhiteShard.window.graphics:lockContext(initialWindow);
  func();
  handler:handleEvents(initialWindow);
  shader:draw({}, secondaryTex, unitMesh, {3}, {
    {[imageUniform] = northernLights, [positionUniform] = northernPosition}
      });
  shader:draw({}, nil, unitMesh, {3}, {
    {[imageUniform] = secondaryTex}
      });
  WhiteShard.window.graphics:render();
end);
