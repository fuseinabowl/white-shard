// defines the selection of subsystems for use in initialising the program
// compartmentalise because it's a hard coded collection, so should be used in program configuration
// rather than anything else
#include "lua_shard.h"
#include "window.h"
#include "messaging.h"
#include "engine.h"
#include "window/factory.h"
#include <cassert>

namespace WhiteShard{
  void Engine::populateSubsystemFactories(lua_State* state)
  {
    Lua::StackChecker(state, 0, "populateSubsystemFactories");

    lua_pushglobaltable(state);

    lua_pushstring(state, "messaging");
    Messaging::SubsystemFactory::createMetatable(state);
    new (Lua::allocate<Messaging::SubsystemFactory>(state)) Messaging::SubsystemFactory(this->subsystems, *this);
    lua_rawset(state, -3);

    lua_pushstring(state, "window");
    Window::Factory::createMetatable(state);
    new (Lua::allocate<Window::Factory>(state)) Window::Factory(this->subsystems);
    lua_rawset(state, -3);

    // pop the global table at the end
    lua_pop(state, 1);
  }
}
