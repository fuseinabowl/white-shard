#ifndef _EVENT_HANDLER_H_
#define _EVENT_HANDLER_H_

#include <string>
#include <unordered_map>

class lua_State;

namespace WhiteShard{
  namespace Window{
    // don't need a pointer wrapper because the event handler
    // cannot be moved between states

    class EventHandler
    {
     public:
      typedef std::unordered_map<std::string, int> EventMap;
     private:
      static const EventMap eventMap;
      static int luaCreateEventHandler(lua_State*);
      static int luaSetHandler(lua_State*); 
      static int luaGetHandler(lua_State*);
      static int luaHandleEvents(lua_State*);
     public:
      static void pushCreateEventHandler(lua_State*);
    };
  }
}

#endif
