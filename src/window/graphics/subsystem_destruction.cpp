#include "subsystem.h"

namespace WhiteShard{
  namespace Window{
    namespace Graphics{
      void Subsystem::pushDestructionCommand(DestructionCommandPtr&& commandPtr) noexcept
      {
        this->objectDestructor.pushDestructionCommand(std::move(commandPtr));
      }
    }
  }
}
