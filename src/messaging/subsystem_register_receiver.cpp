#include "subsystem.h"
#include <boost/mpl/vector.hpp>

namespace WhiteShard{
  namespace Messaging{
    void Subsystem::pushRegisterReceiver(lua_State* state)
    {
      Lua::StackChecker checker(state, 1, "Messaging::pushRegisterReceiver");

      // allow quick access to the white shard registry
      lua_getfield(state, LUA_REGISTRYINDEX, Registry::WhiteShard.c_str());
      lua_pushcclosure(state, Subsystem::luaRegisterReceiver, 1);
    }

    struct RegisterReceiverFunction
    {
      static const std::string& name;
    };
    const std::string& RegisterReceiverFunction::name(Subsystem::registerReceiver);

    int Subsystem::luaRegisterReceiver(lua_State* state)
    {
      Lua::SignatureChecker<
        boost::mpl::vector<>::type,
        SubsystemLuaPtr, RegisterReceiverFunction,
        boost::mpl::vector<Lua::NativeTypeWrapper<LUA_TFUNCTION> >::type
          > sigChecker(state);

      lua_rawseti(state, lua_upvalueindex(1), Registry::MessagingReceiver);

      return sigChecker.resultCount;
    }
  }
}
