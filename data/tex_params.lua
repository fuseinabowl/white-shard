local slave = WhiteShard.messaging:createThread("data/tex_params_slave.lua");

print("sending message");
slave:sendMessage(WhiteShard.window.graphics.magnificationFilters.linear);

WhiteShard.messaging:getThisThread():kill();
