#ifndef _CONTEXT_WRAPPER_BASE_H_
#define _CONTEXT_WRAPPER_BASE_H_

#include <thread>
#include "window/graphics/openGL.h"
#include <SFML/Window.hpp>
#include <memory>
#include <unordered_map>
#include "window/window_wrapper.h"
#include "lua/temporary_object.h"
#include "sync_object.h"
#include "per_context_command.h"
#include "mesh.h"
#include "texture.h"

namespace WhiteShard{
  namespace Window{
    class WindowWrapper;

    namespace Graphics{
      class Subsystem;

      class SyncObject;

      class Mesh;

      class NoVAOException : public std::exception
      {
       public:
        ~NoVAOException() noexcept;
        const char* what() const noexcept;
      };

      class UnsupportedNumberOfTexturesException : public std::exception
      {
        std::string errorMessage;
       public:
        UnsupportedNumberOfTexturesException(GLuint texturesPassedIn, GLuint maxTextures) noexcept;
        ~UnsupportedNumberOfTexturesException() noexcept;
        const char* what() const noexcept;
      };

      class ContextLock;
      typedef std::unique_ptr<ContextLock> ContextLockPtr;

      class ContextWrapperBase
      {
        // data
        const Graphics::Subsystem* subsystem;

        std::mutex commandsAccess;
        std::vector<std::shared_ptr<PerContextCommand> > commands;

        GLuint FBOName;

        std::unordered_map<const Mesh*, GLuint> meshVAOs;
        GLuint currentVAO;

        typedef std::pair<const Texture*, unsigned char> TextureUnit;
        // texture -> textureUnit iterator
        std::mutex cacheAccess;
        std::unordered_map<const Texture*, std::vector<TextureUnit>::iterator> textureUnitLocations;
        std::vector<TextureUnit> textureUnits;
        std::vector<TextureUnit>::iterator currentUnitIterator;

        GLuint currentProgram;

        GLsizei contextWidth;
        GLsizei contextHeight;

        GLsizei viewportWidth;
        GLsizei viewportHeight;

        // methods
       public:
        ContextWrapperBase(const Subsystem&) noexcept;
        virtual ~ContextWrapperBase() noexcept;

        void freeTexture(const Texture*) noexcept;

        void pushPerContextCommand(const std::shared_ptr<PerContextCommand>&) noexcept;
       protected:
        void initialise(GLsizei width, GLsizei height) noexcept; // should be called by inheriting classes in their constructor, after doLock and doUnlock are ready to be called
        void setSize(GLsizei width, GLsizei height) noexcept; // to be called when a context's default framebuffer changes size

       private:
        friend class ContextLock;
      };

      class ContextLock
      {
        typedef std::unordered_map<const SyncObject*, WriteSyncLock> WriteSyncLockMap;
        typedef std::unordered_map<const SyncObject*, ReadSyncLock> ReadSyncLockMap;

        WriteSyncLockMap writeLocks;
        ReadSyncLockMap readLocks;

        // methods
       public:
        ContextLock() noexcept;
        virtual ~ContextLock() noexcept;

        // non-copyable, non-moveable
        ContextLock(const ContextLock&) = delete;
        ContextLock& operator=(const ContextLock&) = delete;

        // user method listing

        // relock will just try to reacquire the context after something has happened that has forced it to be unlocked
        virtual void relock() noexcept = 0;

        // flush all registered commands in the context, then clear the command collection
        void doPerContextCommands() noexcept;

        // tell openGL to render the scene
        // this might have special meaning for some contexts e.g. Windows will swap buffers
        virtual void render() const noexcept = 0;

        // these lock methods are used when using objects in openGL
        // readLock and writeLock should be called before their respective uses
        // on ContextLock exit, the locks will be written automatically
        void readLock(const std::shared_ptr<const SyncObject>& syncTarget) noexcept;
        void writeLock(const std::shared_ptr<SyncObject>& syncTarget) noexcept;

        // constructorLock does not need to do a lookup (as there cannot be a lock on an object that already exists) so is slightly faster but can only be used in constructors
        void constructorLock(const std::shared_ptr<SyncObject>& syncTarget) noexcept;

        // gets the custom framebuffer name (0 is always the framebuffer name if drawing to screen)
        GLuint getFBOName() const noexcept;

        // VAO accessors for faster mesh switching
        GLuint getVAOName(const Mesh&) const throw (NoVAOException);
        void registerVAOName(const Mesh&, GLuint) const noexcept;
        void deregisterVAOName(const Mesh&) const noexcept;

        // TextureUnit mapping
        // the return values are the respective texture units bound
        std::vector<GLuint> cacheTextures(const std::vector<const Texture*>&) throw (UnsupportedNumberOfTexturesException);

        // shader program
        void activateProgram(GLuint programName) noexcept;

        // get the context's size
        void setViewport(Texture&) noexcept; // sets the viewport to the texture's size
        void setViewport() noexcept; // sets the viewport to the context's size

        // ensure the pointer is valid
        static void ensurePointerValid(const std::string&, const std::unique_ptr<ContextLock>&, lua_State*);
       protected:
        // inherited classes override this so that all user methods
        // can be centralised in this class
        virtual ContextWrapperBase& getContextWrapper() const noexcept = 0;
      };

      class PureContextWrapper : public ContextWrapperBase
      {
        sf::Context context;
       public:
        PureContextWrapper(const Subsystem&) noexcept;
        ~PureContextWrapper() noexcept;

        // non-copyable, non-moveable
        PureContextWrapper(const PureContextWrapper&) = delete;
        PureContextWrapper& operator=(const PureContextWrapper&) = delete;

        friend class PureContextLock;
      };

      typedef std::unique_ptr<PureContextWrapper> PureContextWrapperPtr;

      class PureContextLock : public ContextLock
      {
        // data
        PureContextWrapperPtr contextPtr;
        Subsystem* subsystem;

        // methods
        ContextWrapperBase& getContextWrapper() const noexcept;
        void relock() noexcept;
        void render() const noexcept;
       public:
        explicit PureContextLock(PureContextWrapperPtr&&, Subsystem&) noexcept;
        ~PureContextLock() noexcept;
      };

      class WindowContextWrapper : public ContextWrapperBase
      {
        WindowWrapper* windowPtr;
       public:
        WindowContextWrapper(const Subsystem&, WindowWrapper&) noexcept;
        ~WindowContextWrapper() noexcept;

        // non-copyable, non-moveable
        WindowContextWrapper(const WindowContextWrapper&) = delete;
        WindowContextWrapper& operator=(const WindowContextWrapper&) = delete;

        friend class WindowContextLock;
      };

      typedef std::unique_ptr<WindowContextWrapper> WindowContextWrapperPtr;

      class WindowContextLock : public ContextLock
      {
        // data
        WindowContextWrapperPtr contextPtr;
        Subsystem* subsystem;
        WindowWrapperPtr wrapperMaintainer;

        // methods
        ContextWrapperBase& getContextWrapper() const noexcept;
        void relock() noexcept;
        void render() const noexcept;
       public:
        explicit WindowContextLock(WindowContextWrapperPtr&&, Subsystem&, WindowWrapperPtr) noexcept;
        ~WindowContextLock() noexcept;
      };

      class ContextFreer : public Lua::TemporaryObject
      {
        ContextLockPtr* contextLockPtrToNullify;
       public:
        explicit ContextFreer(ContextLockPtr&) noexcept;
        ~ContextFreer() noexcept;

        // non-copyable
        ContextFreer(const ContextFreer&) = delete;
        ContextFreer& operator=(const ContextFreer&) = delete;
        // moveable
        ContextFreer(ContextFreer&&) noexcept;
        ContextFreer& operator=(ContextFreer&&) noexcept;
      };
    }
  }
}

#endif
