#include "subsystem.h"

namespace WhiteShard{
  namespace Window{
    namespace Graphics{
      DestructionCommand::DestructionCommand(SyncObject& syncObject) noexcept :
        syncs(std::move(syncObject.readSyncs))
      {
        syncs.push_back(std::move(syncObject.writeSync));
      }

      DestructionCommand::~DestructionCommand() noexcept
      {
      }

      void DestructionCommand::operator()() noexcept
      {
        // wait on all syncs before performing destruction
        this->syncs.clear();
        this->performDestruction();
      }
    }
  }
}
