#include "subsystem.h"
#include <sstream>

namespace WhiteShard{
  namespace Window{
    namespace Graphics{
      SubsystemConstructorException::SubsystemConstructorException() noexcept
      {}

      SubsystemConstructorException::~SubsystemConstructorException() noexcept
      {}

      // GLEWException methods

      GLEWException::GLEWException(GLenum errorCode) noexcept :
        exceptionString("GLEW failed to initialise: ")
      {
        exceptionString.append(reinterpret_cast<const char*>(glewGetErrorString(errorCode)));
      }

      GLEWException::~GLEWException() noexcept
      {}

      const char* GLEWException::what() noexcept
      {
        return this->exceptionString.c_str();
      }

      // VersionStringException methods

      VersionStringException::VersionStringException() noexcept
      {}

      VersionStringException::~VersionStringException() noexcept
      {}

      const char* VersionStringException::what() noexcept
      {
        static const std::string exceptionString("glGetString failed to produce a parsable output, please check your graphics drivers and try again");
        return exceptionString.c_str();
      }

      // VersionInsufficientException methods

      VersionInsufficientException::VersionInsufficientException(unsigned int _major, unsigned int _minor) noexcept :
        majorVersion(_major), minorVersion(_minor)
      {}

      VersionInsufficientException::~VersionInsufficientException() noexcept
      {}

      const char* VersionInsufficientException::what() noexcept
      {
        std::stringstream exceptionStream;
        exceptionStream << "The OpenGL version on this system is " << this->majorVersion << "." << this->minorVersion << ", but the White-Shard engine requires an OpenGL version of at least "
          << requiredOpenGLMajorVersion << "." << requiredOpenGLMinorVersion << ". Please update your drivers and try again";
        return exceptionStream.str().c_str();
      }
    }
  }
}
