#include "exception.h"
#include <iostream>

namespace WhiteShard{
  namespace Lua{
    MessageException::~MessageException() noexcept
    {}

    LuaException::LuaException(int errorCode, lua_State* state) :
      exceptionType(errorCode)
    {
      this->exceptionString
        .append("error occurred when excuting lua\n");
      switch(this->exceptionType)
      {
      case LUA_ERRRUN:
        this->exceptionString.append("runtime error: ");
        break;
      case LUA_ERRMEM:
        this->exceptionString.append("memory error: ");
        break;
      case LUA_ERRGCMM:
        this->exceptionString.append("garbage collection metamethod failure: ");
        break;
      case LUA_ERRERR:
        this->exceptionString.append("error handler error: ");
        break;
      default:
        this->exceptionString.append("unknown error type: ");
        break;
      }
      this->exceptionString
        .append(lua_isstring(state, -1) == 1 ? lua_tostring(state, -1) : "<no more information>");

      lua_pop(state, 1);
    }

    LuaException::~LuaException() throw()
    {}

    const char * LuaException::what() const throw()
    {
      return this->exceptionString.c_str();
    }

    int LuaException::getExceptionType() const
    {
      return this->exceptionType;
    }

    void observePcall(int pcallReturnCode, lua_State* state) throw (LuaException)
    {
      if (pcallReturnCode != LUA_OK)
      {
        throw LuaException(pcallReturnCode, state);
      }
    }
  }
}
