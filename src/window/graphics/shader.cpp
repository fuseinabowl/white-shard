#include "shader.h"
#include "data_definition.h"
#include "window/graphics/openGL.h"
#include <vector>
#include "lua/temporary_object.h"
#include <iostream>
#include <cassert>
#include "subsystem.h"

namespace WhiteShard{
  namespace Window{
    namespace Graphics{
      // ShaderDestructionCommand definition
      class ShaderDestructionCommand : public DestructionCommand
      {
        GLsync syncObject;
        GLuint shaderName;
       public:
        ShaderDestructionCommand(SyncObject&, GLuint) noexcept;
        ~ShaderDestructionCommand() noexcept;

        ShaderDestructionCommand(const ShaderDestructionCommand&) = delete;
        ShaderDestructionCommand& operator=(const ShaderDestructionCommand&) = delete;

        void performDestruction() noexcept;
      };

      ShaderDestructionCommand::ShaderDestructionCommand(SyncObject& syncObject, GLuint shaderName) noexcept :
        DestructionCommand(syncObject),
        shaderName(shaderName)
      {}

      ShaderDestructionCommand::~ShaderDestructionCommand() noexcept
      {}

      void ShaderDestructionCommand::performDestruction() noexcept
      {
        glDeleteShader(this->shaderName);
      }

      // Shader functions

      Shader::Shader(ShaderType type, const std::string& source, std::string& infoLog, SubsystemPtr subsystem) :
        SyncObject(*subsystem),
        shaderName(glCreateShader(type.shaderType))
      {
        const char* tempSource = source.c_str();
        glShaderSource(this->shaderName, 1, &tempSource, nullptr);
        glCompileShader(this->shaderName);

        infoLog = this->getInfoLog();

        if (!this->isCompiled())
        {
          glDeleteShader(this->shaderName);
          throw ShaderCompilationException();
        }
      }

      Shader::~Shader() noexcept
      {
        this->sendDestructionCommand(DestructionCommandPtr(new ShaderDestructionCommand(*this, this->shaderName)));
      }

      std::string Shader::getInfoLog() const
      {
        GLint infoLogLength;
        glGetShaderiv(this->shaderName, GL_INFO_LOG_LENGTH, &infoLogLength);

        std::vector<GLchar> infoLogBuffer(infoLogLength);
        glGetShaderInfoLog(this->shaderName, infoLogLength, nullptr, &infoLogBuffer.front());

        return std::string(&infoLogBuffer.front());
      }

      bool Shader::isCompiled() const
      {
        GLint isCompiled;
        glGetShaderiv(this->shaderName, GL_COMPILE_STATUS, &isCompiled);
        return isCompiled == GL_TRUE;
      }

      void Shader::attachToProgram(ContextLock& contextLock, const ShaderPtr& shader, GLuint programName)
      {
        contextLock.readLock(shader);

        glAttachShader(programName, shader->shaderName);
      }

      ShaderCompilationException::~ShaderCompilationException() noexcept
      {}

      const char* ShaderCompilationException::what() const noexcept
      {
        static const char* errorString("The shader failed to compile");
        return errorString;
      }
    }
  }
}
