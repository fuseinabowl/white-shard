#ifndef _SHADER_TYPE_H_
#define _SHADER_TYPE_H_

#include "window/graphics/openGL.h"

namespace WhiteShard{
  namespace Window{
    namespace Graphics{
      struct ShaderType
      {
        ShaderType() noexcept;
        ShaderType(GLenum) noexcept;
        GLenum type;
      };
    }
  }
}

#endif
