#ifndef _MATRIX_H_
#define _MATRIX_H_

#include <armadillo>

class lua_State;

namespace WhiteShard{
  namespace Window{
    namespace Graphics{
      template <int x, int y>
        class Matrix
      {
        arma::fmat::fixed<x,y> internalMatrix;
       public:
        Matrix() noexcept;
        Matrix(arma::fmat::fixed<x,y>&&) noexcept;
        ~Matrix() noexcept;

        arma::fmat::fixed<x,y>& operator*() noexcept;
        const arma::fmat::fixed<x,y>& operator*() const noexcept;

        arma::fmat::fixed<x,y>* operator->() noexcept;
        const arma::fmat::fixed<x,y>* operator->() const noexcept;

        // equality not included because I'm using floating point numbers.
        // the equality function would be patchy and unreliable

        // push lua functions
        // arithmetic
        static void pushAdd(lua_State*);
        static void pushSubtract(lua_State*);
        static void pushMultiply(lua_State*);
        static void pushInvert(lua_State*);
        static void pushTranspose(lua_State*);
        // helper functions
        static void pushToTable(lua_State*);

       private:
        // lua functions
        // arithmetic
        static int luaAdd(lua_State*);
        static int luaSubtract(lua_State*);
        static int luaMultiply(lua_State*);
        static int luaInvert(lua_State*);
        static int luaTranspose(lua_State*);
        // helper functions
        static int luaToTable(lua_State*);
      };
    }
  }
}

#include "matrix_fwd.h"

#endif
