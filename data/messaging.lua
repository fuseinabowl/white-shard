local WhiteShard = WhiteShard;
local messaging = WhiteShard.messaging;
local createThread = messaging.createThread;
local timerVal = 3;
local otherThreads = {};
local function countdown()
  -- get a context lock on the window just for testing purposes
  timerVal = timerVal - 1;
  print(timerVal);
  otherThreads[#otherThreads + 1] = createThread(messaging, "data/messaging_aux.lua");

  for _,v in pairs(otherThreads) do
    v:sendMessage({"hello", [2] = "what up", tango = "down", ["squad"] = "eliminated", ["status"] = {"OK","good","injured","killed"}});
  end;

  if (timerVal <= 0) then
    messaging:getThisThread():kill();
    for x,y in pairs(otherThreads) do
      y:kill();
    end
  end
end
messaging:setMessageHandler(countdown);

print(type(5));
print(type(messaging));

-- set up the timer
local getTime = WhiteShard.getTime;
local stepSize = 1.0;
local nextStep = getTime(WhiteShard) + stepSize;
local thisThread = messaging:getThisThread();
local sendMessage = thisThread.sendMessage;
local function getPriority(tasks)
  if tasks > 0 then
    return tasks;
  else
    if getTime(WhiteShard) > nextStep then
      nextStep = nextStep + stepSize;
      sendMessage(thisThread);

      return 1;
    else
      return 0;
    end
  end
end
WhiteShard:setPriorityFunction(getPriority);

print("finished first script");
