#ifndef _LUA_INCLUDE_H_
#define _LUA_INCLUDE_H_

extern "C" {
  #include <lua.h>
  #include <lualib.h>
  #include <lauxlib.h>
}

#endif
