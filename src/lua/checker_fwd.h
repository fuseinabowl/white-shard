#ifndef _LUA_LOCALE_FWD_H_
#define _LUA_LOCALE_FWD_H_

#include "state.h"
#include <cassert>
#include <boost/type_traits.hpp>
#include <boost/mpl/next.hpp>
#include <boost/mpl/begin_end.hpp>
#include <boost/mpl/empty.hpp>
#include <boost/mpl/front.hpp>
#include <boost/utility/enable_if.hpp>

#ifndef NDEBUG
#include <iostream>
#endif

namespace WhiteShard{
  namespace Lua{
    // inline helps optmise out during release builds
#ifndef NDEBUG
    inline StackChecker::StackChecker(lua_State* state, int stackChange, std::string identifier) :
      state(state), expectedStackSize(lua_gettop(state) + stackChange), identifier(std::move(identifier))
    {
    }
#else
    inline StackChecker::StackChecker(lua_State*, int, std::string)
    {}
#endif

    // declare needed classes
    template <int typeEnum>
      class NativeTypeWrapper;

    // exposes the function name from the methodNameClass::name
    template <class ResultsList, class Class, class MethodNameClass, class ParameterList, bool ParamsVariadic>
      const std::string& SignatureChecker<
        ResultsList, Class, MethodNameClass, ParameterList, ParamsVariadic
          >::functionName(MethodNameClass::name);

    // exposes the size of the results list
    template <class ResultsList, class Class, class MethodNameClass, class ParameterList, bool ParamsVariadic>
      const typename boost::mpl::size<ResultsList>::type::value_type SignatureChecker<ResultsList, Class, MethodNameClass, ParameterList, ParamsVariadic>::resultCount = boost::mpl::size<ResultsList>::type::value;

    template <char index, class CurrentIterator, class EndIterator,
        class IteratorsEquivalent = typename boost::is_same<CurrentIterator, EndIterator>::type>
      class CheckIterated;

    template <char index, class CurrentIterator, class EndIterator>
      struct CheckIterated<index, CurrentIterator, EndIterator, boost::false_type>
    {
      static void checkIterated(lua_State* state) throw (BadTypenameException);
    };
    template <char index, class CurrentIterator, class EndIterator>
      void CheckIterated<index, CurrentIterator, EndIterator, boost::false_type>::checkIterated(lua_State* state) throw (BadTypenameException)
    {
      check<typename CurrentIterator::type>(state, index);
      CheckIterated<index + 1, typename boost::mpl::next<CurrentIterator>::type, EndIterator>::checkIterated(state);
    }

    template <char index, class CurrentIterator, class EndIterator>
      struct CheckIterated<index, CurrentIterator, EndIterator, boost::true_type>
    {
      static void checkIterated(lua_State* state) throw ();
    };
    template <char index, class CurrentIterator, class EndIterator>
      void CheckIterated<index, CurrentIterator, EndIterator, boost::true_type>::checkIterated(lua_State*) throw ()
    {
    }

    template <class Results, class Class, class MethodNameClass, class Parameters, bool FinalParamVariadic>
      SignatureChecker<Results, Class, MethodNameClass, Parameters, FinalParamVariadic>
        ::SignatureChecker(lua_State* state) :
          state(state)
    {
      try
      {
        check<Class>(state, 1);
      }
      catch (const BadTypenameException& exception)
      {
        std::stringstream exceptionStream;
        exceptionStream << "could not get self for signiature: "
          << this->constructSigniature()
          << std::endl << "did you forget the ':'?";

        lua_pushstring(state, exceptionStream.str().c_str());
        lua_error(state);
      }

      try
      {
        CheckIterated<2, typename boost::mpl::begin<Parameters>::type, typename boost::mpl::end<Parameters>::type>::checkIterated(state);
      }
      catch (const BadTypenameException& exception)
      {
        // construct the signiature and throw it in the lua state
        std::stringstream exceptionStream;
        exceptionStream << "bad parameter listing for index " << exception.getIndex() - 1
          << ":" << std::endl << "\texpected \"" << exception.getExpectedTypename() << "\" but got \"" << exception.getBadTypename() << "\""
          << std::endl << "for signiature: " << this->constructSigniature();

        lua_pushstring(state, exceptionStream.str().c_str());
        lua_error(state);
      }
    }

#ifndef NDEBUG
    template <class Results, class Class, class MethodNameClass, class Parameters, bool FinalParamVariadic>
      SignatureChecker<Results, Class, MethodNameClass, Parameters, FinalParamVariadic>
        ::~SignatureChecker()
    {
      // check the top of the lua state stack correspond to the Results vector's types
      assert(lua_gettop(this->state) >= SignatureChecker::resultCount);
      try
      {
        CheckIterated<- SignatureChecker::resultCount, typename boost::mpl::begin<Results>::type, typename boost::mpl::end<Results>::type>::checkIterated(this->state);
      }
      catch (const BadTypenameException& exception)
      {
        std::stringstream exceptionStream;
        exceptionStream
          << "bad results returned at " << exception.getIndex()
          << std::endl << "signiature specifies " << exception.getExpectedTypename() << " but function returned " << exception.getBadTypename()
          << std::endl << "for signiature: " << this->constructSigniature();

        // cannot throw a lua error here becuase it would transfer flow to outside of the destructor
        // (should the data be freed or not?)
        // so need to kill the program
        std::cerr << exceptionStream.str();
        raise(SIGABRT);
      }
    }
#endif

    template <class CurrentIterator, class EndIterator>
      inline
        typename boost::enable_if<typename boost::is_same<CurrentIterator, EndIterator>::type>::type
          constructList(std::stringstream&)
    {
    }

    template <class CurrentIterator, class EndIterator>
      inline
        typename boost::disable_if<typename boost::is_same<CurrentIterator, EndIterator>::type>::type 
          constructList(std::stringstream& stream)
    {
      stream << ", " << LuaTraits<typename CurrentIterator::type>::getName();
      constructList<typename boost::mpl::next<CurrentIterator>::type, EndIterator>(stream);
    }

    template <class Collection>
      inline
        typename boost::enable_if<typename boost::mpl::empty<Collection>::type>::type
          toString(std::stringstream& stream)
    {
      stream << LuaTraits<NativeTypeWrapper<LUA_TNIL> >::getName();
    }

    template <class Collection>
      inline
        typename boost::disable_if<typename boost::mpl::empty<Collection>::type>::type 
          toString(std::stringstream& stream)
    {
      stream << LuaTraits<typename boost::mpl::front<Collection>::type>::getName();
      constructList<typename boost::mpl::next<typename boost::mpl::begin<Collection>::type>::type, typename boost::mpl::end<Collection>::type>(stream);
    }

    template <class Results, class Class, class MethodNameClass, class Parameters, bool FinalParamVariadic>
      std::string SignatureChecker<Results, Class, MethodNameClass, Parameters, FinalParamVariadic>::constructSigniature()
    {
      std::stringstream stream;
      toString<Results>(stream);
      stream << " " << LuaTraits<Class>::getName() << ":" << MethodNameClass::name << "(";
      toString<Parameters>(stream);
      stream << ")";
      return stream.str();
    }
  }
}
#endif
