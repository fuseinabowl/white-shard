#include "sync_object.h"
#include <cassert>
#include <iostream>
#include "subsystem.h"
#include "window/graphics/openGL.h"

// this determines the maximum number of read sync objects that can be
// associated with a single SyncObject at a time. When this count is reached,
// the cache is cleared - this prevents SyncObject gobble. 
// OpenGL only supports a certain number of glSyncs, so this helps avoid reaching that limit
const unsigned int MAX_READ_SIZE = 20;

namespace WhiteShard{
  namespace Window{
    namespace Graphics{
      SyncWrapper::SyncWrapper() noexcept :
        sync(0),
        active(false)
      {
      }

      SyncWrapper::SyncWrapper(GLsync sync) noexcept :
        sync(sync),
        active(true)
      {
      }

      SyncWrapper SyncWrapper::generate() noexcept
      {
        return std::move(SyncWrapper(glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0)));
      }

      SyncWrapper::~SyncWrapper() noexcept
      {
        this->kill();
      }

      SyncWrapper::SyncWrapper(SyncWrapper&& rhs) noexcept :
        sync(rhs.sync),
        active(rhs.active)
      {
        rhs.sync = 0;
        rhs.active = false;
      }

      SyncWrapper& SyncWrapper::operator=(SyncWrapper&& rhs) noexcept
      {
        this->kill();

        this->sync = rhs.sync;
        this->active = rhs.active;
        rhs.sync = 0;
        rhs.active = false;

        return *this;
      }

      void SyncWrapper::wait() noexcept
      {
        if (this->active)
        {
          glWaitSync(this->sync, 0, GL_TIMEOUT_IGNORED);
        }
      }

      void SyncWrapper::kill() noexcept
      {
        if (this->active)
        {
          glDeleteSync(this->sync);
          this->active = false;
          this->sync = 0;
        }
      }

      void SyncWrapper::reset() noexcept
      {
        this->kill();

        *this = SyncWrapper::generate();
      }

      bool SyncWrapper::isActive() noexcept
      {
        return this->active;
      }

      // SyncObject methods

      SyncObject::SyncObject(Subsystem& subsystem) noexcept :
        subsystem(&subsystem),
        destructionCommandSent(false)
      {
      }

      SyncObject::~SyncObject() noexcept
      {
#ifndef NDEBUG
        if (false == this->destructionCommandSent)
        {
          std::cerr << "an object inheriting from SyncObject didn't send a destruction command!" << std::endl;
        }
#endif
      }

      Subsystem& SyncObject::getSubsystem() noexcept
      {
        return *this->subsystem;
      }

      void SyncObject::sendDestructionCommand(DestructionCommandPtr&& command) noexcept
      {
        assert(this->destructionCommandSent == false);
        this->destructionCommandSent = true;
        this->subsystem->pushDestructionCommand(std::move(command));
      }

      // ReadSyncLock methods

      ReadSyncLock::ReadSyncLock(std::shared_ptr<const SyncObject> syncTarget) noexcept :
        syncTarget(std::move(syncTarget)), targetLock(this->syncTarget->mutex)
      {
        this->syncTarget->writeSync.wait();
      }

      ReadSyncLock::~ReadSyncLock() noexcept
      {
        if (nullptr != this->syncTarget)
        {
          // make sure no one else messes with the readSyncs vector while you are modifying it
          std::unique_lock<std::mutex> access(this->syncTarget->readSyncsAccess);
          // prevent the vector from growing too large
          // otherwise it can lead to syncObject gobble

          // to solve this wait on all the syncs, then generate a new sync in the same context
          // this effectively combines all the syncs into a single sync object, maintaining thread ordering
          if (this->syncTarget->readSyncs.size() > MAX_READ_SIZE)
          {
            this->syncTarget->readSyncs.clear();
          }

          this->syncTarget->readSyncs.push_back(SyncWrapper::generate());
        }
      }

      ReadSyncLock::ReadSyncLock(ReadSyncLock&& original) noexcept :
        syncTarget(std::move(original.syncTarget)), targetLock(std::move(original.targetLock))
      {
        assert(nullptr == original.syncTarget);
        assert(false == original.targetLock.owns_lock());
      }

      ReadSyncLock& ReadSyncLock::operator=(ReadSyncLock&& rhs) noexcept
      {
        this->syncTarget = std::move(rhs.syncTarget);
        this->targetLock = std::move(rhs.targetLock);

        assert(nullptr == rhs.syncTarget);
        assert(false == rhs.targetLock.owns_lock());

        return *this;
      }

      // WriteSyncLock methods

      WriteSyncLock::WriteSyncLock(std::shared_ptr<SyncObject> syncTarget) noexcept :
        syncTarget(std::move(syncTarget)), targetLock(this->syncTarget->mutex)
      {
        // wait for all the syncs
        this->syncTarget->writeSync.wait();
        this->syncTarget->writeSync.kill();
        // guaranteed to have exlusive access to read syncs vector because
        // it has an exclusive lock on the entire object (required for writing)
        this->syncTarget->readSyncs.clear();

        // this sync object can now be used exclusively (written to)
      }

      WriteSyncLock::WriteSyncLock(std::shared_ptr<SyncObject> syncTarget, WriteSyncLock::Constructor_t) noexcept :
        syncTarget(std::move(syncTarget)), targetLock(this->syncTarget->mutex)
      {
        // do not wait on the syncs, as they have not been created yet
      }

      WriteSyncLock::WriteSyncLock(ReadSyncLock&& original) noexcept :
        syncTarget(std::move(original.syncTarget)), targetLock(std::move(original.targetLock), boost::try_to_lock_t())
      {
        // first try to upgrade the lock atomically
        // if that doesn't work, release and construct a new lock from scratch
        // mention in the user manual that this could cause a drop in ownership between the calls
        if (!this->targetLock.owns_lock())
        {
          original.targetLock.unlock();
          this->targetLock = SyncObject::UniqueLock(this->syncTarget->mutex);
        }

        // we are inheriting a ReadSyncLock, so the writeSync should already have been waited on
        this->syncTarget->writeSync.kill();
        this->syncTarget->readSyncs.clear();
      }

      WriteSyncLock::~WriteSyncLock() noexcept
      {
        if (nullptr != this->syncTarget.get())
        {
          assert(this->syncTarget->readSyncs.empty());
          assert(this->syncTarget->writeSync.isActive() == false);
          this->syncTarget->writeSync.reset();
        }
      }

      WriteSyncLock::WriteSyncLock(WriteSyncLock&& rhs) noexcept :
        syncTarget(std::move(rhs.syncTarget)),
        targetLock(std::move(rhs.targetLock))
      {
        assert(nullptr == rhs.syncTarget);
        assert(!rhs.targetLock.owns_lock());
      }

      WriteSyncLock& WriteSyncLock::operator=(WriteSyncLock&& rhs) noexcept
      {
        this->syncTarget = std::move(rhs.syncTarget);
        this->targetLock = std::move(rhs.targetLock);

        assert(nullptr == rhs.syncTarget);
        assert(!rhs.targetLock.owns_lock());

        return *this;
      }
    }
  }
}
