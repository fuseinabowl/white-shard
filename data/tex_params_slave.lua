WhiteShard.messaging:setMessageHandler(function (filter)
  print("received message with parameter: ", type(filter));

  if (filter == WhiteShard.window.graphics.magnificationFilters.linear) then
    print("horay it works");
  end;

  for name, iterFilter in pairs(WhiteShard.window.graphics.magnificationFilters) do
    if (iterFilter == filter) then
      io.write("filter is of type ", name, "\n");
    end;
  end;

  WhiteShard.messaging:getThisThread():kill();
end);

WhiteShard:setPriorityFunction(function (jobs)
  return 1;
end);
