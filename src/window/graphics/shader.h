#ifndef _GRAPHICS_SHADER_H_
#define _GRAPHICS_SHADER_H_

#include "window/graphics/openGL.h"
#include "context_wrapper.h"
#include <mutex>

class lua_State;

namespace WhiteShard{
  namespace Window{
    namespace Graphics{
      struct ShaderType;
      class ShaderProgram;
      class Subsystem;
      typedef Subsystem* SubsystemPtr;

      class Shader;
      typedef std::shared_ptr<Shader> ShaderPtr;

      class Shader : public SyncObject
      {
        GLuint shaderName;

        std::string getInfoLog() const;
        bool isCompiled() const;
       public:
        Shader(ShaderType, const std::string& source, std::string& infoLog, SubsystemPtr);
        ~Shader() noexcept;

        Shader(const Shader&) = delete;
        Shader& operator=(const Shader&) = delete;

        Shader(Shader&&) = delete;
        Shader& operator=(Shader&&) = delete;

        static void attachToProgram(ContextLock&, const ShaderPtr&, GLuint programName);

        static void pushMetatable(lua_State*);
      };

      struct ShaderCompilationException : public std::exception
      {
        ~ShaderCompilationException() noexcept;
        const char* what() const noexcept;
      };
    }
  }
}

#endif
