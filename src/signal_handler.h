#ifndef _SIGNAL_HANDLER_H_
#define _SIGNAL_HANDLER_H_

#include "engine.h"

namespace WhiteShard
{
  class SignalHandler
  {
    bool interrupted;

    SignalHandler();
    ~SignalHandler() = default;
   public:
    SignalHandler(const SignalHandler&) = delete;
    SignalHandler* operator=(const SignalHandler&) = delete;

    SignalHandler(SignalHandler&&) = delete;
    SignalHandler& operator=(SignalHandler&&) = delete;

    static void handleSignal(int);
    static void initialiseGlobalSignalHandler();
    static bool isInterrupted();
  };
}

#endif
