#include "lua_shard.h"
#include <string>

namespace WhiteShard
{
  namespace Lua
  {
    std::string toString(lua_State* state, int index)
    {
      const char* charPtr(lua_tostring(state, index));

      if (nullptr != charPtr)
      {
        return std::string(charPtr);
      }
      else
      {
        return std::string();
      }
    }
  }
}
