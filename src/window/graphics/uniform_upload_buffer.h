#ifndef _UNIFORM_UPLOAD_BUFFER_H_
#define _UNIFORM_UPLOAD_BUFFER_H_

#include "window/graphics/openGL.h"
#include "texture.h"
#include "context_wrapper.h"
#include <vector>
#include <utility>

namespace WhiteShard{
  namespace Window{
    namespace Graphics{
      class UniformUploadBuffer
      {
        std::vector<const Texture*> texturesToUpload;
        std::vector<GLuint> uploadUniformTargets;
       public:
        void doBufferedUploads(ContextLock&) throw (UnsupportedNumberOfTexturesException);
        void reset() noexcept; // clears all collections
        void pushTexture(const std::pair<const Texture*, GLuint>&) noexcept;
      };
    }
  }
}

#endif
