#include "loadfile.h"
#include <algorithm>

namespace WhiteShard{
  namespace Messaging{
    LoadFileMessageBase::LoadFileMessageBase(std::string filename, unsigned int loadStep) noexcept :
      filename(std::move(filename)), loadStep(loadStep)
    {
    }

    LoadFileMessageBase::~LoadFileMessageBase() noexcept
    {
    }

    std::unique_ptr<std::fstream> LoadFileMessageBase::getFile() const throw (FileAccessException)
    {
      std::unique_ptr<std::fstream> inputFilePtr(new std::fstream(this->filename, std::ios::ate | std::ios::in | std::ios::binary));

      if (inputFilePtr->is_open() == false)
      {
        // problems opening the file
        throw FileAccessException(this->filename);
      }

      return std::move(inputFilePtr);
    }

    bool LoadFileMessageBase::readStep(std::fstream& inputFile, std::fstream::streampos endPos, char*& output) const noexcept
    {
      // deposit step number of bytes into the buffer
      inputFile.read(output, 
        this->loadStep + inputFile.tellg() >= endPos ? 
          endPos - inputFile.tellg() :
          this->loadStep);
      output += this->loadStep; // if the file finishes, output will not be used again

      return inputFile.tellg() != endPos;
    }

    void LoadFileMessageBase::finalise(std::vector<char>& buffer, Lua::State& sendTo) const throw (Lua::LuaException)
    {
      lua_State* state(sendTo);

      Lua::StackChecker checker(state, 0, "LoadFileMessageBase::finalise");

      // now we have a fully loaded file, we need to pass it into the State specified
      int errorState = luaL_loadbuffer(state, &buffer.front(), buffer.size(), "loading");

      if (errorState != LUA_OK)
      {
        // if it's not successful, a string containing the error message will
        // be pushed onto the stack

        std::string exceptionString("unknown error");
        if (lua_isstring(state, -1) == 1)
        {
          exceptionString = std::string(lua_tostring(state, -1));
        }

        // pop the error message here so that the stack checker doesn't catch it
        lua_pop(state, 1);

        switch(errorState)
        {
        case LUA_ERRSYNTAX:
          throw SyntaxException(this->filename, exceptionString);
          break;
        case LUA_ERRMEM:
          throw CompileMemoryException(this->filename, exceptionString);
          break;
        default:
          break;
        }
      }

      // call the chunk that was just loaded
      Lua::observePcall(lua_pcall(state, 0, 0, 0), state);
    }
  }
}
