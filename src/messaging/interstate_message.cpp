#include "interstate_message.h"
#include <cassert>
#include <csignal>

namespace WhiteShard
{
  namespace Messaging
  {
    Subsystem::InterstateMessage::InterstateMessage(const Subsystem& parent, BoxPtrCollection&& boxes) noexcept : parent(&parent), boxes(std::move(boxes))
    {}

    Subsystem::InterstateMessage::~InterstateMessage() noexcept
    {}

    void Subsystem::InterstateMessage::sendMessage(Lua::State& sendTo) const throw(Lua::LuaException, NoMessageHandlerException)
    {
      lua_State* state(sendTo);
      Lua::StackChecker checker(state, 0, "Messaging::InterstateMessage - sendMessage");

      // get the receiver
      lua_getfield(state, LUA_REGISTRYINDEX, Registry::WhiteShard.c_str());
      lua_rawgeti(state, -1, Registry::MessagingReceiver);
      lua_replace(state, -2);

      // check it's a function
      if (0 == lua_isfunction(state, -1))
      {
        lua_pop(state, 1);
        // if it's not a function, the user has done something wrong - you shouldn't
        // send messages to a state without first setting up the message handler
        // raise a C++ exception
        throw NoMessageHandlerException();
      }

      // apply all the boxptrs to the state
      for (const BoxPtr& currentBox : this->boxes)
      {
        Lua::StackChecker boxChecker(state, 1, "Messaging::InterstateMessage - box deposit");
        currentBox->deposit(state);
      }

      // call the function
      Lua::observePcall(lua_pcall(state, this->boxes.size(), 0, 0), state);
    }
  }
}
