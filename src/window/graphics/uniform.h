#ifndef _UNIFORM_H_
#define _UNIFORM_H_

#include "window/graphics/openGL.h"
#include <exception>
#include "lua_shard.h"
#include "uniform_upload_buffer.h"

namespace WhiteShard{
  namespace Window{
    namespace Graphics{
      struct UnknownDataTypeException : public std::exception
      {
        ~UnknownDataTypeException() noexcept;
        const char* what() noexcept;
      };

      class Uniform
      {
        GLenum dataType;
        GLuint uniformLocation;
       public:
        Uniform() noexcept;
        Uniform(GLenum, GLuint) throw (UnknownDataTypeException);
        ~Uniform() noexcept;

        void upload(lua_State*, UniformUploadBuffer&) throw (Lua::BadTypenameException); // uploads the topmost element in the state in the fashion specified by the dataType

        static void pushMetatable(lua_State*) noexcept;
      };
    }
  }
}

#endif
