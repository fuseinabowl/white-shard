#ifndef _LUA_EXCEPTION_H_ 
#define _LUA_EXCEPTION_H_

#include "lua/include.h"
#include <exception>
#include <string>

class lua_State;

namespace WhiteShard{
  namespace Lua{
    class MessageException : public std::exception
    {
     public:
      MessageException() = default;
      virtual ~MessageException() noexcept;
    };

    // this exception can wrap around pcalls for use in state messages
    class LuaException : public MessageException
    {
      std::string exceptionString;
      int exceptionType;
     public:
      LuaException(int exceptionType, lua_State*);
      virtual ~LuaException() throw();
      virtual const char* what() const throw();
      int getExceptionType() const;
    };
    
    void observePcall(int exceptionType, lua_State*) throw (LuaException); // wrap this function around pcalls
  }
}

#endif
