#include "mesh.h"
#include "lua_shard.h"
#include <algorithm>
#include "openGL.h"
#include <SFML/Graphics.hpp>
#include "restart_element.h"
#include "context_wrapper.h"
#include "subsystem.h"
#include "per_context_command.h"

namespace WhiteShard{
  namespace Lua{
    template <>
      const std::string& LuaTraits<Window::Graphics::MeshPtr>::getName() noexcept
    {
      static const std::string name("mesh");
      return name;
    }
  }

  namespace Window{
    namespace Graphics{
      // constructor helper functions
      struct IncompleteVertexFinder
      {
        unsigned char size;
        unsigned int currentListingIndex;
        unsigned int currentVertexIndex;
        IncompleteVertexFinder(unsigned int currentListing, unsigned char size) noexcept;

        void operator()(const std::vector<GLfloat>&) throw (IncompleteVertexException);
      };

      IncompleteVertexFinder::IncompleteVertexFinder(unsigned int currentListingIndex, unsigned char size) noexcept :
        size(size),
        currentListingIndex(currentListingIndex),
        currentVertexIndex(0)
      {}

      void IncompleteVertexFinder::operator()(const std::vector<GLfloat>& vertex) throw (IncompleteVertexException)
      {
        if (vertex.size() < this->size)
        {
          throw IncompleteVertexException(
            this->currentListingIndex,
            this->currentVertexIndex
              );
        }

        ++currentVertexIndex;
      }

      // the actual constructor
      Mesh::Mesh(
          const std::vector<DataListing>& dataListings,
          const std::vector<SubmeshListing>& submeshListings,
          Subsystem& subsystem,
          ContextLock&
            ) throw (
              ElementOutOfRangeException,
              InconsistentDataListingSizeException,
              InvalidDataListingTypeException,
              IncompleteVertexException
                )
              :
        SyncObject(subsystem),
        stride(0),
        bufferNames{0,0}
      {
        // construct the data array in memory first

        // if there are no data elements,
        // no useful submeshes can be constructed so throw an error here
        if (dataListings.size() == 0)
        {
          throw ElementOutOfRangeException(-1, -1);
        }

        // ensure all data listings are the same size
        auto firstDataListingSize = dataListings.front().dataListing.size();
        typename std::vector<std::vector<GLfloat> >::const_iterator testIterator;

        // also calculate the filler elements required to push a vertex
        // to a power of two size (stop cross fetch boundary vertices)
        // to improve speed
        unsigned int currentListingIndex(0);

        for (auto currentListing : dataListings)
        {
          if (currentListing.dataListing.size() != firstDataListingSize)
          {
            throw InconsistentDataListingSizeException(currentListingIndex);
          }

          switch (currentListing.dataType)
          {
            case GL_FLOAT:
              std::for_each(currentListing.dataListing.begin(), currentListing.dataListing.end(), IncompleteVertexFinder(currentListingIndex, 1));
              this->stride += 1 * sizeof(GLfloat);
              break;
            case GL_FLOAT_VEC2:
              std::for_each(currentListing.dataListing.begin(), currentListing.dataListing.end(), IncompleteVertexFinder(currentListingIndex, 2));
              this->stride += 2 * sizeof(GLfloat);
              break;
            case GL_FLOAT_VEC3:
              std::for_each(currentListing.dataListing.begin(), currentListing.dataListing.end(), IncompleteVertexFinder(currentListingIndex, 3));
              this->stride += 3 * sizeof(GLfloat);
              break;
            case GL_FLOAT_VEC4:
              std::for_each(currentListing.dataListing.begin(), currentListing.dataListing.end(), IncompleteVertexFinder(currentListingIndex, 4));
              this->stride += 4 * sizeof(GLfloat);
              break;
              // TODO add matrices here
            default:
              throw InvalidDataListingTypeException(currentListingIndex);
              break;
          }

          ++currentListingIndex;
        }

        // find the number of filler elements to add
				// this algorithm finds the next highest power of two
        --this->stride;
        this->stride |= this->stride >> 1;
        this->stride |= this->stride >> 2;
        this->stride |= this->stride >> 4;
        this->stride |= this->stride >> 8;
        this->stride |= this->stride >> 16;
        ++this->stride;

        // create a vertex buffer large enough to hold all the elements
        // stride is in the pointer domain, divide by sizeof to get to the count domain
        std::vector<GLfloat> vertexUpload((this->stride / sizeof(GLfloat)) * firstDataListingSize);

        // upload each vertex in turn
        GLfloat* writePointer(nullptr);
        for (unsigned int currentVertex = 0; currentVertex < firstDataListingSize; ++currentVertex)
        {
          writePointer = &vertexUpload[currentVertex * this->stride / sizeof(GLfloat)];

          for (const DataListing& currentListing : dataListings)
          {
            // write the current vertex and update the write pointer
            switch (currentListing.dataType)
            {
            case GL_FLOAT:
              *writePointer++ = currentListing.dataListing[currentVertex][0];
              break;
            case GL_FLOAT_VEC2:
              *writePointer++ = currentListing.dataListing[currentVertex][0];
              *writePointer++ = currentListing.dataListing[currentVertex][1];
              break;
            case GL_FLOAT_VEC3:
              *writePointer++ = currentListing.dataListing[currentVertex][0];
              *writePointer++ = currentListing.dataListing[currentVertex][1];
              *writePointer++ = currentListing.dataListing[currentVertex][2];
              break;
            case GL_FLOAT_VEC4:
              *writePointer++ = currentListing.dataListing[currentVertex][0];
              *writePointer++ = currentListing.dataListing[currentVertex][1];
              *writePointer++ = currentListing.dataListing[currentVertex][2];
              *writePointer++ = currentListing.dataListing[currentVertex][3];
              break;
            default:
              break;
            }
          }
        }

        // construct the element array buffer in memory
        std::vector<GLushort> indexUpload;

        for (auto currentListing = submeshListings.begin(); currentListing != submeshListings.end(); ++currentListing)
        {
          // test to make sure each vertex target is in the vertex bounds or is
          // the restart element
          // then push it onto the index upload list and construct a submesh from it
          for (auto currentTarget = currentListing->elementListing.begin(); currentTarget != currentListing->elementListing.end(); ++currentTarget)
          {
            if (*currentTarget < firstDataListingSize || *currentTarget == Graphics::RESTART_INDEX)
            {
              indexUpload.push_back(*currentTarget);
            }
            else
            {
              throw ElementOutOfRangeException(
                  std::distance(submeshListings.begin(), currentListing),
                  std::distance(currentListing->elementListing.begin(), currentTarget)
                    );
            }
          }

          // construct the submesh
          this->submeshes.push_back(
              Submesh{
                currentListing->mode, 
                static_cast<GLsizei>(currentListing->elementListing.size()),
                static_cast<GLushort*>(nullptr) + (indexUpload.size() - currentListing->elementListing.size())
							}
						);
        }

        // if state hasn't locked a context, we
        // need an openGL context in order to set up the buffers
        std::unique_ptr<sf::Context> constructionContext;
        // TODO check state for context
        constructionContext = std::unique_ptr<sf::Context>(new sf::Context());

        // construct the buffers and the vertex array object
        // do it here so that exceptions can occur and we don't
        // need to worry about cleaning up during the constructor
        glGenBuffers(2, this->bufferNames);

        // bind all the buffers
        glBindBuffer(GL_ARRAY_BUFFER, this->bufferNames[0]);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->bufferNames[1]);

        // fill the buffers with the data
        glBufferData(GL_ARRAY_BUFFER, vertexUpload.size() * sizeof(GLfloat), &vertexUpload.front(), GL_STATIC_DRAW);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexUpload.size() * sizeof(GLfloat), &indexUpload.front(), GL_STATIC_DRAW);

        // set up the pointers
        GLchar* currentPointer(nullptr);
        for (GLuint currentAttribute(0); currentAttribute < dataListings.size(); ++currentAttribute)
        {
          this->channels.push_back(DataChannel{currentPointer, 2/*TODO get the size*/});
          currentPointer += sizeof(GLfloat) * this->channels.back().size;
        }
      }

      void Mesh::activate(const std::shared_ptr<Mesh>& meshPtr, ContextLock& contextLock) noexcept
      {
        // lock the mesh
        assert(nullptr != meshPtr);
        contextLock.readLock(meshPtr);

        // allow use as a reference
        Mesh& mesh = *meshPtr;

        // does the context lock already have a VAO set up for this mesh?
        try
        {
          glBindVertexArray(contextLock.getVAOName(mesh));
        }
        catch (NoVAOException&)
        {
          // no VAO registered, make one and register it, then leave it active
          GLuint VAOName(0);
          glGenVertexArrays(1, &VAOName);
          glBindVertexArray(VAOName);

          // load the buffers
          glBindBuffer(GL_ARRAY_BUFFER, mesh.bufferNames[0]);
          glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh.bufferNames[1]);

          // set up the pointers
          DataChannel* currentChannel(nullptr);
          for (unsigned short currentIndex(0); currentIndex < mesh.channels.size(); ++currentIndex)
          {
            currentChannel = &mesh.channels[currentIndex];
            glVertexAttribPointer(currentIndex, currentChannel->size, GL_FLOAT, GL_FALSE, mesh.stride, currentChannel->address);
            glEnableVertexAttribArray(currentIndex);
          }

          // register the VAO now
          contextLock.registerVAOName(mesh, VAOName);
        }
      }

      void Mesh::drawSubmesh(unsigned int submeshIndex, unsigned int instances) throw (std::out_of_range)
      {
        auto& selectedSubmesh(this->submeshes.at(submeshIndex));

        glDrawElementsInstanced(selectedSubmesh.mode, selectedSubmesh.count, GL_UNSIGNED_SHORT, selectedSubmesh.indices, instances);
      }

      void Mesh::pushMetatable(lua_State* state)
      {
        Lua::generateMetatable<MeshPtr>(state, 1, 0);
        Lua::registerMetatable<MeshPtr>(state);
      }

      // DestructionCommand stuff

      // DestructionCommand - will be run once in an arbitrary context
      class MeshDestructionCommand : public DestructionCommand
      {
        GLuint names[2];
       public:
        MeshDestructionCommand(SyncObject&, GLuint VBOname, GLuint IBOname) noexcept;
        ~MeshDestructionCommand() noexcept;
       private:
        void performDestruction() noexcept;
      };

      MeshDestructionCommand::MeshDestructionCommand(SyncObject& syncObject, GLuint VBOname, GLuint IBOname) noexcept :
        DestructionCommand(syncObject),
        names{VBOname, IBOname}
      {
      }

      MeshDestructionCommand::~MeshDestructionCommand() noexcept
      {
      }

      void MeshDestructionCommand::performDestruction() noexcept
      {
        glDeleteBuffers(2, this->names);
      }

      // MeshVAODestructionCommand - will be run in every user accessible context
      class MeshVAODestructionCommand : public PerContextCommand
      {
        // this field can only be used as a key for maps, the pointer will go out of
        // scope almost as soon as this object is created
        const Mesh* meshKey;
       public:
        MeshVAODestructionCommand(const Mesh&) noexcept;
        ~MeshVAODestructionCommand() noexcept;

        void operator()(ContextLock&) const noexcept;
      };

      MeshVAODestructionCommand::MeshVAODestructionCommand(const Mesh& meshKey) noexcept :
        meshKey(&meshKey)
      {
      }

      MeshVAODestructionCommand::~MeshVAODestructionCommand() noexcept
      {
      }

      void MeshVAODestructionCommand::operator()(ContextLock& contextLock) const noexcept
      {
        // find if the mesh VAO exists in this context
        // if it does, delete the VAO
        try
        {
          GLuint VAOName = contextLock.getVAOName(*this->meshKey);
          glDeleteVertexArrays(1, &VAOName);
        }
        catch (NoVAOException&)
        {
        }
      }

      Mesh::~Mesh() noexcept
      {
        this->getSubsystem().pushPerContextCommand(PerContextCommandPtr(new MeshVAODestructionCommand(*this)));
        this->sendDestructionCommand(DestructionCommandPtr(new MeshDestructionCommand(*this, this->bufferNames[0], this->bufferNames[1])));
      }
    }
  }
}
