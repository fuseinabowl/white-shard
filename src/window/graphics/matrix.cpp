#include "matrix.h"

namespace WhiteShard{
  namespace Window{
    namespace Graphics{
      const std::string matrixName("matrix");

      const std::string MatrixAddFunction::name("add");
      const std::string MatrixSubtractFunction::name("subtract");
      const std::string MatrixMultiplyFunction::name("multiply");
      const std::string MatrixInvertFunction::name("invert");
      const std::string MatrixTransposeFunction::name("transpose");
      const std::string MatrixToTableFunction::name("toTable");
    }
  }
}
