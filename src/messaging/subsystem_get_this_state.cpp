#include "subsystem.h"
#include <boost/mpl/vector.hpp>

namespace WhiteShard{
  namespace Messaging{
    void Subsystem::pushGetThisState(lua_State* state, Lua::StatePtr pointer)
    {
      Lua::StackChecker checker(state, 1, "Messaging::pushGetThisState");

      Lua::getMetatable<Lua::StatePtr>(state);
      new (Lua::allocate<Lua::StatePtr>(state)) Lua::StatePtr(std::move(pointer));

      lua_pushcclosure(state, Subsystem::luaGetThisState, 1);
    }

    struct GetThisStateFunction
    {
      static const std::string name;
    };
    const std::string GetThisStateFunction::name("getThisState");

    int Subsystem::luaGetThisState(lua_State* state)
    {
      // gets the state pointer from the metatable,
      // puts it in stack space 1 then shrinks the
      // stack to one
      Lua::SignatureChecker<
        boost::mpl::vector<Lua::StatePtr>::type,
        SubsystemLuaPtr, GetThisStateFunction,
        boost::mpl::vector<>::type
          > sigChecker(state);

      lua_settop(state, 1);
      lua_copy(state, lua_upvalueindex(1), 1);
      
      return 1;
    }
  }
}
