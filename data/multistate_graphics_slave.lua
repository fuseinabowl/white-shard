-- this thread will draw a picture to a texture
-- while the other thread asynchronously uses the texture to draw to the screen

-- the first message will have the shaderProgram and the texture to use
-- the rest will be signals to work
WhiteShard:setPriorityFunction(function (jobs)
  return jobs;
end);

WhiteShard.messaging:setMessageHandler(function (shaderProgram, texture)
  local unitMesh = WhiteShard.window.graphics.unitMesh;
  local emptyTable = {};
  local threeTable = {3};
  local uploadTable = {};
  local allUploadsTable = {uploadTable};
  local time = 0.0;

  WhiteShard.window.graphics:lockContext(nil);
  local plusColour = shaderProgram:getUniform("plusColour");

  WhiteShard.messaging:setMessageHandler(function ()
    time = time + 0.01;
    time = time - math.floor(time);
    uploadTable[plusColour] = WhiteShard.window.graphics:createMatrix({{time,time,time}});
    WhiteShard.window.graphics:lockContext(nil);
    shaderProgram:draw(emptyTable, texture, unitMesh, threeTable, allUploadsTable);
    WhiteShard.window.graphics:render();
  end);
end);
