#include "subsystem.h"
#include "lua_shard.h"
#include "registry.h"
#include "subsystem_functions.h"
#include "../window_wrapper.h"
#include <boost/mpl/vector.hpp>
#include "context_wrapper.h"

namespace WhiteShard{
  namespace Window{
    namespace Graphics{
      template <>
        const std::string ContextLockFunction::name("lockContext");

      void Subsystem::pushContextLock(lua_State* state)
      {
        Lua::StackChecker checker(state, 1, "Subsystem::pushContextLock");
        
        lua_getfield(state, LUA_REGISTRYINDEX, Registry::WhiteShard.c_str());
        lua_rawgeti(state, -1, Registry::TemporaryCollection);
        lua_rawgeti(state, -2, Registry::ContextLock);
        lua_replace(state, -3);

        lua_pushcclosure(state, Subsystem::luaContextLock, 2);
      }

      int Subsystem::luaContextLock(lua_State* state)
      {
        Lua::SignatureChecker<
          boost::mpl::vector<>::type,
          Graphics::SubsystemPtr, ContextLockFunction,
          boost::mpl::vector<Lua::Optional<WindowWrapperPtr> >::type
            > sigChecker(state);

        Subsystem& subsystem(*Lua::get<Graphics::SubsystemPtr>(state, 1));
        ContextLockPtr& contextLock(Lua::get<ContextLockPtr>(state, lua_upvalueindex(1)));
        Lua::TemporaryObjectPtr tempObjectPtr(nullptr);

        if (0 == lua_isnoneornil(state, 2))
        {
          // free the current context lock
          // if we don't, we might be waiting on one that would never be returned
          contextLock.reset();

          // we are getting a specific window
          std::unique_lock<std::mutex> availableContextLock(subsystem.availableContextAccess);

          WindowWrapperPtr windowToLock(Lua::get<WindowWrapperPtr>(state, 2));
          auto findIterator = subsystem.availableWindowContexts.find(windowToLock.get());
          while (subsystem.availableWindowContexts.end() == findIterator)
          {
            subsystem.windowContextAdded.wait(availableContextLock);
            findIterator = subsystem.availableWindowContexts.find(windowToLock.get());
          }

          // findIterator contains the WindowContextWrapperPtr we want
          contextLock.reset(new WindowContextLock(std::move(findIterator->second.second), subsystem, findIterator->second.first.lock()));
          subsystem.availableWindowContexts.erase(findIterator);
        }
        else if (nullptr == contextLock.get())
        {
          // if we don't currently own a context lock, then we need to get one
          // if we do, we can just keep on using that one

          // we need any context
          std::unique_lock<std::mutex> availableContextLock(subsystem.availableContextAccess);
          while(subsystem.availablePureContexts.empty() && subsystem.availableWindowContexts.empty())
          {
            subsystem.contextAdded.wait(availableContextLock);
          }

          if (!subsystem.availablePureContexts.empty())
          {
            // try contexts first because the windows may be explicitly requested
            contextLock.reset(new PureContextLock(std::move(subsystem.availablePureContexts.back()), subsystem));
            subsystem.availablePureContexts.pop_back();
          }
          else
          {
            // because of the while loop and contexts is empty, we know that windows must be non-empty
            auto popIterator = subsystem.availableWindowContexts.begin();
            contextLock = ContextLockPtr(new WindowContextLock(std::move(popIterator->second.second), subsystem, popIterator->second.first.lock()));
            subsystem.availableWindowContexts.erase(popIterator);
          }
        }

        // we have a context lock now, it should be cycled for per-context actions
        contextLock->doPerContextCommands();

        Lua::get<Lua::TemporaryCollection>(state, lua_upvalueindex(2))->insert(std::make_pair(contextLock.get(), Lua::TemporaryObjectPtr(new ContextFreer(contextLock))));

        return sigChecker.resultCount;
      }

      void Subsystem::addWindowContext(const WindowWrapperPtr& windowWrapper) noexcept
      {
        std::unique_lock<std::mutex> availableContextsLock(this->availableContextAccess);

        // construct the context wrapper
        std::unique_ptr<WindowContextWrapper> windowContext(
            new WindowContextWrapper(*this, *windowWrapper)
              );

        // register the context
        this->activeWindowContexts.insert(std::make_pair(windowWrapper.get(), windowContext.get()));
        this->availableWindowContexts.insert
          (std::make_pair(
            windowWrapper.get(),
            std::make_pair(
              std::weak_ptr<WindowWrapper>(windowWrapper), 
              std::move(windowContext)
            )
          ));

        this->contextAdded.notify_all();
        // we will never have replaced a window that someone is explicitly looking for
        // so don't notify threads waiting on windowContextAdded
      }

      void Subsystem::removeWindowContext(const WindowWrapperPtr& windowWrapper) noexcept
      {
        std::unique_lock<std::mutex> availableContextsLock(this->availableContextAccess);

        this->activeWindowContexts.erase(windowWrapper.get());

        // now look for the window in the available list
        auto findIterator = this->availableWindowContexts.find(windowWrapper.get());
        if (findIterator != this->availableWindowContexts.end())
        {
          this->availableWindowContexts.erase(findIterator);
        }
      }

      void Subsystem::returnWindowContext(const WindowWrapperPtr& windowWrapper, WindowContextWrapperPtr&& windowContext) noexcept
      {
        std::unique_lock<std::mutex> contextLock(this->availableContextAccess);

        // if the context is still in the active contexts collection, put it back in
        // the available contexts
        auto findIterator = this->activeWindowContexts.find(windowWrapper.get());
        if (findIterator != this->activeWindowContexts.end())
        {
          this->availableWindowContexts.emplace(windowWrapper.get(), std::make_pair(std::weak_ptr<WindowWrapper>(windowWrapper), std::move(windowContext)));

          this->contextAdded.notify_all();
          this->windowContextAdded.notify_all();
        }
      }

      void Subsystem::returnPureContext(PureContextWrapperPtr&& context) noexcept
      {
        std::unique_lock<std::mutex> contextLock(this->availableContextAccess);

        auto findIterator = std::find(this->activePureContexts.begin(), this->activePureContexts.end(), context.get());
        if (findIterator != this->activePureContexts.end())
        {
          this->availablePureContexts.push_back(std::move(context));

          this->contextAdded.notify_all();
        }
      }
    }
  }
}
