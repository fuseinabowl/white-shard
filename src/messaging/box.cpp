#include "box.h"
#include "messaging.h"

namespace WhiteShard{
  namespace Messaging{
    Box::~Box()
    {}

    void Box::extract(lua_State* state, int index)
    {
      lua_pushnil(state);
      if (index > 0)
      {
        lua_copy(state, index, -1);
      }
      else
      {
        lua_copy(state, index - 1, -1);
      }

      this->extract(state);

      lua_pop(state, 1);
    }
  }
}
