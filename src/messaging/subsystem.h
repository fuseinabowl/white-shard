#ifndef _MESSAGING_SUBSYSTEM_H_
#define _MESSAGING_SUBSYSTEM_H_

#include "engine/engine.h"

namespace WhiteShard{
  namespace Messaging{
    class Subsystem;
    typedef Subsystem* SubsystemLuaPtr;

    class Subsystem : public WhiteShard::Subsystem
    {
     public:
      // lua callable
      static const std::string 
        createState,
        sendMessage,
        killState,
        getThisState,
        registerReceiver,

      // internal
        attachName;
     private:
      class InterstateMessage; // defined in "messaging_interstate_message.h"

      Engine* engine;
     public:
      Subsystem(Engine&);

      std::string pushSubsystem(Lua::State&);

     private:
      static void pushCreateState(lua_State*);
      static void pushSendMessage(lua_State*, int subsystemIndex);
      static void pushKillState(lua_State*);
      static void pushGetThisState(lua_State*, Lua::StatePtr);
      static void pushRegisterReceiver(lua_State*);

      static int luaCreateState(lua_State*);
      static int luaSendMessage(lua_State*);
      static int luaKillState(lua_State*);
      static int luaGetThisState(lua_State*);
      static int luaRegisterReceiver(lua_State*);
    };
  }
}

#endif
