#include "context_wrapper.h"
#include "subsystem.h"

namespace WhiteShard{
  namespace Window{
    namespace Graphics{
      // PureContextWrapper methods
      PureContextWrapper::PureContextWrapper(const Subsystem& subs) noexcept :
        ContextWrapperBase(subs)
      {
        this->initialise(0,0);
        this->context.setActive(false);
      }

      PureContextWrapper::~PureContextWrapper() noexcept
      {
      }

      // PureContextLock methods
      PureContextLock::PureContextLock(PureContextWrapperPtr&& contextWrapper, Subsystem& subsystem) noexcept :
        contextPtr(std::move(contextWrapper)), subsystem(&subsystem)
      {
        // try to lock the context until it is successfully locked
        while(!this->contextPtr->context.setActive(true));
      }

      PureContextLock::~PureContextLock() noexcept
      {
        this->contextPtr->context.setActive(false);

        // return the context to the subsystem
        this->subsystem->returnPureContext(std::move(this->contextPtr));
      }

      ContextWrapperBase& PureContextLock::getContextWrapper() const noexcept
      {
        return *this->contextPtr;
      }

      void PureContextLock::relock() noexcept
      {
#ifdef NDEBUG
        this->contextPtr->context.setActive(true);
#else
        assert(this->contextPtr->context.setActive(true));
#endif
      }

      void PureContextLock::render() const noexcept
      {
        glFlush();
      }
    }
  }
}
