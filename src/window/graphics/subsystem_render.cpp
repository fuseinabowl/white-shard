#include "subsystem.h"
#include "subsystem_functions.h"
#include "registry.h"
#include "lua/checker.h"
#include <boost/mpl/vector.hpp>

namespace WhiteShard{
  namespace Window{
    namespace Graphics{
      template <>
        const std::string RenderFunction::name("render");

      void Subsystem::pushRender(lua_State* state)
      {
        // we need the context lock to be available for the function
        lua_getfield(state, LUA_REGISTRYINDEX, Registry::WhiteShard.c_str());
        lua_rawgeti(state, -1, Registry::ContextLock);

        lua_remove(state, -2);

        lua_pushcclosure(state, Subsystem::luaRender, 1);
      }

      int Subsystem::luaRender(lua_State* state)
      {
        Lua::SignatureChecker<
          boost::mpl::vector<>::type,
          SubsystemPtr, RenderFunction,
          boost::mpl::vector<>::type
            > checker(state);

        ContextLockPtr& contextLock(Lua::get<ContextLockPtr>(state, lua_upvalueindex(1)));
        ContextLock::ensurePointerValid(RenderFunction::name, contextLock, state);

        // call render on the ContextLock upvalue we stored
        contextLock->render();

        return checker.resultCount;
      }
    }
  }
}
