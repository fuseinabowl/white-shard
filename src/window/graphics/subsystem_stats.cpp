#include "subsystem.h"

namespace WhiteShard{
  namespace Window{
    namespace Graphics{
      GLuint Subsystem::getTextureUnits() const noexcept
      {
        return this->textureUnits;
      }
    }
  }
}
