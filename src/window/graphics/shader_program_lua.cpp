#include "shader_program.h"
#include "lua_shard.h"
#include <boost/mpl/vector.hpp>
#include "window/graphics/openGL.h"
#include "mesh.h"
#include <sstream>

namespace WhiteShard{
  namespace Lua{
    template <>
      const std::string& LuaTraits<Window::Graphics::ShaderProgramPtr>::getName() noexcept
    {
      static const std::string name("shader_program");
      return name;
    }
  }

  namespace Window{
    namespace Graphics{
      const std::string GetUniformFunction::name("getUniform");
      const std::string DrawFunction::name("draw");

      void ShaderProgram::pushMetatable(lua_State* state)
      {
        Lua::StackChecker checker(state, 1, "ShaderProgram::pushMetatable");

        Lua::generateMetatable<ShaderProgramPtr>(state, 1, 1);
        Lua::registerMetatable<ShaderProgramPtr>(state);

        lua_createtable(state, 0, 1); // index table

        ShaderProgram::pushGetUniform(state);
        lua_setfield(state, -2, GetUniformFunction::name.c_str());

        ShaderProgram::pushDraw(state);
        lua_setfield(state, -2, DrawFunction::name.c_str());

        lua_setfield(state, -2, "__index");
      }

      void ShaderProgram::pushGetUniform(lua_State* state)
      {
        Lua::StackChecker checker(state, 1, "ShaderProgram::pushGetUniform");

        // we need the uniform metatable
        Uniform::pushMetatable(state);

        lua_getfield(state, LUA_REGISTRYINDEX, Registry::WhiteShard.c_str());
        lua_rawgeti(state, -1, Registry::ContextLock);
        lua_remove(state, -2);

        // push the closure
        lua_pushcclosure(state, ShaderProgram::luaGetUniform, 2);
      }

      int ShaderProgram::luaGetUniform(lua_State* state)
      {
        Lua::SignatureChecker<
          boost::mpl::vector<Lua::Optional<Uniform> >::type,
          ShaderProgramPtr, GetUniformFunction,
          boost::mpl::vector<Lua::NativeTypeWrapper<LUA_TSTRING> >::type
            > sigChecker(state);

        ShaderProgram& program = *Lua::get<ShaderProgramPtr>(state, 1);
        ContextLock::ensurePointerValid(GetUniformFunction::name, Lua::get<ContextLockPtr>(state, lua_upvalueindex(2)), state);

        // push the uniform object onto the stack and return it
        // first, find the uniform
        auto uniformFindIterator = program.uniformMap.find(std::string(lua_tostring(state, 2)));

        if (uniformFindIterator != program.uniformMap.end())
        {
          // if the uniform exists, push it
          lua_pushnil(state);
          lua_copy(state, lua_upvalueindex(1), -1);
          new (Lua::allocate<Uniform>(state)) Uniform(uniformFindIterator->second);
        }
        else
        {
          // otherwise, push nil
          lua_pushnil(state);
        }

        return sigChecker.resultCount;
      }

      void ShaderProgram::pushDraw(lua_State* state)
      {
        Lua::StackChecker checker(state, 1, "ShaderProgram::pushDraw");

        // get the graphics state
        lua_getfield(state, LUA_REGISTRYINDEX, Registry::WhiteShard.c_str());
        lua_rawgeti(state, -1, Registry::ContextLock);

        // pop the registry table
        lua_remove(state, -2);

        // push the closure
        lua_pushcclosure(state, ShaderProgram::luaDraw, 1);
      }

      int ShaderProgram::luaDraw(lua_State* state)
      {
        Lua::SignatureChecker<
          boost::mpl::vector<>::type,
          ShaderProgramPtr, DrawFunction,
          boost::mpl::vector<
            Lua::NativeTypeWrapper<LUA_TTABLE>,
            Lua::Optional<TexturePtr>, // will be an optional texture later (nil indicates write to screen)
            MeshPtr,
            std::vector<Lua::NativeTypeWrapper<LUA_TNUMBER> >, // submesh indices
            std::vector<Lua::NativeTypeWrapper<LUA_TTABLE> >, // vectors of uniform data for corresponding submesh index
            Lua::Optional<Lua::NativeTypeWrapper<LUA_TNUMBER> > // instance count (defaults to 1)
              >::type
            > sigChecker(state);


        ShaderProgramPtr& programPtr(Lua::get<ShaderProgramPtr>(state, 1));
        ShaderProgram& program(*programPtr);

        assert(programPtr.get() != nullptr);

        ContextLock& contextLock = *Lua::get<ContextLockPtr>(state, lua_upvalueindex(1));
        ContextLock::ensurePointerValid(DrawFunction::name, Lua::get<ContextLockPtr>(state, lua_upvalueindex(1)), state);

        contextLock.writeLock(programPtr);
        contextLock.activateProgram(program.programName);

        // upload the initial uniforms
        UniformUploadBuffer uploadBuffer;

        lua_pushnil(state);
        try
        {
          while (lua_next(state, 2))
          {
            if (Lua::checkBool<Uniform>(state, -2))
            {
              Lua::get<Uniform>(state, -2).upload(state, uploadBuffer);
            }
            lua_pop(state, 1);
          }
        }
        catch (Lua::BadTypenameException& ex)
        {
          lua_pushstring(state, ex.what());
          lua_error(state);
        }

        try
        {
          uploadBuffer.doBufferedUploads(contextLock);
        }
        catch (UnsupportedNumberOfTexturesException& ex)
        {
          lua_pushstring(state, ex.what());
          lua_error(state);
        }

        uploadBuffer.reset();

        // select the draw buffer
        // TODO put this in context lock so it can manage whether to change targets or not
        if (!lua_isnil(state, 3))
        {
          TexturePtr& textureToWriteTo = Lua::get<TexturePtr>(state, 3);
          contextLock.writeLock(textureToWriteTo);

          // bind a framebuffer
          glBindFramebuffer(GL_FRAMEBUFFER, contextLock.getFBOName());

          // set the drawbuffer to the correct texture
          glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, textureToWriteTo->getTexName(), 0);
          GLuint drawBuffer = GL_COLOR_ATTACHMENT0;
          glDrawBuffers(1, &drawBuffer);

          contextLock.setViewport(*textureToWriteTo);
        }
        else
        {
          glBindFramebuffer(GL_FRAMEBUFFER, 0);
          glDrawBuffer(GL_BACK);

          // need to set the viewport to the size of the window
          contextLock.setViewport();
        }

        MeshPtr& mesh(Lua::get<MeshPtr>(state, 4));
        Mesh::activate(mesh, contextLock);

        // set up for drawing
        int instances(lua_tointeger(state, 7));
        if (instances < 1)
        {
          instances = 1;
        }

        int currentIndex(1);
        lua_rawgeti(state, 5, currentIndex);
        lua_rawgeti(state, 6, currentIndex);

        while (lua_isnumber(state, -2) && lua_istable(state, -1))
        {
          // upload the uniforms
          lua_pushnil(state);
          try 
          {
            while (lua_next(state, -2))
            {
              if (Lua::checkBool<Uniform>(state, -2))
              {
                Lua::get<Uniform>(state, -2).upload(state, uploadBuffer);
              }
              lua_pop(state, 1);
            }
          }
          catch (Lua::BadTypenameException& ex)
          {
            lua_pushstring(state, ex.what());
            lua_error(state);
          }

          try
          {
            uploadBuffer.doBufferedUploads(contextLock);
          }
          catch (UnsupportedNumberOfTexturesException& ex)
          {
            lua_pushstring(state, ex.what());
            lua_error(state);
          }

          uploadBuffer.reset();

          // draw the submesh
          try
          {
            mesh->drawSubmesh(lua_tointeger(state, -2) - 1, instances);
          }
          catch (std::out_of_range& ex)
          {
            std::stringstream errStream;
            errStream
              << "submesh draw call " << currentIndex << " was not in the number of accessible submeshes (must be between 1 and submesh count inclusive)";
            lua_pushstring(state, errStream.str().c_str());
            lua_error(state);
          }

          // get the next pair
          lua_pop(state, 2);
          ++currentIndex;
          lua_rawgeti(state, 5, currentIndex);
          lua_rawgeti(state, 6, currentIndex);
        }

        return sigChecker.resultCount;
      }
    }
  }
}
