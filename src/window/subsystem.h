#ifndef _WINDOW_SUBSYSTEM_H_
#define _WINDOW_SUBSYSTEM_H_

#include "engine.h"
#include <SFML/Window.hpp>
#include <vector>
#include <string>
#include <memory>
#include "graphics/subsystem.h"

namespace WhiteShard{
  namespace Window{
    class Subsystem;
    typedef Subsystem* SubsystemLuaPtr;

    class WindowWrapper;
    typedef std::shared_ptr<WindowWrapper> WindowWrapperPtr;

    class Subsystem : public WhiteShard::Subsystem
    {
      Graphics::Subsystem graphicsSubsystem;
     public:
      explicit Subsystem(int numberOfContexts) throw (Graphics::SubsystemConstructorException);
      ~Subsystem() noexcept;
      std::string pushSubsystem(Lua::State&) noexcept;
      
      static void pushCreateWindow(lua_State*);
      static int luaCreateWindow(lua_State*);
      static void pushCreateFullscreenWindow(lua_State*);
      static int luaCreateFullscreenWindow(lua_State*);

      // data
      static void pushFullscreenModes(lua_State*);
    };

    struct CreateWindowFunction
    {
      static const std::string name;
    };

    struct CreateFullscreenWindowFunction
    {
      static const std::string name;
    };
  }
}

#endif
