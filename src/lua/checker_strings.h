#ifndef _LUA_LOCALE_STRINGS_H_
#define _LUA_LOCALE_STRINGS_H_

#include <sstream>
#include <cassert>

namespace WhiteShard{
  namespace Lua{
    template <class T>
      T& checkSelf(lua_State* state, const FunctionSigniature& sig)
    {
      try
      {
        return check<T>(state, 1);
      }
      catch (BadTypenameException)
      {
        std::stringstream errorStream;

        errorStream
          << "'self' was of wrong type when calling the function \""
          << sig.getSigniature()
          << "\" - did you use '.' instead of ':'?";

#ifndef NDEBUG
        // we are telling the user the right thing, right?
        std::string sigString = sig.getSigniature();

        std::string nameAndColon(LuaTraits<T>::name);
        nameAndColon.append(":");

        assert(-1 != sigString.find(nameAndColon));
#endif

        lua_pushstring(state, errorStream.str().c_str());
        lua_error(state);
      }
    }

    template <class T>
      T& checkParameter(lua_State* state, int index, const FunctionSigniature& sig)
    {
      try
      {
        return check<T>(state, index);
      }
      catch (BadTypenameException& exception)
      {
        std::stringstream errorStream;

        errorStream
          << "Invalid type for parameter "
          << index
          << " of function \""
          << sig.getSigniature()
          << "\" - expected "
          << exception.getExpectedTypename()
          << " and got "
          << exception.getBadTypename()
          << "remember to replace optional parameters with nil if not using them";

        lua_pushstring(state, errorStream.str().c_str());
        lua_error(state);
      }
    }
  }
}

#endif
