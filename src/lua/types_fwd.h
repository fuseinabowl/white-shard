#ifdef _LUA_H_
// should only be included by lua.h

#ifndef _LUA_FWD_H_
#define _LUA_FWD_H_

#include <csignal>
#include <sstream>
#include <unordered_map>
#include <vector>
#include "checker.h"
#include <iostream>
#include "registry.h"
#include "messaging/box.h"

namespace WhiteShard
{
  namespace Lua
  {
    // specialising native types
    template <int luaTypeEnum>
      class NativeTypeWrapper;

    inline const std::string stringifyLuaType(int luaTypeEnum)
    {
      lua_State* tempState(luaL_newstate());
      std::string returnString(lua_typename(tempState, luaTypeEnum));
      lua_close(tempState);

      return returnString;
    }

#define specialise( enumType )\
    template <> inline const std::string& LuaTraits<NativeTypeWrapper< enumType > >::getName() noexcept\
    {\
      static const std::string name(stringifyLuaType(enumType));\
      return name;\
    }
    specialise(LUA_TNIL);
    specialise(LUA_TNUMBER);
    specialise(LUA_TBOOLEAN);
    specialise(LUA_TSTRING);
    specialise(LUA_TTABLE);
    specialise(LUA_TFUNCTION);
    specialise(LUA_TTHREAD);
    specialise(LUA_TLIGHTUSERDATA);
#undef specialise

    // program user functions exposed
    inline std::string getTypename(lua_State* state, int index) noexcept
    {
      StackChecker checker(state, 0, "getTypename");

      if (lua_isuserdata(state, index) == 1)
      {
        // is userdata, check metatable.__tname
        
        if (lua_getmetatable(state, index))
        {
          lua_getfield(state, -1, "__tname");
          const char* tname = lua_tostring(state, -1);
          lua_pop(state, 2);

          if (nullptr != tname)
          {
            return std::string(tname);
          }
          else
          {
            return std::string("userdata with no \"__tname\" field");
          }
        }
        else
        {
          return std::string("userdata with no metatable");
        }
      }

      // fallback to use the lua typename
      return std::string(luaL_typename(state, index));
    }

    template <class T>
      T* allocate(lua_State* state)
    {
      T* returnValue = reinterpret_cast<T*>(lua_newuserdata(state, sizeof(T)));
      lua_insert(state, -2);
      lua_setmetatable(state, -2);

#ifndef NDEBUG
      try
      {
        check<T>(state, -1);
      }
      catch (BadTypenameException& exception)
      {
        std::cerr << "allocate failed - metatable on the top of the stack was invalid for this type\nerror:\n" << exception.what() << std::endl;
        raise(SIGABRT);
      }
#endif
      return returnValue;
    }

    template <class T>
      void registerMetatable(lua_State* state)
    {
      StackChecker checker(state, 0, "registerMetatable");

      // add a box getter function for messaging use
      lua_pushcfunction(state, Messaging::Box::allocateTemplateBox<T>);
      lua_rawseti(state, -2, Registry::Box);

      // put the metatable in the registry's metatable table
      lua_getfield(state, LUA_REGISTRYINDEX, Registry::WhiteShard.c_str());
      lua_rawgeti(state, -1, Registry::Metatables);
      lua_pushvalue(state, -3);
      lua_setfield(state, -2, LuaTraits<T>::getName().c_str());

      // clean up
      lua_pop(state, 2);
    }

    template <class T>
      void getMetatable(lua_State* state)
    {
      StackChecker checker(state, 1, "getMetatable");

      lua_getfield(state, LUA_REGISTRYINDEX, Registry::WhiteShard.c_str());
      lua_rawgeti(state, -1, Registry::Metatables);
      lua_getfield(state, -1, LuaTraits<T>::getName().c_str());

      lua_replace(state, -3);
      lua_pop(state, 1);
#ifndef NDEBUG
      // check to make sure there's actually a table here
      if (0 == lua_istable(state, -1))
      {
        std::cerr << "no metatable registered for call to " << LuaTraits<T>::getName() << std::endl;
      }
      else
      {
        lua_getfield(state, -1, "__tname");
        if (0 == lua_isstring(state, -1))
        {
          std::cerr << "no string in \"__tname\" field of metatable for call to " << LuaTraits<T>::getName() << std::endl;
        }
        else if(std::string(lua_tostring(state, -1)) != LuaTraits<T>::getName())
        {
          std::cerr << "name mismatch (\"" << lua_tostring(state, -1) << " for metatable for call to " << LuaTraits<T>::getName() << std::endl;
        }

        lua_pop(state, 1);
      }
#endif
    }

    // use this in check to get pertial specialisation
    // use TypeChecker<T>::check(orignial params) to return if it succeeds or
    //  throw if it fails
    // use TypeChecker<T>::get<original params) to get a T&
    template <class T>
      struct TypeChecker;

    template <class T>
      struct TypeChecker
    {
      TypeChecker() = delete;

      static void check(lua_State* state, int index) throw (BadTypenameException);
      static T& get(lua_State* state, int index) noexcept;
    };
    
    template <class T>
      struct TypeChecker<Optional<T> >
    {
      TypeChecker() = delete;

      static void check(lua_State*, int) throw (BadTypenameException);
      static T& get(lua_State*, int) noexcept;
    };

    template <class T, class Allocator>
      struct TypeChecker<std::vector<T, Allocator> >
    {
      TypeChecker() = delete;

      static void check(lua_State*, int) throw (BadTypenameException);
      static std::vector<T, Allocator> get(lua_State*, int) noexcept;
    };

    template <class Key, class Value, class Hash, class EqualTo, class Allocator>
      struct TypeChecker<std::unordered_map<Key, Value, Hash, EqualTo, Allocator> >
    {
      TypeChecker() = delete;
      static void check(lua_State*, int) throw (BadTypenameException);
      static std::unordered_map<Key, Value, Hash, EqualTo, Allocator> get(lua_State*, int) noexcept;
    };

    template <class T>
      void TypeChecker<T>::check(lua_State* state, int index) throw (BadTypenameException)
    {
      // tests the typename against the cast
      if (1 == lua_isuserdata(state, index))
      {
        if (lua_getmetatable(state, index))
        {
          lua_getfield(state, -1, "__tname");

          if (1 == lua_isstring(state, -1))
          {
            std::string valueTypename(lua_tostring(state, -1));

            if (valueTypename == LuaTraits<T>::getName())
            {
              lua_pop(state, 2);

              return;
            }
          }
        }
      }

      // exiting the nested ifs at any point implies that an error has occurred
      throw BadTypenameException(
        LuaTraits<T>::getName(),
        getTypename(state, index),
        index
          );
    }

    template <class T>
      void TypeChecker<Optional<T> >::check(lua_State* state, int index) throw (BadTypenameException)
    {
      if (lua_isnoneornil(state, index) != 1)
      {
        // if a proper element exists at index
        TypeChecker<T>::check(state, index);
      }
    }

    template <class T, class Allocator>
      void TypeChecker<std::vector<T, Allocator> >::check(lua_State* state, int index) throw (BadTypenameException)
    {
      StackChecker checker(state, 0, "TypeChecker<vector>::check");

      // is there a length function?
      if (1 == lua_istable(state, index)) 
      {
        // if it's a table then it has one
      }
      else
      {
        // otherwise we need to check for a __len function in the metatable
        
        if (lua_getmetatable(state, index))
        {
          lua_getfield(state, -1, "__len");
          if (1 == lua_isfunction(state, -1))
          {
            // success
            lua_pop(state, 2);
          }
          else
          {
            // it isn't a function, cannot call it
            lua_pop(state, 2);
            throw BadTypenameException(LuaTraits<std::vector<T, Allocator> >::getName(),
                                       getTypename(state, index),
                                       index);
          }
        }
        else
        {
          throw BadTypenameException(LuaTraits<std::vector<T, Allocator> >::getName(),
                                     getTypename(state, index),
                                     index);
        }
      }

      // we can now assume there's a length function
      lua_len(state, index);
      int length = lua_tointeger(state, -1);
      lua_pop(state, 1);

      for (int currentIterator = 1; currentIterator <= length; ++currentIterator)
      {
        lua_pushinteger(state, currentIterator);
        lua_gettable(state, index);

        try
        {
          TypeChecker<T>::check(state, lua_gettop(state));
        }
        catch (BadTypenameException& ex)
        {
          lua_pop(state, 1);
          throw ex;
        }

        lua_pop(state, 1);
      }
    }

    template <class Key, class Value, class Hash, class EqualTo, class Allocator>
      void TypeChecker<std::unordered_map<Key, Value, Hash, EqualTo, Allocator> >::check(lua_State* state, int index) throw (BadTypenameException)
    {
      StackChecker checker(state, 0, "TypeChecker<unordered_map>::check");

      // is the element a map? (aka a table in lua)
      if (!lua_istable(state, index))
      {
        throw BadTypenameException(
            LuaTraits<std::unordered_map<Key, Value, Hash, EqualTo, Allocator> >::getName(),
            getTypename(state, index),
            index);
      }

      // iterate through the map
      // ensure all pairs are key->value
      int loopCount = 0;
      lua_pushnil(state);
      while (1 == lua_next(state, index))
      {
        // make sure we don't modify the key as we will need it for the next stepping function
        lua_pushnil(state);
        lua_copy(state, -3, -1);

        try
        {
          TypeChecker<Key>::check(state, lua_absindex(state, lua_gettop(state)));
          TypeChecker<Value>::check(state, lua_absindex(state, -2));
        }
        catch (BadTypenameException& ex)
        {
          lua_pop(state, 3);
          throw BadTypenameException(
              LuaTraits<std::unordered_map<Key, Value, Hash, EqualTo, Allocator> >::getName(),
              getTypename(state, index),
              index);
        }

        lua_pop(state, 2);
      }
    }

    template <class T>
      inline void check(lua_State* state, int index) throw (BadTypenameException)
    {
      TypeChecker<T>::check(state, index);
    }

    // get<T> implementation
    template <class T>
      inline T& TypeChecker<T>::get(lua_State* state, int index) noexcept
    {
      return *reinterpret_cast<T*>(lua_touserdata(state, index));
    }

    template <class T>
      inline T& TypeChecker<Optional<T> >::get(lua_State* state, int index) noexcept
    {
      return TypeChecker<T>::get(state, index);
    }

    template <class T, class Allocator>
      inline std::vector<T, Allocator> TypeChecker<std::vector<T, Allocator> >::get(lua_State* state, int index) noexcept
    {
      StackChecker checker(state, 0, "TypeChecker<vector>::get");

      // get the length of the table
      lua_len(state, index);
      int tableLength = lua_tointeger(state, -1);
      lua_pop(state, 1);

      std::vector<T, Allocator> returnVector(tableLength);

      // get each individual elemnent and add it to the collection
      for (int currentValue = 1; currentValue <= tableLength; ++currentValue)
      {
        lua_pushinteger(state, currentValue);
        lua_gettable(state, index);
        returnVector[currentValue - 1] = TypeChecker<T>::get(state, lua_gettop(state));
        lua_pop(state, 1);
      }

      // return the collection
      return std::move(returnVector);
    }

    template <class Key, class Value, class Hash, class EqualTo, class Allocator>
      inline std::unordered_map<Key, Value, Hash, EqualTo, Allocator>
        TypeChecker<std::unordered_map<Key, Value, Hash, EqualTo, Allocator> >::get(lua_State* state, int index) noexcept
    {
      StackChecker checker(state, 0, "TypeChecker<map>::get");

      // add all pairs in the table to an unordered map
      std::unordered_map<Key, Value, Hash, EqualTo, Allocator> returnMap;

      lua_pushnil(state);
      while(lua_next(state, index))
      {
        returnMap.insert(std::make_pair(TypeChecker<Key>::get(state, -2), TypeChecker<Value>::get(state, -1)));
        lua_pop(state, 1);
      }

      return std::move(returnMap);
    }

    template <class T>
      typename GetReturnTraits<T>::type get(lua_State* state, int index) noexcept
    {
#ifndef NDEBUG
      try
      {
        check<T>(state, index);
      }
      catch (BadTypenameException& exception)
      {
        std::stringstream exceptionStream;
        exceptionStream << "bad get: " << exception.what();
        lua_pushstring(state, exceptionStream.str().c_str());
        lua_error(state);
      }
#endif
      return TypeChecker<T>::get(state, index);
    }

    template <class T>
      inline bool checkBool(lua_State* state, int index) noexcept
    {
      try
      {
        TypeChecker<T>::check(state, index);
        return true;
      }
      catch(BadTypenameException)
      {
        return false;
      }
    }

    template <int enumType>
      class NativeTypeWrapper;

#define specialise( enumType , typeTest )\
    template <>\
      struct TypeChecker<NativeTypeWrapper< enumType > >\
    {\
      TypeChecker() = delete;\
      static inline void check(lua_State* state, int index)\
      {\
        if (0 == typeTest (state, index))\
        {\
          throw BadTypenameException(LuaTraits<NativeTypeWrapper< enumType > >::getName(), getTypename(state, index), index);\
        }\
      }\
    };

    specialise(LUA_TNIL, lua_isnil);
    specialise(LUA_TSTRING, lua_tostring); // use tostring here as it will return nullptr = 0 on failure
    // we need tostring because numbers and objects with metatables can sometimes be converted to strings
    specialise(LUA_TTABLE, lua_istable);
    specialise(LUA_TFUNCTION, lua_isfunction);
    // use generic specialisation for LUA_TUSERDATA
    specialise(LUA_TTHREAD, lua_isthread);
    specialise(LUA_TLIGHTUSERDATA, lua_islightuserdata);
#undef specialise
    template <>
      struct TypeChecker<NativeTypeWrapper<LUA_TBOOLEAN> >
    {
      static inline void check(lua_State*, int)
      {
        // elements are always convertible to a boolean
      }
    };

    template <>
      struct TypeChecker<NativeTypeWrapper<LUA_TNUMBER> >
    {
      static inline void check(lua_State* state, int index)
      {
        lua_tonumber(state, index);
        if (lua_isnumber(state, index) == 0)
        {
          throw BadTypenameException(LuaTraits<NativeTypeWrapper<LUA_TNUMBER> >::getName(), getTypename(state, index), index);
        }
      }
    };

    template <class T>
      void generateMetatable(lua_State* state, int arraySize, int hashSize)
    {
      lua_createtable(state, arraySize, hashSize + 2);

      lua_pushstring(state, LuaTraits<T>::getName().c_str());
      lua_setfield(state, -2, "__tname");
      lua_pushcfunction(state, garbageCollect<T>);
      lua_setfield(state, -2, "__gc");
    }

    template <class T>
      int garbageCollect(lua_State* state)
    {
      T& objectToKill(get<T>(state, -1));
      objectToKill.~T();
      lua_pop(state, 1);
      return 0;
    }

    template <class T>
      int equality(lua_State* state)
    {
      try
      {
        check<T>(state, 2);
      }
      catch (BadTypenameException&)
      {
        // they are not of the same type - they cannot be equivalent
        lua_pushboolean(state, 0);
      }

      lua_pushboolean(state, get<T>(state, 2) == get<T>(state, 1) ? 1 : 0);

      return 1;
    }

  }
}

#endif
#endif
