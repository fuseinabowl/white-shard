#ifndef _WINDOW_WRAPPER_FUNCTIONS_H_
#define _WINDOW_WRAPPER_FUNCTIONS_H_

#include <string>

namespace WhiteShard{
  namespace Window{
    enum WindowWrapperFunctions
    {
      setWindowPosition,
      getWindowPosition,
      setSize,
      getSize,

      setMouseCursorVisibility,
      setVSync,
      setVisible,
      setTitle,
      setKeyRepeatEnabled,
      setJoystickThreshold,

      restartWindow,
      restartFullscreenWindow,
    };

    template <WindowWrapperFunctions Key>
      struct FunctionName
    {
      static const std::string name;
    };
  }
}

#endif
