#include "window/graphics/openGL.h"

namespace WhiteShard{
  namespace Window{
    namespace Graphics{
      struct ShaderType
      {
        GLuint shaderType;
      };

      struct EnableCode
      {
        GLuint enableCode;
      };

      struct PrimitiveType
      {
        GLenum primitiveType;
      };
    }
  }
}
