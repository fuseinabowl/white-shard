#ifndef _LUA_HANDLE_H_
#define _LUA_HANDLE_H_

#include <string>
#include "state.h"

class lua_State;

namespace WhiteShard{
  class Engine;

  namespace Lua{
    // the only way to create States
    // not thread safe
    // holds a shared_ptr to a lua state
    class Handle{
      static const std::string subsystems;

      static int subsystemIndex(lua_State*);
      static int subsystemNewIndex(lua_State*);

      StatePtr state;
     public:

      Handle(State::precedence, Engine&) noexcept;
      Handle() noexcept;
      ~Handle() noexcept;

      Handle(const Handle&)            = delete;
      Handle& operator=(const Handle&) = delete;
      Handle(Handle&&) noexcept;
      Handle& operator=(Handle&&) noexcept;

      const StatePtr& operator*() const;
      const StatePtr* operator->() const;
    };
  }
}

#endif
