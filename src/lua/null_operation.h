#ifndef _NULL_OPERATION_H_
#define _NULL_OPERATION_H_

class lua_State;

namespace WhiteShard{
  namespace Lua{
    int nullOperation(lua_State*);
  }
}

#endif
