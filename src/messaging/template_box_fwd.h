#ifndef _MESSAGING_TEMPLATE_BOX_FWD_H_
#define _MESSAGING_TEMPLATE_BOX_FWD_H_

#include <cassert>
#include "lua_shard.h"
#include "lua/checker.h"
#include "registry.h"

namespace WhiteShard{
  namespace Messaging{
    // most generic template box
    template <class Object>
      class TemplateBox : public Box
    {
      Object object;
     public:
      TemplateBox() = default;
      void extract(lua_State*);
      void deposit(lua_State*);
    };

    // box for pointer like objects
    template <class PointerObject>
      class PointerTemplateBox : public Box
    {
      PointerObject pointer;
     public:
      PointerTemplateBox() noexcept;
      void extract(lua_State*);
      void deposit(lua_State*);
    };

    // Box Traits specialisations
    template <class T>
      struct BoxTraits
    {
      typedef TemplateBox<T> type;
    };

    template <class T>
      struct BoxTraits<T*>
    {
      typedef PointerTemplateBox<T*> type;
    };

    template <class T>
      struct BoxTraits<std::shared_ptr<T> >
    {
      typedef PointerTemplateBox<std::shared_ptr<T> > type;
    };
    // end of specialisations

    template <class Object>
      int Box::allocateTemplateBox(lua_State* state)
    {
      assert(lua_gettop(state) == 1);

      new (Lua::allocate<BoxPtr>(state)) BoxPtr(new typename BoxTraits<Object>::type());

      return 1;
    }

    template <class Object>
      void TemplateBox<Object>::extract(lua_State* state)
    {
      this->object = Lua::get<Object>(state, -1);
    }

    template <class Object>
      void TemplateBox<Object>::deposit(lua_State* state)
    {
      Lua::StackChecker checker(state, 1, "Box::deposit");

      lua_getfield(state, LUA_REGISTRYINDEX, Registry::WhiteShard.c_str());
      assert(0 == lua_isnil(state, -1));
      lua_rawgeti(state, -1, Registry::Metatables);
      assert(0 == lua_isnil(state, -1));
      lua_getfield(state, -1, Lua::LuaTraits<Object>::getName().c_str());
      new (Lua::allocate<Object>(state)) Object(std::move(this->object));

      lua_replace(state, -3);
      lua_pop(state, 1);
    }

    template <class PointerObject>
      PointerTemplateBox<PointerObject>::PointerTemplateBox() noexcept :
        pointer(nullptr)
    {}

    template <class PointerObject>
      void PointerTemplateBox<PointerObject>::extract(lua_State* state)
    {
      Lua::StackChecker checker(state, 0, "PointerTemplateBox::extract");
      assert(nullptr == this->pointer);
      this->pointer = Lua::get<PointerObject>(state, -1);

      // register the object's address and this object in the light userdata -> userdata map
      lua_getfield(state, LUA_REGISTRYINDEX, Registry::WhiteShard.c_str());
      lua_rawgeti(state, -1, Registry::PointerToUserdataMap);
      lua_pushnil(state);
      lua_copy(state, -4, -1);
      lua_rawsetp(state, -2, &*this->pointer);

      lua_pop(state, 2);
    }

    template <class PointerObject>
      void PointerTemplateBox<PointerObject>::deposit(lua_State* state)
    {
      Lua::StackChecker checker(state, 1, "PointerTemplateBox::deposit");

      // look at first in the registry for the userdata
      // if it's not there, add it
      lua_getfield(state, LUA_REGISTRYINDEX, Registry::WhiteShard.c_str());
      lua_rawgeti(state, -1, Registry::PointerToUserdataMap);
      auto rawPointer = &*this->pointer;
      lua_rawgetp(state, -1, rawPointer);

      if (1 == lua_isnil(state, -1))
      {
        // pop the nil
        lua_pop(state, 1);

        // need to add the object ourselves
        Lua::getMetatable<PointerObject>(state);
        new (Lua::allocate<PointerObject>(state)) PointerObject(std::move(this->pointer));

        // put a copy at the bottom of the stack used in this function
        lua_pushnil(state);
        lua_copy(state, -2, -1);
        lua_insert(state, -4);

        // now register the object
        lua_rawsetp(state, -2, rawPointer);
      }
      else
      {
        // move the object to the bottom of the stack
        lua_insert(state, -3);
      }

      // should have the stack in the following configuration:
      // ... / depositedObject / WhiteShard registry / pointerToUserdataMap
      // and we want
      // ... / depositedObject
      lua_pop(state, 2);

      this->pointer = nullptr; // in case we are dealing with a standard pointer
    }
  }
}

#endif
