#ifndef _GRAPHICS_H_
#define _GRAPHICS_H_

#include <memory>
#include "lua/temporary_object.h"
#include <vector>
#include <utility>
#include <unordered_set>
#include <thread>
#include "context_wrapper.h"
#include "window/window_wrapper.h"
#include "destructor_thread.h"

class lua_State;

namespace WhiteShard{
  namespace Window{
    namespace Graphics{
      extern const unsigned int
        requiredOpenGLMajorVersion,
        requiredOpenGLMinorVersion;

      class Subsystem;
      typedef Subsystem* SubsystemPtr;

      class SubsystemConstructorException : public std::exception
      {
       public:
        SubsystemConstructorException() noexcept;
        virtual ~SubsystemConstructorException() noexcept;

        virtual const char* what() noexcept = 0;
      };

      class GLEWException : public SubsystemConstructorException
      {
        std::string exceptionString;
       public:
        GLEWException(GLenum errorType) noexcept;
        ~GLEWException() noexcept;

        const char* what() noexcept;
      };

      class VersionStringException : public SubsystemConstructorException
      {
       public:
        VersionStringException() noexcept;
        ~VersionStringException() noexcept;

        const char* what() noexcept;
      };

      class VersionInsufficientException : public SubsystemConstructorException
      {
        unsigned int majorVersion, minorVersion;
       public:
        VersionInsufficientException(unsigned int major, unsigned int minor) noexcept;
        ~VersionInsufficientException() noexcept;

        const char* what() noexcept;
      };

      class Shader;
      typedef std::shared_ptr<Shader> ShaderPtr;
      class ShaderProgram;
      typedef std::shared_ptr<ShaderProgram> ShaderProgramPtr;
      class Texture;
      typedef std::shared_ptr<Texture> TexturePtr;
      class Mesh;
      typedef std::shared_ptr<Mesh> MeshPtr;

      struct ShaderType;
      struct EnableCode;
      struct PrimitiveType;

      class PerContextCommand;
      typedef std::unique_ptr<PerContextCommand> PerContextCommandPtr;

      class Subsystem
      {
        // data for all Lua states to access
        MeshPtr unitMesh;

        // texture support stats
        GLint textureUnits; // max textures that can be cached and used simultaneously

        // context locking
        std::mutex availableContextAccess;
        std::condition_variable contextAdded; // signaled on both pure context added and window context added
        std::condition_variable windowContextAdded; // signaled only on window contexts being added
        std::vector<PureContextWrapperPtr> availablePureContexts;
        std::unordered_map<WindowWrapper*, std::pair<std::weak_ptr<WindowWrapper>, WindowContextWrapperPtr> > availableWindowContexts;

        // per context object notification
        // use the same mutex as context locking
        std::vector<PureContextWrapper*> activePureContexts;
        std::unordered_map<WindowWrapper*, WindowContextWrapper*> activeWindowContexts;

        // destruction command
        ObjectDestructor objectDestructor;

        // data about supported extensions and system limits
        GLfloat maxAnisotropicFiltering;
       public:
        explicit Subsystem(unsigned int extraContexts) throw (SubsystemConstructorException);
        ~Subsystem() noexcept;

        // openGL implementation stats
        GLuint getTextureUnits() const noexcept;

        // context returning (for ContextLock objects)
        void returnWindowContext(const WindowWrapperPtr&, std::unique_ptr<WindowContextWrapper>&&) noexcept;
        void returnPureContext(std::unique_ptr<PureContextWrapper>&&) noexcept;

        // registering windows with the context manager
        void addWindowContext(const WindowWrapperPtr&) noexcept;
        void removeWindowContext(const WindowWrapperPtr&) noexcept;

        // deleting textures (needs to be resent to all activeContextWrappers)
        void freeTexture(const Texture*) noexcept;

        // command pushing
        // DestructionCommands will be run once in an arbitrary context
        void pushDestructionCommand(DestructionCommandPtr&&) noexcept;
        // PerContextCommands will be run in each context that is accessible to the user
        void pushPerContextCommand(PerContextCommandPtr&&) noexcept;

        void pushSubsystem(lua_State*);

        static void pushCreateShader(lua_State*);
        static void pushCreateShaderProgram(lua_State*);
        static void pushEnable(lua_State*);
        static void pushDisable(lua_State*);
        static void pushContextLock(lua_State*);
        static void pushHintExtraContexts(lua_State*);
        static void pushRender(lua_State*);
        // pushLoadTexture should be called before pushCreateTexture
        static void pushLoadTexture(lua_State*);
        static void pushCreateTexture(lua_State*);
        static void pushCreateMatrix(lua_State*);
        // data
        void pushUnitMesh(lua_State*);
        static void pushShaderTypes(lua_State*);
        static void pushEnableCodes(lua_State*);
        static void pushMagnificationFilters(lua_State*);
        static void pushAnisotropicFilters(lua_State*, const Subsystem&);
        static void pushMinificationFilters(lua_State*);
        static void pushWrapTypes(lua_State*);

        static int luaCreateShader(lua_State*);
        static int luaCreateShaderProgram(lua_State*);
        static int luaEnable(lua_State*);
        static int luaDisable(lua_State*);
        static int luaContextLock(lua_State*);
        static int luaHintExtraContexts(lua_State*);
        static int luaRender(lua_State*);
        static int luaLoadTexture(lua_State*);
        static int luaCreateTexture(lua_State*);
        static int luaCreateMatrix(lua_State*);
      };
    }
  }
}

#endif
