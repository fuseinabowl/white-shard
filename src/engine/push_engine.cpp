#include "engine.h"
#include <boost/mpl/vector.hpp>
#include <memory>

namespace WhiteShard{
  typedef Engine* EnginePtr;

  namespace Lua{
    template <>
      const std::string& LuaTraits<EnginePtr>::getName() noexcept
    {
      static const std::string name("engine");
      return name;
    }
  }

  struct GetTimeFunction
  {
    static const std::string name;
  };
  const std::string GetTimeFunction::name("getTime");

  struct SetPriorityFunctionFunction
  {
    static const std::string name;
  };
  const std::string SetPriorityFunctionFunction::name("setPriorityFunction");

  struct StopThreadFunction
  {
    static const std::string name;
  };
  const std::string StopThreadFunction::name("stop");

  void Engine::attachToState(Lua::State& luaState)
  {
    lua_State* state = luaState;
    Lua::StackChecker checker(state, 0, "Engine::pushEngine");

    Lua::generateMetatable<EnginePtr>(state, 0, 1);

    // put the engine on the registry
    lua_getfield(state, LUA_REGISTRYINDEX, Registry::WhiteShard.c_str());
    lua_pushnil(state);
    lua_copy(state, -3, -1);
    new (Lua::allocate<EnginePtr>(state)) EnginePtr(this);
    lua_rawseti(state, -2, Registry::Engine);
    lua_pop(state, 1);

    // we now have the metatable again

    // index table
    lua_createtable(state, 0, this->subsystems.size() + 2);

    // add the timer
    lua_pushcfunction(state, Engine::luaGetTime);
    lua_setfield(state, -2, GetTimeFunction::name.c_str());

    // add the priority function
    // need to store the registry table so I can put the function directly in there
    lua_getfield(state, LUA_REGISTRYINDEX, Registry::WhiteShard.c_str());
    lua_pushcclosure(state, Engine::luaSetPriorityFunction, 1);
    lua_setfield(state, -2, SetPriorityFunctionFunction::name.c_str());

    // add the stop function
    lua_getfield(state, LUA_REGISTRYINDEX, Registry::WhiteShard.c_str());
    lua_rawgeti(state, -1, Registry::Engine);
    lua_remove(state, -2);
    lua_pushcclosure(state, Engine::luaStopThread, 1);
    lua_setfield(state, -2, StopThreadFunction::name.c_str());

    // add the subsystems
    std::string subsystemName;
    for (const SubsystemPtr& currentSubsystem : this->subsystems)
    {
      subsystemName = currentSubsystem->pushSubsystem(luaState);
      lua_setfield(state, -2, subsystemName.c_str());
    }

    lua_setfield(state, -2, "__index");

    lua_pop(state, 1);
  }

  int Engine::luaGetTime(lua_State* state)
  {
    Lua::SignatureChecker<
      boost::mpl::vector<Lua::NativeTypeWrapper<LUA_TNUMBER> >::type,
      EnginePtr, GetTimeFunction,
      boost::mpl::vector<>::type
        > sigChecker(state);

    lua_pushnumber(state, Lua::get<EnginePtr>(state, 1)->timer.getElapsedTime().asSeconds());

    return sigChecker.resultCount;
  }

  int Engine::luaSetPriorityFunction(lua_State* state)
  {
    Lua::SignatureChecker<
      boost::mpl::vector<>::type,
      EnginePtr, SetPriorityFunctionFunction,
      boost::mpl::vector<Lua::NativeTypeWrapper<LUA_TFUNCTION> >::type
        > sigChecker(state);

    lua_settop(state, 2);
    lua_rawseti(state, lua_upvalueindex(1), Registry::PriorityFunction);

    return sigChecker.resultCount;
  }

  int Engine::luaStopThread(lua_State* state)
  {
    Lua::SignatureChecker<
      boost::mpl::vector<>::type,
      EnginePtr, StopThreadFunction,
      boost::mpl::vector<>::type
        > sigChecker(state);

    Engine& engine = *Lua::get<EnginePtr>(state, 1);

    std::unique_lock<std::mutex> handleLock(engine.handleAccess);
    auto deletionIterator = std::find_if(engine.runningStates.begin(), engine.runningStates.end(), [state] (const Lua::StatePtr& testPtr)
        {
          return *testPtr == state;
        }
      );

    if (deletionIterator != engine.runningStates.end())
    {
      engine.runningStates.erase(deletionIterator);
      engine.handleCondition.notify_all();
    }

    return sigChecker.resultCount;
  }
}
